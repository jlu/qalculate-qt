<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message>
        <source>Qalculate!</source>
        <translation type="vanished">Qalculate!</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="vanished">Taschenrechner</translation>
    </message>
    <message>
        <source>Powerful and easy to use calculator</source>
        <translation type="vanished">Leistungsstarker und einfach zu bedienender Taschenrechner</translation>
    </message>
    <message>
        <source>calculation;arithmetic;scientific;financial;</source>
        <translation type="vanished">Berechnung;Arithmetik; Wissenschaft;Finanzen;</translation>
    </message>
    <message>
        <source>Qalculate! (Qt UI)</source>
        <translation type="vanished">Qalculate! (Qt UI)</translation>
    </message>
    <message>
        <source>Qalculate! is a multi-purpose cross-platform desktop calculator. It is simple to use but provides power and versatility normally reserved for complicated math packages, as well as useful tools for everyday needs (such as currency conversion and percent calculation).</source>
        <translation type="vanished">Qalculate! ist ein vielseitig einsetzbarer, plattformübergreifender Desktop-Rechner. Er ist einfach zu bedienen, bietet aber Leistung und Vielseitigkeit die normalerweise nur für komplizierte mathematische Pakete vorbehalten sind, sowie nützliche ls auch für den täglichen Bedarf (wie Währungsumrechnung und Prozentrechnung).</translation>
    </message>
    <message>
        <source>Features include a large library of customizable functions, unit calculations and conversion, physical constants, symbolic calculations (including integrals and equations), arbitrary precision, uncertainty propagation, interval arithmetic, plotting, and a user-friendly interface.</source>
        <translation type="vanished">Zu den Leistungsmerkmalen gehören eine große Bibliothek anpassbarer Funktionen, Einheiten-berechnungen und -umrechnungen, physikalische Konstanten, symbolische Berechnungen (einschließlich Integrale und Gleichungen), beliebige Genauigkeit, Unsicherheits-fortpflanzung, Intervallarithmetik, Plotten und eine benutzerfreundliche Oberfläche.</translation>
    </message>
    <message>
        <source>Argument Rules</source>
        <translation type="vanished">Regeln für Argumente</translation>
    </message>
    <message>
        <source>_Cancel</source>
        <translation type="vanished">_Abbruch</translation>
    </message>
    <message>
        <source>Do not save modifications</source>
        <translation type="vanished">Änderungen nicht speichern</translation>
    </message>
    <message>
        <source>_OK</source>
        <translation type="vanished">_OK</translation>
    </message>
    <message>
        <source>Accept the modification of argument rules</source>
        <translation type="vanished">Änderung der Argumentregeln akzeptieren</translation>
    </message>
    <message>
        <source>Enable rules and type test</source>
        <translation type="vanished">Regeln und Typenprüfung einschalten</translation>
    </message>
    <message>
        <source>Custom condition</source>
        <translation type="vanished">Benutzerdefinierte Bedingung</translation>
    </message>
    <message>
        <source>For example if argument is a matrix that must have equal number of rows and columns: rows(\x) = columns(\x)</source>
        <translation type="vanished">Wenn das Argument zum Beispiel eine Matrix ist, die die gleiche Anzahl von Zeilen und Spalten haben muss Spalten: Zeilen(\x) = Spalten(\x)</translation>
    </message>
    <message>
        <source>Allow matrix</source>
        <translation type="vanished">Matrix zulassen</translation>
    </message>
    <message>
        <source>Forbid zero</source>
        <translation type="vanished">Null verbieten</translation>
    </message>
    <message>
        <source>Handle vector</source>
        <translation type="vanished">Vektor verarbeiten</translation>
    </message>
    <message>
        <source>Calculate function for each separate element in vector.</source>
        <translation type="vanished">Funktion für jedes einzelne Element im Vektor berechnen.</translation>
    </message>
    <message>
        <source>Min</source>
        <translation type="vanished">Min</translation>
    </message>
    <message>
        <source>Include equals</source>
        <translation type="vanished">Schließe Gleichheiten ein</translation>
    </message>
    <message>
        <source>Max</source>
        <translation type="vanished">Max</translation>
    </message>
    <message>
        <source>Keyboard Shortcuts</source>
        <translation type="vanished">Tastaturkürzel</translation>
    </message>
    <message>
        <source>_Close</source>
        <translation type="vanished">_Schließen</translation>
    </message>
    <message>
        <source>Label</source>
        <translation type="vanished">Beschriftung</translation>
    </message>
    <message>
        <source>Left-click</source>
        <translation type="vanished">Linksklick</translation>
    </message>
    <message>
        <source>Right-click</source>
        <translation type="vanished">Rechtsklick</translation>
    </message>
    <message>
        <source>Middle-click</source>
        <translation type="vanished">Mittelklick</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Zurücksetzen</translation>
    </message>
    <message>
        <source>Button Action</source>
        <translation type="vanished">Aktion für Schaltfläche</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Wert</translation>
    </message>
    <message>
        <source>Argument name</source>
        <translation type="vanished">Argumentname</translation>
    </message>
    <message>
        <source>Calendar Conversion</source>
        <translation type="vanished">Kalender Konvertierung</translation>
    </message>
    <message>
        <source>Export CSV File</source>
        <translation type="vanished">CSV-Datei exportieren</translation>
    </message>
    <message>
        <source>Current result</source>
        <translation type="vanished">Aktuelles Ergebnis</translation>
    </message>
    <message>
        <source>Matrix/vector variable</source>
        <translation type="vanished">Matrix/Vektor-Variable</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Datei</translation>
    </message>
    <message>
        <source>Delimiter</source>
        <translation type="vanished">Begrenzungszeichen</translation>
    </message>
    <message>
        <source>Comma</source>
        <translation type="vanished">Komma</translation>
    </message>
    <message>
        <source>Tabulator</source>
        <translation type="vanished">Tabulator</translation>
    </message>
    <message>
        <source>Semicolon</source>
        <translation type="vanished">Semikolon</translation>
    </message>
    <message>
        <source>Space</source>
        <translation type="vanished">Leerzeichen</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="vanished">Andere</translation>
    </message>
    <message>
        <source>Import CSV File</source>
        <translation type="vanished">CSV-Datei importieren</translation>
    </message>
    <message>
        <source>Do not import the file</source>
        <translation type="vanished">Die Datei nicht importieren</translation>
    </message>
    <message>
        <source>Import the file</source>
        <translation type="vanished">Die Datei importieren</translation>
    </message>
    <message>
        <source>Name of the data file to import</source>
        <translation type="vanished">Name der zu importierenden Datendatei</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Wählen Sie eine Datei</translation>
    </message>
    <message>
        <source>Import as</source>
        <translation type="vanished">Importieren als</translation>
    </message>
    <message>
        <source>Matrix</source>
        <translation type="vanished">Matrix</translation>
    </message>
    <message>
        <source>If a matrix shall be generated from the contents of the file</source>
        <translation type="vanished">Wenn aus dem Inhalt der Datei eine Matrix erzeugt werden soll</translation>
    </message>
    <message>
        <source>Vectors</source>
        <translation type="vanished">Vektoren</translation>
    </message>
    <message>
        <source>If vectors shall be generated from the contents of the file</source>
        <translation type="vanished">Wenn aus dem Inhalt der Datei Vektoren generiert werden sollen</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Name (or name prefix) used to reference generated variable(s) in expressions</source>
        <translation type="vanished">Name (oder Namenspräfix), der verwendet wird, um die erzeugte(n) Variable(n) in Ausdrücken zu referenzieren</translation>
    </message>
    <message>
        <source>Descriptive name</source>
        <translation type="vanished">Beschreibender Name</translation>
    </message>
    <message>
        <source>Title displayed in menus and in variable manager</source>
        <translation type="vanished">Titel, der in Menüs und im Variablen-Manager angezeigt wird</translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="vanished">Kategorie</translation>
    </message>
    <message>
        <source>First row</source>
        <translation type="vanished">Erste Zeile</translation>
    </message>
    <message>
        <source>The first row with data to import in the file</source>
        <translation type="vanished">Die erste Zeile mit zu importierenden Daten in der Datei</translation>
    </message>
    <message>
        <source>Includes headings</source>
        <translation type="vanished">Enthält Überschriften</translation>
    </message>
    <message>
        <source>If the first row contains column headings</source>
        <translation type="vanished">Wenn die erste Zeile Spaltenüberschriften enthält</translation>
    </message>
    <message>
        <source>Delimiter used to separate columns in the file</source>
        <translation type="vanished">Trennzeichen, das zum Trennen von Spalten in der Datei verwendet wird</translation>
    </message>
    <message>
        <source>Custom delimiter</source>
        <translation type="vanished">Benutzerdefiniertes Begrenzungszeichen</translation>
    </message>
    <message>
        <source>Edit Data Property</source>
        <translation type="vanished">Dateneigenschaft bearbeiten</translation>
    </message>
    <message>
        <source>Do not create/modify this data set</source>
        <translation type="vanished">Diesen Datensatz nicht erstellen/verändern</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this data set</source>
        <translation type="vanished">Erstellung/Änderung dieses Datensatzes akzeptieren</translation>
    </message>
    <message>
        <source>Name used for reference</source>
        <translation type="vanished">Name als Referenz verwendet</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Eigenschaften</translation>
    </message>
    <message>
        <source>Title displayed in menus and in data set manager</source>
        <translation type="vanished">Titel wird in Menüs und im Datensatzmanager angezeigt</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Beschreibung</translation>
    </message>
    <message>
        <source>Description of this data property</source>
        <translation type="vanished">Beschreibung dieser Dateneigenschaft</translation>
    </message>
    <message>
        <source>Value Type</source>
        <translation type="vanished">Wert Typ</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Text</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="vanished">Zahl</translation>
    </message>
    <message>
        <source>Expression</source>
        <translation type="vanished">Ausdruck</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="vanished">Ausblenden</translation>
    </message>
    <message>
        <source>Use as key</source>
        <translation type="vanished">Als Schlüssel verwenden</translation>
    </message>
    <message>
        <source>Approximate value</source>
        <translation type="vanished">Näherungswert</translation>
    </message>
    <message>
        <source>Case sensitive value</source>
        <translation type="vanished">Wert unterscheidet Groß-/Kleinschreibung</translation>
    </message>
    <message>
        <source>Value uses brackets</source>
        <translation type="vanished">Wert verwendet Klammern</translation>
    </message>
    <message>
        <source>Unit expression</source>
        <translation type="vanished">Einheitenausdruck</translation>
    </message>
    <message>
        <source>Edit Data Set</source>
        <translation type="vanished">Datensatz bearbeiten</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="vanished">Titel</translation>
    </message>
    <message>
        <source>Data file</source>
        <translation type="vanished">Daten Datei</translation>
    </message>
    <message>
        <source>Description of this data set</source>
        <translation type="vanished">Beschreibung dieses Datensatzes</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation type="vanished">Urheberrecht</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Allgemein</translation>
    </message>
    <message>
        <source>Properties:</source>
        <translation type="vanished">Eigenschaften</translation>
    </message>
    <message>
        <source>Definition of the properties of this data set</source>
        <translation type="vanished">Definition der Eigenschaften dieses Datensatzes</translation>
    </message>
    <message>
        <source>_New</source>
        <translation type="vanished">_Neu</translation>
    </message>
    <message>
        <source>_Edit</source>
        <translation type="vanished">_Bearbeiten</translation>
    </message>
    <message>
        <source>_Delete</source>
        <translation type="vanished">_Löschen</translation>
    </message>
    <message>
        <source>Name used to invoke the function in expressions</source>
        <translation type="vanished">Name, mit dem die Funktion in Ausdrücken aufgerufen wird</translation>
    </message>
    <message>
        <source>Object argument name</source>
        <translation type="vanished">Objekt-Argumentname</translation>
    </message>
    <message>
        <source>Property argument name</source>
        <translation type="vanished">Name des Eigenschaftsarguments</translation>
    </message>
    <message>
        <source>Default property</source>
        <translation type="vanished">Standard-Eigenschaft</translation>
    </message>
    <message>
        <source>Function</source>
        <translation type="vanished">Funktion</translation>
    </message>
    <message>
        <source>Edit Data Object</source>
        <translation type="vanished">Datenobjekt bearbeiten</translation>
    </message>
    <message>
        <source>Do not create/modify this data object</source>
        <translation type="vanished">Dieses Datenobjekt nicht anlegen/verändern</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this data object</source>
        <translation type="vanished">Anlegen/Ändern dieses Datenobjekts zulassen</translation>
    </message>
    <message>
        <source>Data Sets</source>
        <extracomment>new dataset</extracomment>
        <translation type="vanished">Datensätze</translation>
    </message>
    <message>
        <source>Data Set</source>
        <translation type="vanished">Datensatz</translation>
    </message>
    <message>
        <source>Create a new data set</source>
        <translation type="vanished">Einen neuen Datensatz anlegen</translation>
    </message>
    <message>
        <source>Edit the selected data set</source>
        <translation type="vanished">Ausgewählten Datensatz bearbeiten</translation>
    </message>
    <message>
        <source>Delete the selected data set</source>
        <translation type="vanished">Löschen des ausgewählten Datensatzes</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation type="vanished">Objekte</translation>
    </message>
    <message>
        <source>Create a new data object</source>
        <translation type="vanished">Anlegen eines neuen Datenobjekts</translation>
    </message>
    <message>
        <source>Edit the selected data object</source>
        <translation type="vanished">Ausgewähltes Datenobjekt bearbeiten</translation>
    </message>
    <message>
        <source>Remove the selected data object</source>
        <translation type="vanished">Ausgewähltes Datenobjekt entfernen</translation>
    </message>
    <message>
        <source>Data Set Description</source>
        <translation type="vanished">Datensatz Beschreibung</translation>
    </message>
    <message>
        <source>Object Attributes</source>
        <translation type="vanished">Objekt-Attribute</translation>
    </message>
    <message>
        <source>Decimals</source>
        <translation type="vanished">Dezimalstellen</translation>
    </message>
    <message>
        <source>Close this window</source>
        <translation type="vanished">Dieses Fenster schließen</translation>
    </message>
    <message>
        <source>Min decimals</source>
        <translation type="vanished">Min Dezimalen</translation>
    </message>
    <message>
        <source>Max decimals</source>
        <translation type="vanished">Max Dezimalen</translation>
    </message>
    <message>
        <source>Minimal number of displayed decimals</source>
        <translation type="vanished">Minimale Anzahl der angezeigten Nachkommastellen</translation>
    </message>
    <message>
        <source>Maximal number of decimals to display (and round to)</source>
        <translation type="vanished">Maximale Anzahl der anzuzeigenden (und zu rundenden) Nachkommastellen</translation>
    </message>
    <message>
        <source>Floating Point Conversion</source>
        <translation type="vanished">Gleitkomma-Umrechnung</translation>
    </message>
    <message>
        <source>Decimal value</source>
        <translation type="vanished">Dezimaler Wert</translation>
    </message>
    <message>
        <source>Binary value</source>
        <translation type="vanished">Binärer Wert</translation>
    </message>
    <message>
        <source>Octal value</source>
        <translation type="vanished">Oktalwert</translation>
    </message>
    <message>
        <source>Hexadecimal representation</source>
        <translation type="vanished">Hexadezimale Darstellung</translation>
    </message>
    <message>
        <source>Conversion error</source>
        <translation type="vanished">Konvertierungsfehler</translation>
    </message>
    <message>
        <source>Binary representation</source>
        <translation type="vanished">Binäre Darstellung</translation>
    </message>
    <message>
        <source>Floating point value</source>
        <translation type="vanished">Gleitkommawert</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">Format</translation>
    </message>
    <message>
        <source>16-bit (half precision)</source>
        <translation type="vanished">16-Bit (halbe Genauigkeit)</translation>
    </message>
    <message>
        <source>32-bit (single precision)</source>
        <translation type="vanished">32-Bit (einfache Genauigkeit)</translation>
    </message>
    <message>
        <source>64-bit (double precision)</source>
        <translation type="vanished">64-Bit (doppelte Genauigkeit)</translation>
    </message>
    <message>
        <source>80-bit (x86 extended format)</source>
        <translation type="vanished">80-Bit (erweitertes x86-Format)</translation>
    </message>
    <message>
        <source>128-bit (quadruple precision)</source>
        <translation type="vanished">128-Bit (vierfache Genauigkeit)</translation>
    </message>
    <message>
        <source>Hexadecimal value</source>
        <translation type="vanished">Hexadezimalwert</translation>
    </message>
    <message>
        <source>Edit Function</source>
        <translation type="vanished">Bearbeite Funktion</translation>
    </message>
    <message>
        <source>_Help</source>
        <translation type="vanished">_Hilfe</translation>
    </message>
    <message>
        <source>Do not create/modify this function</source>
        <translation type="vanished">Diese Funktion nicht erstellen/verändern</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this function</source>
        <translation type="vanished">Erstellung/Änderung dieser Funktion zulassen</translation>
    </message>
    <message>
        <source>Name used to invoke this function in expressions</source>
        <translation type="vanished">Name, der zum Aufrufen dieser Funktion in Ausdrücken verwendet wird</translation>
    </message>
    <message>
        <source>Title displayed in menus and in function manager</source>
        <translation type="vanished">Titel, der in Menüs und im Funktionsmanager angezeigt wird</translation>
    </message>
    <message>
        <source>Hide function</source>
        <translation type="vanished">Funktion ausblenden</translation>
    </message>
    <message>
        <source>If this function shall be hidden in menus</source>
        <translation type="vanished">Wenn diese Funktion in Menüs ausgeblendet werden soll</translation>
    </message>
    <message>
        <source>Description of this function</source>
        <translation type="vanished">Beschreibung dieser Funktion</translation>
    </message>
    <message>
        <source>Use \x for the first, \y for the second and \z for the third argument. For more information click the help button.</source>
        <translation type="vanished">Verwenden Sie \x für das erste, \y für das zweite und \z für das dritte Argument. Für weitere Informationen klicken Sie auf die Schaltfläche Hilfe.</translation>
    </message>
    <message>
        <source>Condition that must be true for the function (e.g. if the second argument must be greater than the first: &quot;\y &gt; \x&quot;)</source>
        <translation type="vanished">Bedingung, die für die Funktion wahr sein muss (z. B. wenn das zweite Argument größer sein muss als das erste: &quot;\y &gt; \x&quot;)</translation>
    </message>
    <message>
        <source>Sub-Functions</source>
        <translation type="vanished">Unterfunktionen</translation>
    </message>
    <message>
        <source>Arguments:</source>
        <translation type="vanished">Argumente:</translation>
    </message>
    <message>
        <source>Definition of this function&apos;s arguments</source>
        <translation type="vanished">Definition der Argumente für diese Funktion</translation>
    </message>
    <message>
        <source>Free</source>
        <translation type="vanished">Frei</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation type="vanished">Ganzzahl</translation>
    </message>
    <message>
        <source>Symbol</source>
        <translation type="vanished">Symbol</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Datum</translation>
    </message>
    <message>
        <source>Vector</source>
        <translation type="vanished">Vektor</translation>
    </message>
    <message>
        <source>Positive number</source>
        <translation type="vanished">Positive Zahl</translation>
    </message>
    <message>
        <source>Non-zero number</source>
        <translation type="vanished">Nicht-Null-Zahl</translation>
    </message>
    <message>
        <source>Non-negative number</source>
        <translation type="vanished">Nicht-negative Zahl</translation>
    </message>
    <message>
        <source>Positive integer</source>
        <translation type="vanished">Positive Ganzzahl</translation>
    </message>
    <message>
        <source>Non-zero integer</source>
        <translation type="vanished">Nicht-Null-Ganzzahl</translation>
    </message>
    <message>
        <source>Non-negative integer</source>
        <translation type="vanished">Nicht-negative ganze Zahl</translation>
    </message>
    <message>
        <source>Boolean</source>
        <translation type="vanished">Boolescher Wert</translation>
    </message>
    <message>
        <source>Object</source>
        <translation type="vanished">Objekt</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation type="vanished">Einheit</translation>
    </message>
    <message>
        <source>Variable</source>
        <translation type="vanished">Variable</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="vanished">Winkel</translation>
    </message>
    <message>
        <source>Data object</source>
        <translation type="vanished">Daten-Objekt</translation>
    </message>
    <message>
        <source>Data property</source>
        <translation type="vanished">Daten-Eigenschaft</translation>
    </message>
    <message>
        <source>_Add</source>
        <translation type="vanished">_Hinzufügen</translation>
    </message>
    <message>
        <source>Add entered argument definition</source>
        <translation type="vanished">Eingegebene Argumentdefinition hinzufügen</translation>
    </message>
    <message>
        <source>_Apply</source>
        <translation type="vanished">_Anwenden</translation>
    </message>
    <message>
        <source>Modify selected argument</source>
        <translation type="vanished">Selektiertes Argument modifizieren</translation>
    </message>
    <message>
        <source>Remove selected argument</source>
        <translation type="vanished">Ausgewähltes Argument entfernen</translation>
    </message>
    <message>
        <source>Rules</source>
        <translation type="vanished">Regeln</translation>
    </message>
    <message>
        <source>Edit conditions for selected argument</source>
        <translation type="vanished">Bedingungen für ausgewähltes Argument bearbeiten</translation>
    </message>
    <message>
        <source>Close this dialog</source>
        <translation type="vanished">Diesen Dialog schließen</translation>
    </message>
    <message>
        <source>Precalculate</source>
        <translation type="vanished">Vorberechnen</translation>
    </message>
    <message>
        <source>Calculate the subfunction only once, before the parent function</source>
        <translation type="vanished">Die Unterfunktion nur einmal berechnen, vor der übergeordneten Funktion</translation>
    </message>
    <message>
        <source>Add entered subfunction</source>
        <translation type="vanished">Eingegebene Unterfunktion hinzufügen</translation>
    </message>
    <message>
        <source>Apply changes to the selected subfunction</source>
        <translation type="vanished">Änderungen auf die ausgewählte Unterfunktion anwenden</translation>
    </message>
    <message>
        <source>Remove the selected subfunction</source>
        <translation type="vanished">Markierte Teilfunktion entfernen</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation type="vanished">Funktionen</translation>
    </message>
    <message>
        <source>Create a new function</source>
        <translation type="vanished">Eine neue Funktion erstellen</translation>
    </message>
    <message>
        <source>Edit the selected function</source>
        <translation type="vanished">Ausgewählte Funktion bearbeiten</translation>
    </message>
    <message>
        <source>_Insert</source>
        <translation type="vanished">_Einfügen</translation>
    </message>
    <message>
        <source>Insert (or execute) the selected function into the expression entry</source>
        <translation type="vanished">Einfügen (oder Ausführen) der markierten Funktion in den Ausdruckseintrag</translation>
    </message>
    <message>
        <source>Delete the selected function</source>
        <translation type="vanished">Löschen der markierten Funktion</translation>
    </message>
    <message>
        <source>(De)activate the selected function</source>
        <translation type="vanished">Die gewählte Funktion (de)aktivieren</translation>
    </message>
    <message>
        <source>Deacti_vate</source>
        <translation type="vanished">Deakti_vieren</translation>
    </message>
    <message>
        <source>Apply the selected function to the current expression</source>
        <translation type="vanished">Anwenden der ausgewählten Funktion auf den aktuellen Ausdruck</translation>
    </message>
    <message>
        <source>Categor_y</source>
        <translation type="vanished">Kategorie_y</translation>
    </message>
    <message>
        <source>_Function</source>
        <translation type="vanished">_Funktion</translation>
    </message>
    <message>
        <source>Descri_ption</source>
        <translation type="vanished">Beschrei_bung</translation>
    </message>
    <message>
        <source>Degrees</source>
        <translation type="vanished">Grad</translation>
    </message>
    <message>
        <source>Radians</source>
        <translation type="vanished">Bogenmaß</translation>
    </message>
    <message>
        <source>Gradians</source>
        <translation type="vanished">Neugrad</translation>
    </message>
    <message>
        <source>Default assumptions</source>
        <translation type="vanished">Standardannahmen</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Unbekannt</translation>
    </message>
    <message>
        <source>Not Matrix</source>
        <translation type="vanished">Nicht Matrix</translation>
    </message>
    <message>
        <source>Complex</source>
        <translation type="vanished">Komplex</translation>
    </message>
    <message>
        <source>Real</source>
        <translation type="vanished">Real</translation>
    </message>
    <message>
        <source>Rational</source>
        <translation type="vanished">Rational</translation>
    </message>
    <message>
        <source>Non-Zero</source>
        <translation type="vanished">Nicht-Null</translation>
    </message>
    <message>
        <source>Positive</source>
        <translation type="vanished">Positiv</translation>
    </message>
    <message>
        <source>Non-Negative</source>
        <translation type="vanished">Nicht-Negativ</translation>
    </message>
    <message>
        <source>Negative</source>
        <translation type="vanished">Negativ</translation>
    </message>
    <message>
        <source>Non-Positive</source>
        <translation type="vanished">Nicht-Positiv</translation>
    </message>
    <message>
        <source>_File</source>
        <translation type="vanished">_Datei</translation>
    </message>
    <message>
        <source>Unknown Variable</source>
        <translation type="vanished">Unbekannte Variable</translation>
    </message>
    <message>
        <source>Function (simplified)</source>
        <translation type="vanished">Funktion (vereinfacht)</translation>
    </message>
    <message>
        <source>Import CSV File…</source>
        <translation type="vanished">CSV-Datei importieren...</translation>
    </message>
    <message>
        <source>Export CSV File…</source>
        <translation type="vanished">CSV-Datei exportieren...</translation>
    </message>
    <message>
        <source>_Store Result…</source>
        <translation type="vanished">_Ergebnis speichern...</translation>
    </message>
    <message>
        <source>Save Result Image…</source>
        <translation type="vanished">Ergebnisbild speichern...</translation>
    </message>
    <message>
        <source>Save local functions, variables and units</source>
        <translation type="vanished">Lokale Funktionen, Variablen und Einheiten speichern</translation>
    </message>
    <message>
        <source>Save Definitions</source>
        <translation type="vanished">Definitionen speichern</translation>
    </message>
    <message>
        <source>Import Definitions File…</source>
        <translation type="vanished">Definitionsdatei importieren...</translation>
    </message>
    <message>
        <source>Fetch current exchange rates from the Internet</source>
        <translation type="vanished">Aktuelle Wechselkurse aus dem Internet abrufen</translation>
    </message>
    <message>
        <source>Update Exchange Rates</source>
        <translation type="vanished">Wechselkurse aktualisieren</translation>
    </message>
    <message>
        <source>Plot Functions/Data</source>
        <translation type="vanished">Funktionen/Daten plotten</translation>
    </message>
    <message>
        <source>Convert Number Bases</source>
        <translation type="vanished">Zahlenbasen umwandeln</translation>
    </message>
    <message>
        <source>Floating Point Conversion (IEEE 754)</source>
        <translation type="vanished">Gleitkomma-Konvertierung (IEEE 754)</translation>
    </message>
    <message>
        <source>Percentage Calculation Tool</source>
        <translation type="vanished">Werkzeug zur Prozentberechnung</translation>
    </message>
    <message>
        <source>Periodic Table</source>
        <translation type="vanished">Periodensystem</translation>
    </message>
    <message>
        <source>Minimal Window</source>
        <translation type="vanished">Minimales Fenster</translation>
    </message>
    <message>
        <source>_Quit</source>
        <translation type="vanished">_Beenden</translation>
    </message>
    <message>
        <source>Manage Variables</source>
        <translation type="vanished">Variablen verwalten</translation>
    </message>
    <message>
        <source>Manage Functions</source>
        <translation type="vanished">Funktionen verwalten</translation>
    </message>
    <message>
        <source>Manage Units</source>
        <translation type="vanished">Einheiten verwalten</translation>
    </message>
    <message>
        <source>Manage Data Sets</source>
        <translation type="vanished">Datensätze verwalten</translation>
    </message>
    <message>
        <source>Factorize</source>
        <translation type="vanished">Faktorisieren</translation>
    </message>
    <message>
        <source>Expand</source>
        <translation type="vanished">Erweitern</translation>
    </message>
    <message>
        <source>Apply partial fraction decomposition to the current result.</source>
        <translation type="vanished">Teilbruchzerlegung auf das aktuelle Ergebnis anwenden.</translation>
    </message>
    <message>
        <source>Expand Partial Fractions</source>
        <translation type="vanished">Partielle Brüche expandieren</translation>
    </message>
    <message>
        <source>Set Unknowns…</source>
        <translation type="vanished">Unbekannte bestimmen...</translation>
    </message>
    <message>
        <source>Convert to Unit</source>
        <translation type="vanished">In Einheit umrechnen</translation>
    </message>
    <message>
        <source>Set Prefix</source>
        <translation type="vanished">Präfix setzen</translation>
    </message>
    <message>
        <source>Convert to Unit Expression…</source>
        <translation type="vanished">In Einheitsausdruck umrechnen...</translation>
    </message>
    <message>
        <source>Convert to Base Units</source>
        <translation type="vanished">In Basiseinheiten umrechnen</translation>
    </message>
    <message>
        <source>Convert to Optimal Unit</source>
        <translation type="vanished">In optimale Einheit umrechnen</translation>
    </message>
    <message>
        <source>Insert Date…</source>
        <translation type="vanished">Datum einfügen...</translation>
    </message>
    <message>
        <source>Insert Matrix…</source>
        <translation type="vanished">Matrix einfügen...</translation>
    </message>
    <message>
        <source>Insert Vector…</source>
        <translation type="vanished">Vektor einfügen...</translation>
    </message>
    <message>
        <source>_Copy Result</source>
        <translation type="vanished">_Ergebnis kopieren</translation>
    </message>
    <message>
        <source>Customize Keypad Buttons</source>
        <translation type="vanished">Tastenfeld-Schaltflächen anpassen</translation>
    </message>
    <message>
        <source>_Preferences</source>
        <translation type="vanished">_Voreinstellungen</translation>
    </message>
    <message>
        <source>_Mode</source>
        <translation type="vanished">_Modus</translation>
    </message>
    <message>
        <source>Number Base</source>
        <translation type="vanished">Zahlenbasis</translation>
    </message>
    <message>
        <source>Select Result and Expression Base…</source>
        <translation type="vanished">Ergebnis und Ausdrucksbasis wählen...</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation type="vanished">Binär</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="vanished">Oktal</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation type="vanished">Dezimal</translation>
    </message>
    <message>
        <source>Duodecimal</source>
        <translation type="vanished">Duodezimal</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation type="vanished">Hexadezimal</translation>
    </message>
    <message>
        <source>Other…</source>
        <translation type="vanished">Andere...</translation>
    </message>
    <message>
        <source>Sexagesimal</source>
        <translation type="vanished">Sexagesimal</translation>
    </message>
    <message>
        <source>Time Format</source>
        <translation type="vanished">Zeitformat</translation>
    </message>
    <message>
        <source>Roman Numerals</source>
        <translation type="vanished">Römische Ziffern</translation>
    </message>
    <message>
        <source>Numerical Display</source>
        <translation type="vanished">Numerische Anzeige</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">Normal</translation>
    </message>
    <message>
        <source>Engineering</source>
        <translation type="vanished">Technisch</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation type="vanished">Wissenschaftlich</translation>
    </message>
    <message>
        <source>Purely Scientific</source>
        <translation type="vanished">Rein wissenschaftlich</translation>
    </message>
    <message>
        <source>Simple</source>
        <translation type="vanished">Einfach</translation>
    </message>
    <message>
        <source>Off: 1/7 ≈ 0.14285714
On: 1/7 = 0.142857 142857...</source>
        <translation type="vanished">Aus: 1/7 ≈ 0.14285714
Ein: 1/7 = 0.142857 142857...</translation>
    </message>
    <message>
        <source>Indicate Repeating Decimals</source>
        <translation type="vanished">Wiederholte Dezimalstellen anzeigen</translation>
    </message>
    <message>
        <source>Show Ending Zeroes</source>
        <translation type="vanished">Endnullen anzeigen</translation>
    </message>
    <message>
        <source>Off: 2.5 ≈ 3,  1.5 ≈ 2
On: 2.5 ≈ 2, 1.5 ≈ 2</source>
        <translation type="vanished">Aus: 2.5 ≈ 3,  1.5 ≈ 2
Ein: 2.5 ≈ 2, 1.5 ≈ 2</translation>
    </message>
    <message>
        <source>Round Halfway Numbers to Even</source>
        <translation type="vanished">Halbe Zahlen auf gerade Zahlen runden</translation>
    </message>
    <message>
        <source>Off: -x + y
On: y - x</source>
        <translation type="vanished">Aus: -x + y
Ein: y - x</translation>
    </message>
    <message>
        <source>Sort Minus Last</source>
        <translation type="vanished">Minus zuletzt sortieren</translation>
    </message>
    <message>
        <source>Complex Rectangular Form</source>
        <translation type="vanished">Komplexe Rechtwinklige Form</translation>
    </message>
    <message>
        <source>Complex Exponential Form</source>
        <translation type="vanished">Komplexe Exponentialform</translation>
    </message>
    <message>
        <source>Complex Polar Form</source>
        <translation type="vanished">Komplexe Polarform</translation>
    </message>
    <message>
        <source>Complex Angle/Phasor Notation</source>
        <translation type="vanished">Komplexe Winkel-/Phasenschreibweise</translation>
    </message>
    <message>
        <source>Rational Number Form</source>
        <translation type="vanished">Rationale Zahlenform</translation>
    </message>
    <message>
        <source>1/3 ≈ 0.33333</source>
        <translation type="vanished">1/3 ≈ 0.33333</translation>
    </message>
    <message>
        <source>Decimal Fractions</source>
        <translation type="vanished">Dezimalbrüche</translation>
    </message>
    <message>
        <source>3/9 = 1/3
6/4 = 1.5</source>
        <translation type="vanished">3/9 = 1/3
6/4 = 1.5</translation>
    </message>
    <message>
        <source>Exact Decimal Fractions</source>
        <translation type="vanished">Exakte Dezimalbrüche</translation>
    </message>
    <message>
        <source>6/4 = 3/2</source>
        <translation type="vanished">6/4 = 3/2</translation>
    </message>
    <message>
        <source>Simple Fractions</source>
        <translation type="vanished">Einfache Brüche</translation>
    </message>
    <message>
        <source>6/4 = 1+1/2</source>
        <translation type="vanished">6/4 = 1+1/2</translation>
    </message>
    <message>
        <source>Mixed Fractions</source>
        <translation type="vanished">Gemischte Brüche</translation>
    </message>
    <message>
        <source>Interval Display</source>
        <translation type="vanished">Intervall-Anzeige</translation>
    </message>
    <message>
        <source>Off: 1/2*pi ≈ 1.5707963
On: 1/2*pi = 0.5 pi</source>
        <translation type="vanished">Aus: 1/2*pi ≈ 1.5707963
Ein: 1/2*pi = 0.5 pi</translation>
    </message>
    <message>
        <source>Adaptive</source>
        <translation type="vanished">Adaptiv</translation>
    </message>
    <message>
        <source>Calculates an interval of possible values and keeps track of precision changes.</source>
        <translation type="vanished">Berechnet ein Intervall möglicher Werte und verfolgt die Präzisions-.änderungen</translation>
    </message>
    <message>
        <source>Significant Digits</source>
        <translation type="vanished">Signifikante Ziffern</translation>
    </message>
    <message>
        <source>Interval</source>
        <translation type="vanished">Intervall</translation>
    </message>
    <message>
        <source>Plus/Minus</source>
        <translation type="vanished">Plus/Minus</translation>
    </message>
    <message>
        <source>Midpoint</source>
        <translation type="vanished">Mittelwert</translation>
    </message>
    <message>
        <source>Unit Display</source>
        <translation type="vanished">Anzeige der Einheit</translation>
    </message>
    <message>
        <source>Do not use any prefixes in result</source>
        <translation type="vanished">Keine Präfixe im Ergebnis verwenden</translation>
    </message>
    <message>
        <source>Show prefixes for primarily SI and CGS units.</source>
        <translation type="vanished">Präfixe für primär SI- und CGS-Einheiten anzeigen.</translation>
    </message>
    <message>
        <source>Use prefixes for selected units</source>
        <translation type="vanished">Präfixe für ausgewählte Einheiten verwenden</translation>
    </message>
    <message>
        <source>Use prefixes also for currencies</source>
        <translation type="vanished">Präfixe auch für Währungen verwenden</translation>
    </message>
    <message>
        <source>Use prefixs for all units</source>
        <translation type="vanished">Präfixe für alle Einheiten verwenden</translation>
    </message>
    <message>
        <source>Enables automatic use of hekto, deka, deci and centi when prefixes is enabled</source>
        <translation type="vanished">Ermöglicht die automatische Verwendung von Hekto, Deka, Dezi und Centi, wenn Präfixe aktiviert sind</translation>
    </message>
    <message>
        <source>Enable All SI Prefixes</source>
        <translation type="vanished">Alle SI-Präfixe einschalten</translation>
    </message>
    <message>
        <source>Enables automatic setting of prefix for denominator in addition to the numerator</source>
        <translation type="vanished">Ermöglicht das automatische Setzen des Präfixes für den Nenner zusätzlich zum Zähler</translation>
    </message>
    <message>
        <source>Enable Denominator Prefixes</source>
        <translation type="vanished">Nenner-Präfixe einschalten</translation>
    </message>
    <message>
        <source>Off: J / K
On: J * K^-1</source>
        <translation type="vanished">Aus: J / K
Ein: J * K^-1</translation>
    </message>
    <message>
        <source>Negative Exponents</source>
        <translation type="vanished">Negative Exponenten</translation>
    </message>
    <message>
        <source>Off: (2 m)/s
On: 2 (m/s)</source>
        <translation type="vanished">Aus: (2 m)/s
Ein: 2 (m/s)</translation>
    </message>
    <message>
        <source>Place Units Separately</source>
        <translation type="vanished">Einheiten gesondert platzieren</translation>
    </message>
    <message>
        <source>No Additional Conversion</source>
        <translation type="vanished">Keine zusätzliche Umrechnung</translation>
    </message>
    <message>
        <source>Convert to Optimal SI Unit</source>
        <translation type="vanished">In optimale SI-Einheit umrechnen</translation>
    </message>
    <message>
        <source>If enabled:
15 in = 1 ft + 3 in
3.2 h = 3 h + 12 min</source>
        <translation type="vanished">Wenn aktiviert:
15 in = 1 ft + 3 in
3,2 h = 3 h + 12 min</translation>
    </message>
    <message>
        <source>Convert to Mixed Units</source>
        <translation type="vanished">In gemischte Einheiten umrechnen</translation>
    </message>
    <message>
        <source>Abbreviate Names</source>
        <translation type="vanished">Namen abkürzen</translation>
    </message>
    <message>
        <source>Enabled Objects</source>
        <translation type="vanished">Aktivierte Objekte</translation>
    </message>
    <message>
        <source>Variables</source>
        <translation type="vanished">Variablen</translation>
    </message>
    <message>
        <source>Units</source>
        <translation type="vanished">Einheiten</translation>
    </message>
    <message>
        <source>Unknowns</source>
        <translation type="vanished">Unbekannte</translation>
    </message>
    <message>
        <source>Units in Physical Constants</source>
        <translation type="vanished">Einheiten in physikalischen Konstanten</translation>
    </message>
    <message>
        <source>If not enabled, treats all variables as unknown</source>
        <translation type="vanished">Wenn nicht aktiviert, werden alle Variablen als unbekannt behandelt</translation>
    </message>
    <message>
        <source>Calculate Variables</source>
        <translation type="vanished">Variablen berechnen</translation>
    </message>
    <message>
        <source>Disables/enables complex numbers in result</source>
        <translation type="vanished">Deaktiviert/aktiviert komplexe Zahlen im Ergebnis</translation>
    </message>
    <message>
        <source>Allow Complex Result</source>
        <translation type="vanished">Komplexes Ergebnis zulassen</translation>
    </message>
    <message>
        <source>Disables/enables infinite numbers in result</source>
        <translation type="vanished">Deaktiviert/aktiviert unendliche Zahlen im Ergebnis</translation>
    </message>
    <message>
        <source>Allow Infinite Result</source>
        <translation type="vanished">Unendliches Ergebnis zulassen</translation>
    </message>
    <message>
        <source>Approximation</source>
        <translation type="vanished">Annäherung</translation>
    </message>
    <message>
        <source>Always Exact</source>
        <translation type="vanished">Immer genau</translation>
    </message>
    <message>
        <source>Try Exact</source>
        <translation type="vanished">Exakt versuchen</translation>
    </message>
    <message>
        <source>Approximate</source>
        <translation type="vanished">Annähern</translation>
    </message>
    <message>
        <source>Interval Arithmetic</source>
        <translation type="vanished">Intervall-Arithmetik</translation>
    </message>
    <message>
        <source>Interval Calculation</source>
        <translation type="vanished">Intervall-Berechnung</translation>
    </message>
    <message>
        <source>Variance Formula</source>
        <translation type="vanished">Varianz-Formel</translation>
    </message>
    <message>
        <source>Change angle unit used in trigonometric functions</source>
        <translation type="vanished">Ändern der in trigonometrischen Funktionen verwendeten Winkeleinheit</translation>
    </message>
    <message>
        <source>Angle Unit</source>
        <translation type="vanished">Winkeleinheit</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">Keine</translation>
    </message>
    <message>
        <source>Assumptions</source>
        <translation type="vanished">Annahmen</translation>
    </message>
    <message>
        <source>Algebraic Mode</source>
        <translation type="vanished">Algebraischer Modus</translation>
    </message>
    <message>
        <source>Assume that unknown denominators are non-zero</source>
        <translation type="vanished">Annehmen, dass unbekannte Nenner ungleich Null sind</translation>
    </message>
    <message>
        <source>Non-Zero Denominators</source>
        <translation type="vanished">Nicht-Null-Nenner</translation>
    </message>
    <message>
        <source>Warn when unknown denominators are assumed non-zero</source>
        <translation type="vanished">Warnen, wenn unbekannte Nenner als ungleich Null angenommen werden</translation>
    </message>
    <message>
        <source>Warn About Denominators Assumed Non-Zero</source>
        <translation type="vanished">Warnung bei angenommenen Nennern ungleich Null</translation>
    </message>
    <message>
        <source>Parsing Mode</source>
        <translation type="vanished">Analyse-Modus</translation>
    </message>
    <message>
        <source>Adaptive Parsing</source>
        <translation type="vanished">Adaptive Analyse</translation>
    </message>
    <message>
        <source>Parse Implicit Multiplication First</source>
        <translation type="vanished">Implizite Multiplikation zuerst analysieren</translation>
    </message>
    <message>
        <source>Conventional Parsing</source>
        <translation type="vanished">Konventionelles Analysieren</translation>
    </message>
    <message>
        <source>Chain Syntax</source>
        <translation type="vanished">Verketteter Syntax</translation>
    </message>
    <message>
        <source>RPN Syntax</source>
        <translation type="vanished">RPN-Syntax</translation>
    </message>
    <message>
        <source>Off: xy = x*y
On: xy != x*y</source>
        <translation type="vanished">Aus: xy = x*y
Ein: xy != x*y</translation>
    </message>
    <message>
        <source>Parse decimal numbers as approximate with precision equal to the number of digits.

Off: 1.1 * 1.1 = 1.21
On: 1.1 * 1.1 ≈ 1.2</source>
        <translation type="vanished">Analysiert Dezimalzahlen als Näherungswerte mit einer Genauigkeit, die der Anzahl der Ziffern entspricht.

Aus: 1.1 * 1.1 = 1.21
Ein: 1.1 * 1.1 ≈ 1.2</translation>
    </message>
    <message>
        <source>Read Precision</source>
        <translation type="vanished">Genauigkeit lesen</translation>
    </message>
    <message>
        <source>_Precision</source>
        <translation type="vanished">_Genauigkeit</translation>
    </message>
    <message>
        <source>_Decimals</source>
        <translation type="vanished">_Dezimalstellen</translation>
    </message>
    <message>
        <source>Calculate As You Type</source>
        <translation type="vanished">Berechnen während der Eingabe</translation>
    </message>
    <message>
        <source>Chain Mode</source>
        <translation type="vanished">Methodenverkettung</translation>
    </message>
    <message>
        <source>Activate the RPN stack.</source>
        <translation type="vanished">Aktivieren Sie den RPN-Stapel.</translation>
    </message>
    <message>
        <source>RPN Mode</source>
        <translation type="vanished">RPN-Modus</translation>
    </message>
    <message>
        <source>Meta Modes</source>
        <translation type="vanished">Meta-Modi</translation>
    </message>
    <message>
        <source>Save Mode…</source>
        <translation type="vanished">Speichere Modus...</translation>
    </message>
    <message>
        <source>Delete Mode…</source>
        <translation type="vanished">Modus löschen...</translation>
    </message>
    <message>
        <source>Save Default _Mode</source>
        <translation type="vanished">Standard _Modus speichern</translation>
    </message>
    <message>
        <source>Fu_nctions</source>
        <translation type="vanished">Fu_nktionen</translation>
    </message>
    <message>
        <source>_Variables</source>
        <translation type="vanished">_Variablen</translation>
    </message>
    <message>
        <source>_Units</source>
        <translation type="vanished">_Einheiten</translation>
    </message>
    <message>
        <source>_Contents</source>
        <translation type="vanished">_Inhalte</translation>
    </message>
    <message>
        <source>Report a Bug</source>
        <translation type="vanished">Einen Fehler melden</translation>
    </message>
    <message>
        <source>Check for Updates</source>
        <translation type="vanished">Nach Updates suchen</translation>
    </message>
    <message>
        <source>_About</source>
        <translation type="vanished">_Über</translation>
    </message>
    <message>
        <source>Toggle minimal window</source>
        <translation type="vanished">Schalte um auf minimales Fenster</translation>
    </message>
    <message>
        <source>Calculation result</source>
        <translation type="vanished">Berechnungsergebnis</translation>
    </message>
    <message>
        <source>_Keypad</source>
        <translation type="vanished">_Tastatur</translation>
    </message>
    <message>
        <source>Toggles persistent keypad (makes it possible to show keypad and history simultaneously)</source>
        <translation type="vanished">Schaltet das beständige Tastenfeld um (ermöglicht die gleichzeitige Anzeige von Tastenfeld und Verlauf)</translation>
    </message>
    <message>
        <source>_History</source>
        <translation type="vanished">_Historie</translation>
    </message>
    <message>
        <source>C_onversion</source>
        <translation type="vanished">U_mrechnung</translation>
    </message>
    <message>
        <source>RPN Stack</source>
        <translation type="vanished">RPN-Stack</translation>
    </message>
    <message>
        <source>Insert the selected value</source>
        <translation type="vanished">Einfügen des markierten Wertes</translation>
    </message>
    <message>
        <source>Insert the selected text</source>
        <translation type="vanished">Einfügen des markierten Textes</translation>
    </message>
    <message>
        <source>Copy the selected text</source>
        <translation type="vanished">Kopieren des markierten Textes</translation>
    </message>
    <message>
        <source>Add the selected value(s)</source>
        <translation type="vanished">Addiert den/die markierten Wert(e)</translation>
    </message>
    <message>
        <source>Subtract the selected value(s)</source>
        <translation type="vanished">Subtrahiert den/die markierten Wert(e)</translation>
    </message>
    <message>
        <source>Multiply the selected value(s)</source>
        <translation type="vanished">Multiplizieren des/der markierten Werte(s)</translation>
    </message>
    <message>
        <source>Divide the the selected value(s)</source>
        <translation type="vanished">Dividieren des/der markierten Werte(s)</translation>
    </message>
    <message>
        <source>Raise to the power of the selected value</source>
        <translation type="vanished">Erhöhen mit der Potenz des markierten Wertes</translation>
    </message>
    <message>
        <source>Calculate the square root of the selected value</source>
        <translation type="vanished">Berechnen der Quadratwurzel des gewählten Wertes</translation>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">Verlauf</translation>
    </message>
    <message>
        <source>Subtract the top value from the second value</source>
        <translation type="vanished">Subtrahieren des oberen Wertes vom zweiten Wert</translation>
    </message>
    <message>
        <source>Multiply the top two values</source>
        <translation type="vanished">Multiplizieren der beiden oberen Werte</translation>
    </message>
    <message>
        <source>Divide the second value by the top value</source>
        <translation type="vanished">Dividieren des zweiten Wertes durch den oberen Wert</translation>
    </message>
    <message>
        <source>Raise the second value to the power of the top value</source>
        <translation type="vanished">Erhöhen des zweiten Wertes mit der Potenz des oberen Wertes</translation>
    </message>
    <message>
        <source>Negate the top value (Ctrl+-)</source>
        <translation type="vanished">Negieren des oberen Wertes (Strg+-)</translation>
    </message>
    <message>
        <source>Invert the top value</source>
        <translation type="vanished">Invertieren des oberen Wertes</translation>
    </message>
    <message>
        <source>Calculate the square root of the top value</source>
        <translation type="vanished">Quadratwurzel des obersten Wertes berechnen</translation>
    </message>
    <message>
        <source>Calculate the sum of all values</source>
        <translation type="vanished">Summe aller Werte berechnen</translation>
    </message>
    <message>
        <source>Rotate the stack or move selected register up</source>
        <translation type="vanished">Drehen des Stapels oder Verschieben des ausgewählten Register nach oben</translation>
    </message>
    <message>
        <source>Rotate the stack or move selected register down</source>
        <translation type="vanished">Drehen des Stapels oder Verschieben des ausgewählten Register nach unten</translation>
    </message>
    <message>
        <source>Swap the two top values or move the selected value to the top of the stack</source>
        <translation type="vanished">Vertauschen Sie die beiden oberen Werte oder verschieben Sie den ausgewählten Wert an die Spitze des Stapels</translation>
    </message>
    <message>
        <source>Copy the selected or top value to the top of the stack</source>
        <translation type="vanished">Kopieren des ausgewählten oder obersten Wertes an die Spitze des Stapels</translation>
    </message>
    <message>
        <source>Enter the top value from before the last numeric operation</source>
        <translation type="vanished">Eingabe des obersten Wertes von vor der letzten numerischen Operation</translation>
    </message>
    <message>
        <source>Delete the top or selected value</source>
        <translation type="vanished">Löschen des oberen oder ausgewählten Wertes</translation>
    </message>
    <message>
        <source>Edit the selected value</source>
        <translation type="vanished">Bearbeiten des ausgewählten Wertes</translation>
    </message>
    <message>
        <source>Clear the RPN stack</source>
        <translation type="vanished">Löschen des RPN-Stack</translation>
    </message>
    <message>
        <source>Unit(s) and prefix to convert result to</source>
        <translation type="vanished">Einheit(en) und Präfix zum Umrechnen des Ergebnisses in</translation>
    </message>
    <message>
        <source>Convert</source>
        <translation type="vanished">Umrechnen</translation>
    </message>
    <message>
        <source>Continuous conversion</source>
        <translation type="vanished">Kontinuierliche Konvertierung</translation>
    </message>
    <message>
        <source>Automatically convert result to the current unit expression as long as the conversion box is visible.</source>
        <translation type="vanished">Ergebnis automatisch in den aktuellen Einheitenausdruck umrechnen, solange das Umrechnungsfeld sichtbar ist.</translation>
    </message>
    <message>
        <source>Add prefix</source>
        <translation type="vanished">Präfix hinzufügen</translation>
    </message>
    <message>
        <source>If unit expression does not contain any prefixes, use optimal prefix. 

This can be overridden by prepending the unit expression with &quot;?&quot; or &quot;0&quot;.</source>
        <translation type="vanished">Wenn der Einheitsausdruck keine Präfixe enthält, verwenden Sie das optimale Präfix. 

Dies kann außer Kraft gesetzt werden, indem dem Einheitenausdruck ein &quot;?&quot; oder &quot;0&quot; vorangestellt wird.</translation>
    </message>
    <message>
        <source>Conversion</source>
        <translation type="vanished">Konvertierung</translation>
    </message>
    <message>
        <source>Show/hide programming keypad</source>
        <translation type="vanished">Programmiertastatur ein-/ausblenden</translation>
    </message>
    <message>
        <source>Exact</source>
        <translation type="vanished">Genau</translation>
    </message>
    <message>
        <source>Fraction</source>
        <translation type="vanished">Bruchteil</translation>
    </message>
    <message>
        <source>Numerical display</source>
        <translation type="vanished">Numerische Anzeige</translation>
    </message>
    <message>
        <source>Pure</source>
        <translation type="vanished">Rein</translation>
    </message>
    <message>
        <source>Number base</source>
        <translation type="vanished">Zahlenbasis</translation>
    </message>
    <message>
        <source>Time format</source>
        <translation type="vanished">Zeitformat</translation>
    </message>
    <message>
        <source>Roman</source>
        <translation type="vanished">Römisch</translation>
    </message>
    <message>
        <source>sin</source>
        <translation type="vanished">sin</translation>
    </message>
    <message>
        <source>cos</source>
        <translation type="vanished">cos</translation>
    </message>
    <message>
        <source>tan</source>
        <translation type="vanished">tan</translation>
    </message>
    <message>
        <source>ln</source>
        <translation type="vanished">ln</translation>
    </message>
    <message>
        <source>Equals</source>
        <translation type="vanished">Entspricht</translation>
    </message>
    <message>
        <source>sqrt</source>
        <translation type="vanished">sqrt</translation>
    </message>
    <message>
        <source>sum</source>
        <translation type="vanished">sum</translation>
    </message>
    <message>
        <source>Unknown variable</source>
        <translation type="vanished">Unbekannte Variable</translation>
    </message>
    <message>
        <source>mod</source>
        <translation type="vanished">mod</translation>
    </message>
    <message>
        <source>mean</source>
        <translation type="vanished">Mittelwert</translation>
    </message>
    <message>
        <source>Store result as a variable</source>
        <translation type="vanished">Ergebnis als Variable speichern</translation>
    </message>
    <message>
        <source>STO</source>
        <extracomment>Standard calculator button. Do not use more than three characters.</extracomment>
        <translation type="vanished">STO</translation>
    </message>
    <message>
        <source>Convert number bases</source>
        <translation type="vanished">Zahlenbasen umrechnen</translation>
    </message>
    <message>
        <source>Imaginary unit i (√-1)</source>
        <translation type="vanished">Imaginäre Einheit i (√-1)</translation>
    </message>
    <message>
        <source>Manage units</source>
        <translation type="vanished">Einheiten verwalten</translation>
    </message>
    <message>
        <source>Conversion operator</source>
        <translation type="vanished">Umrechnungsoperator</translation>
    </message>
    <message>
        <source>Kilogram</source>
        <translation type="vanished">Kilogramm</translation>
    </message>
    <message>
        <source>Two&apos;s complement input</source>
        <translation type="vanished">Zweierkomplement Eingabe</translation>
    </message>
    <message>
        <source>Two&apos;s complement output</source>
        <translation type="vanished">Zweierkomplement-Ausgabe</translation>
    </message>
    <message>
        <source>Bitwise Exclusive OR</source>
        <translation type="vanished">Bitweise Exklusiv-ODER</translation>
    </message>
    <message>
        <source>Bitwise Left Shift</source>
        <translation type="vanished">Bitweise Linksverschiebung</translation>
    </message>
    <message>
        <source>Bitwise Right Shift</source>
        <translation type="vanished">Bitweise Rechtsverschiebung</translation>
    </message>
    <message>
        <source>Floating point conversion</source>
        <translation type="vanished">Gleitkomma-Konvertierung</translation>
    </message>
    <message>
        <source>Show/hide left keypad</source>
        <translation type="vanished">Linkes Tastenfeld ein-/ausblenden</translation>
    </message>
    <message>
        <source>Show/hide right keypad</source>
        <translation type="vanished">Rechtes Tastenfeld ein-/ausblenden</translation>
    </message>
    <message>
        <source>DEL</source>
        <extracomment>Standard calculator button. Do not use more than three characters.</extracomment>
        <translation type="vanished">DEL</translation>
    </message>
    <message>
        <source>AC</source>
        <extracomment>Standard calculator button. Do not use more than three characters.</extracomment>
        <translation type="vanished">AC</translation>
    </message>
    <message>
        <source>Previous result</source>
        <translation type="vanished">Vorheriges Ergebnis</translation>
    </message>
    <message>
        <source>ANS</source>
        <extracomment>Standard calculator button. Do not use more than three characters.</extracomment>
        <translation type="vanished">ANS</translation>
    </message>
    <message>
        <source>EXP</source>
        <translation type="vanished">EXP</translation>
    </message>
    <message>
        <source>Add to Expression</source>
        <translation type="vanished">Zum Ausdruck hinzufügen</translation>
    </message>
    <message>
        <source>Persistent Keypad</source>
        <translation type="vanished">Beständiges Tastenfeld</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Bearbeiten</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Löschen</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Aktualisieren</translation>
    </message>
    <message>
        <source>Insert Value</source>
        <translation type="vanished">Wert einfügen</translation>
    </message>
    <message>
        <source>Insert Text</source>
        <translation type="vanished">Text einfügen</translation>
    </message>
    <message>
        <source>Insert Parsed Text</source>
        <translation type="vanished">Analysierten Text einfügen</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Kopieren</translation>
    </message>
    <message>
        <source>Copy Full Text</source>
        <translation type="vanished">Vollständigen Text kopieren</translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="vanished">Suchen...</translation>
    </message>
    <message>
        <source>Add Bookmark…</source>
        <translation type="vanished">Lesezeichen hinzufügen...</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation type="vanished">Lesezeichen</translation>
    </message>
    <message>
        <source>Protect</source>
        <translation type="vanished">Schützen</translation>
    </message>
    <message>
        <source>Move To Top</source>
        <translation type="vanished">Nach oben verschieben</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Entfernen</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="vanished">Alles löschen</translation>
    </message>
    <message>
        <source>_Copy</source>
        <translation type="vanished">_Kopieren</translation>
    </message>
    <message>
        <source>_Store…</source>
        <translation type="vanished">_Sichern...</translation>
    </message>
    <message>
        <source>Save Image…</source>
        <translation type="vanished">Bild speichern...</translation>
    </message>
    <message>
        <source>_Factorize</source>
        <translation type="vanished">_Faktorisieren</translation>
    </message>
    <message>
        <source>_Expand</source>
        <translation type="vanished">_Expandieren</translation>
    </message>
    <message>
        <source>_Normal</source>
        <translation type="vanished">_Normal</translation>
    </message>
    <message>
        <source>Sc_ientific</source>
        <translation type="vanished">Wi_ssenschaftlich</translation>
    </message>
    <message>
        <source>Purel_y Scientific</source>
        <translation type="vanished">Rei_n Wissenschaftlich</translation>
    </message>
    <message>
        <source>Simp_le</source>
        <translation type="vanished">Simp_le</translation>
    </message>
    <message>
        <source>_Binary</source>
        <translation type="vanished">_Binär</translation>
    </message>
    <message>
        <source>_Octal</source>
        <translation type="vanished">_Oktal</translation>
    </message>
    <message>
        <source>_Decimal</source>
        <translation type="vanished">_Dezimal</translation>
    </message>
    <message>
        <source>_Hexadecimal</source>
        <translation type="vanished">_Hexadezimal</translation>
    </message>
    <message>
        <source>Decimal Fraction</source>
        <translation type="vanished">Dezimalbruch</translation>
    </message>
    <message>
        <source>Exact Decimal Fraction</source>
        <translation type="vanished">Exakter Dezimalbruch</translation>
    </message>
    <message>
        <source>Simple Fraction</source>
        <translation type="vanished">Einfacher Bruch</translation>
    </message>
    <message>
        <source>Mixed Fraction</source>
        <translation type="vanished">Gemischter Bruch</translation>
    </message>
    <message>
        <source>_Abbreviate Names</source>
        <translation type="vanished">_Namen abkürzen</translation>
    </message>
    <message>
        <source>C_onvert…</source>
        <translation type="vanished">U_mrechnen...</translation>
    </message>
    <message>
        <source>Convert to Base _Units</source>
        <translation type="vanished">Umrechnen in Basis _Einheiten</translation>
    </message>
    <message>
        <source>Convert _to Optimal Unit</source>
        <translation type="vanished">Umrechnen _in optimale Einheit</translation>
    </message>
    <message>
        <source>Use Optimal Prefix</source>
        <translation type="vanished">Optimalen Präfix verwenden</translation>
    </message>
    <message>
        <source>Convert to</source>
        <translation type="vanished">Konvertiere in</translation>
    </message>
    <message>
        <source>Convert to UTC</source>
        <translation type="vanished">Konvertiere in UTC</translation>
    </message>
    <message>
        <source>Convert to Calendars</source>
        <translation type="vanished">Konvertiere in Kalender</translation>
    </message>
    <message>
        <source>Use prefixes for all units</source>
        <translation type="vanished">Präfixe für alle Einheiten verwenden</translation>
    </message>
    <message>
        <source>Enable All SI Prefi_xes</source>
        <translation type="vanished">Alle SI-Präfi_xe aktivieren</translation>
    </message>
    <message>
        <source>View/Edit Matrix</source>
        <translation type="vanished">Matrix anzeigen/bearbeiten</translation>
    </message>
    <message>
        <source>View/Edit Vector</source>
        <translation type="vanished">Vektor anzeigen/bearbeiten</translation>
    </message>
    <message>
        <source>Copy Text</source>
        <translation type="vanished">Text kopieren</translation>
    </message>
    <message>
        <source>To Top</source>
        <translation type="vanished">Nach oben</translation>
    </message>
    <message>
        <source>Swap</source>
        <translation type="vanished">Tauschen</translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="vanished">Hoch</translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="vanished">Runter</translation>
    </message>
    <message>
        <source>Negate</source>
        <translation type="vanished">Negieren</translation>
    </message>
    <message>
        <source>Invert</source>
        <translation type="vanished">Invertieren</translation>
    </message>
    <message>
        <source>Square</source>
        <translation type="vanished">Quadratisch</translation>
    </message>
    <message>
        <source>Square Root</source>
        <translation type="vanished">Quadratische Wurzel</translation>
    </message>
    <message>
        <source>Clear Stack</source>
        <translation type="vanished">Stapel löschen</translation>
    </message>
    <message>
        <source>Select Number Base…</source>
        <translation type="vanished">Zahlenbasis wählen...</translation>
    </message>
    <message>
        <source>Store result</source>
        <translation type="vanished">Ergebnis speichern</translation>
    </message>
    <message>
        <source>Add result</source>
        <extracomment>Add current result to variable value</extracomment>
        <translation type="vanished">Ergebnis addieren</translation>
    </message>
    <message>
        <source>Subtract result</source>
        <extracomment>Subtruct current result from variable value</extracomment>
        <translation type="vanished">Ergebnis subtrahieren</translation>
    </message>
    <message>
        <source>Insert the matrix/vector into the expression</source>
        <translation type="vanished">Einfügen der Matrix/des Vektors in den Ausdruck</translation>
    </message>
    <message>
        <source>Rows</source>
        <translation type="vanished">Zeilen</translation>
    </message>
    <message>
        <source>Number of rows in this matrix (rows displayed for vectors)</source>
        <translation type="vanished">Anzahl der Zeilen in dieser Matrix (bei Vektoren werden die Zeilen angezeigt)</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="vanished">Spalten</translation>
    </message>
    <message>
        <source>Number of columns in this matrix (columns displayed for vectors)</source>
        <translation type="vanished">Anzahl der Spalten in dieser Matrix (bei Vektoren werden die Spalten angezeigt)</translation>
    </message>
    <message>
        <source>If this is a matrix or vector</source>
        <translation type="vanished">Wenn dies eine Matrix oder ein Vektor ist</translation>
    </message>
    <message>
        <source>Elements</source>
        <translation type="vanished">Elemente</translation>
    </message>
    <message>
        <source>Current element:</source>
        <translation type="vanished">Aktuelles Element:</translation>
    </message>
    <message>
        <source>Edit Matrix</source>
        <translation type="vanished">Matrix bearbeiten</translation>
    </message>
    <message>
        <source>Do not create/modify this matrix/vector</source>
        <translation type="vanished">Diese Matrix/Vektor nicht erstellen/verändern</translation>
    </message>
    <message>
        <source>Create/modify the matrix/vector</source>
        <translation type="vanished">Matrix/Vektor erstellen/verändern</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this matrix/vector</source>
        <translation type="vanished">Anlegen/Ändern dieser Matrix/des Vektors übernehmen</translation>
    </message>
    <message>
        <source>Name used to reference this variable in expressions</source>
        <translation type="vanished">Name, der verwendet wird, um diese Variable in Ausdrücken zu referenzieren</translation>
    </message>
    <message>
        <source>Names</source>
        <translation type="vanished">Namen</translation>
    </message>
    <message>
        <source>Add new name</source>
        <translation type="vanished">Neuen Namen hinzufügen</translation>
    </message>
    <message>
        <source>Apply changes to the selected name</source>
        <translation type="vanished">Änderungen auf den ausgewählten Namen anwenden</translation>
    </message>
    <message>
        <source>Remove the selected name</source>
        <translation type="vanished">Den ausgewählten Namen entfernen</translation>
    </message>
    <message>
        <source>Abbreviation</source>
        <translation type="vanished">Abkürzung</translation>
    </message>
    <message>
        <source>Unicode</source>
        <translation type="vanished">Unicode</translation>
    </message>
    <message>
        <source>Plural</source>
        <translation type="vanished">Plural</translation>
    </message>
    <message>
        <source>Suffix</source>
        <translation type="vanished">Nachsilbe</translation>
    </message>
    <message>
        <source>Reference</source>
        <translation type="vanished">Referenz</translation>
    </message>
    <message>
        <source>Avoid input</source>
        <translation type="vanished">Eingabe vermeiden</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation type="vanished">Groß-/Kleinschreibung beachten</translation>
    </message>
    <message>
        <source>Completion only</source>
        <translation type="vanished">Nur Vervollständigung</translation>
    </message>
    <message>
        <source>Number Bases</source>
        <translation type="vanished">Zahlen-Basen</translation>
    </message>
    <message>
        <source>Roman numerals</source>
        <translation type="vanished">Römische Ziffern</translation>
    </message>
    <message>
        <source>BIN</source>
        <translation type="vanished">BIN</translation>
    </message>
    <message>
        <source>OCT</source>
        <translation type="vanished">OKT</translation>
    </message>
    <message>
        <source>DEC</source>
        <translation type="vanished">DEZ</translation>
    </message>
    <message>
        <source>DUO</source>
        <translation type="vanished">DUO</translation>
    </message>
    <message>
        <source>HEX</source>
        <translation type="vanished">HEX</translation>
    </message>
    <message>
        <source>ROM</source>
        <translation type="vanished">ROM</translation>
    </message>
    <message>
        <source>Subtract</source>
        <translation type="vanished">Subtrahieren</translation>
    </message>
    <message>
        <source>Multiply</source>
        <translation type="vanished">Multiplizieren</translation>
    </message>
    <message>
        <source>Divide</source>
        <translation type="vanished">Dividieren</translation>
    </message>
    <message>
        <source>Bitwise AND</source>
        <translation type="vanished">Bitweise UND</translation>
    </message>
    <message>
        <source>Bitwise OR</source>
        <translation type="vanished">Bitweises ODER</translation>
    </message>
    <message>
        <source>Bitwise NOT</source>
        <translation type="vanished">Bitweises NICHT</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Löschen</translation>
    </message>
    <message>
        <source>Enter two values, of which at most one is a percentage, and the others will be calculated for you.</source>
        <translation type="vanished">Geben Sie zwei Werte ein, von denen höchstens einer ein Prozentwert ist, und die anderen werden für Sie berechnet.</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation type="vanished">Prozentsatz</translation>
    </message>
    <message>
        <source>Calculate</source>
        <translation type="vanished">Berechnen</translation>
    </message>
    <message>
        <source>Value 1</source>
        <translation type="vanished">Wert 1</translation>
    </message>
    <message>
        <source>2 compared to 1</source>
        <translation type="vanished">2 im Vergleich zu 1</translation>
    </message>
    <message>
        <source>Change from 1 to 2</source>
        <translation type="vanished">Änderung von 1 auf 2</translation>
    </message>
    <message>
        <source>Value 2</source>
        <translation type="vanished">Wert 2</translation>
    </message>
    <message>
        <source>1 compared to 2</source>
        <translation type="vanished">1 im Vergleich zu 2</translation>
    </message>
    <message>
        <source>Change from 2 to 1</source>
        <translation type="vanished">Änderung von 2 auf 1</translation>
    </message>
    <message>
        <source>Plot</source>
        <translation type="vanished">Plotten</translation>
    </message>
    <message>
        <source>_Save</source>
        <translation type="vanished">_Speichern</translation>
    </message>
    <message>
        <source>Save as png, svg, postscript, eps, latex or fig</source>
        <translation type="vanished">Speichern als png, svg, postscript, eps, latex oder fig</translation>
    </message>
    <message>
        <source>Vector/matrix</source>
        <translation type="vanished">Vektor/Matrix</translation>
    </message>
    <message>
        <source>Paired matrix</source>
        <translation type="vanished">Gepaarte Matrix</translation>
    </message>
    <message>
        <source>if you want to split matrix in rows instead of columns</source>
        <translation type="vanished">wenn Sie die Matrix in Zeilen statt in Spalten aufteilen wollen</translation>
    </message>
    <message>
        <source>X variable</source>
        <translation type="vanished">X Variable</translation>
    </message>
    <message>
        <source>The variable name used in expression</source>
        <translation type="vanished">Der Name der Variable, die im Ausdruck verwendet wird</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="vanished">Stil</translation>
    </message>
    <message>
        <source>Line</source>
        <translation type="vanished">Zeile</translation>
    </message>
    <message>
        <source>Points</source>
        <translation type="vanished">Punkte</translation>
    </message>
    <message>
        <source>Line with points</source>
        <translation type="vanished">Linie mit Punkten</translation>
    </message>
    <message>
        <source>Boxes/bars</source>
        <translation type="vanished">Boxen/Balken</translation>
    </message>
    <message>
        <source>Histogram</source>
        <translation type="vanished">Histogramm</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation type="vanished">Stufen</translation>
    </message>
    <message>
        <source>Candlesticks</source>
        <translation type="vanished">Kerzenständer</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation type="vanished">Punkte</translation>
    </message>
    <message>
        <source>Smoothing</source>
        <translation type="vanished">Glättung</translation>
    </message>
    <message>
        <source>Monotonic</source>
        <translation type="vanished">Monoton</translation>
    </message>
    <message>
        <source>Natural cubic splines</source>
        <translation type="vanished">Natürliche kubische Splines</translation>
    </message>
    <message>
        <source>Bezier</source>
        <translation type="vanished">Bézier</translation>
    </message>
    <message>
        <source>Bezier (monotonic)</source>
        <translation type="vanished">Bézier (monoton)</translation>
    </message>
    <message>
        <source>Y-axis</source>
        <translation type="vanished">Y-Achse</translation>
    </message>
    <message>
        <source>Primary</source>
        <translation type="vanished">Primär</translation>
    </message>
    <message>
        <source>Secondary</source>
        <translation type="vanished">Sekundär</translation>
    </message>
    <message>
        <source>_Remove</source>
        <translation type="vanished">_Entfernen</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="vanished">Daten</translation>
    </message>
    <message>
        <source>Minimum x value</source>
        <translation type="vanished">Minimaler x-Wert</translation>
    </message>
    <message>
        <source>Maximum x value</source>
        <translation type="vanished">Maximaler x-Wert</translation>
    </message>
    <message>
        <source>Sampling rate</source>
        <translation type="vanished">Abtastrate</translation>
    </message>
    <message>
        <source>Step size</source>
        <translation type="vanished">Schrittweite</translation>
    </message>
    <message>
        <source>Function Range</source>
        <translation type="vanished">Funktionsbereich</translation>
    </message>
    <message>
        <source>Display grid</source>
        <translation type="vanished">Raster anzeigen</translation>
    </message>
    <message>
        <source>Display full border</source>
        <translation type="vanished">Vollen Rand anzeigen</translation>
    </message>
    <message>
        <source>Minimum y value</source>
        <translation type="vanished">Minimaler y-Wert</translation>
    </message>
    <message>
        <source>Maximum y value</source>
        <translation type="vanished">Maximaler y-Wert</translation>
    </message>
    <message>
        <source>Logarithmic x scale</source>
        <translation type="vanished">Logarithmische x-Skala</translation>
    </message>
    <message>
        <source>Logarithmic y scale</source>
        <translation type="vanished">Logarithmische y-Skala</translation>
    </message>
    <message>
        <source>X-axis label</source>
        <translation type="vanished">X-Achsen-Beschriftung</translation>
    </message>
    <message>
        <source>Y-axis label</source>
        <translation type="vanished">Y-Achsen-Beschriftung</translation>
    </message>
    <message>
        <source>Line width</source>
        <translation type="vanished">Linienbreite</translation>
    </message>
    <message>
        <source>Color display</source>
        <translation type="vanished">Farbdarstellung</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Monochrome</source>
        <translation type="vanished">Monochrom</translation>
    </message>
    <message>
        <source>Legend placement</source>
        <translation type="vanished">Platzierung der Legende</translation>
    </message>
    <message>
        <source>Top-left</source>
        <translation type="vanished">Oben-links</translation>
    </message>
    <message>
        <source>Top-right</source>
        <translation type="vanished">Oben-rechts</translation>
    </message>
    <message>
        <source>Bottom-left</source>
        <translation type="vanished">Unten-links</translation>
    </message>
    <message>
        <source>Bottom-right</source>
        <translation type="vanished">Unten-rechts</translation>
    </message>
    <message>
        <source>Below</source>
        <translation type="vanished">Unterhalb</translation>
    </message>
    <message>
        <source>Outside</source>
        <translation type="vanished">Außerhalb</translation>
    </message>
    <message>
        <source>Appearance</source>
        <translation type="vanished">Erscheinungsbild</translation>
    </message>
    <message>
        <source>Precision</source>
        <translation type="vanished">Genauigkeit</translation>
    </message>
    <message>
        <source>_Recalculate</source>
        <translation type="vanished">_Neuberechnen</translation>
    </message>
    <message>
        <source>Recalculate expression</source>
        <translation type="vanished">Ausdruck neu berechnen</translation>
    </message>
    <message>
        <source>The number of significant digits to display/calculate (simple arithmetics are always calculated exact)</source>
        <translation type="vanished">Die Anzahl der signifikanten Stellen, die angezeigt/berechnet werden sollen (einfache Arithmetik wird immer exakt berechnet)</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Save mode on exit</source>
        <translation type="vanished">Modus beim Beenden speichern</translation>
    </message>
    <message>
        <source>If the mode of the calculator shall be restored</source>
        <translation type="vanished">Wenn der Modus des Taschenrechners wiederhergestellt werden soll</translation>
    </message>
    <message>
        <source>Save definitions on exit</source>
        <translation type="vanished">Definitionen beim Beenden speichern</translation>
    </message>
    <message>
        <source>If changes to functions, units and variables shall be saved automatically</source>
        <translation type="vanished">Wenn Änderungen an Funktionen, Einheiten und Variablen automatisch gespeichert werden sollen</translation>
    </message>
    <message>
        <source>Clear history on exit</source>
        <translation type="vanished">Verlauf beim Beenden löschen</translation>
    </message>
    <message>
        <source>Allow multiple instances</source>
        <translation type="vanished">Mehrere Instanzen zulassen</translation>
    </message>
    <message>
        <source>Allow multiple instances of the Qalculate! main window to be open at the same time. 

Note that only the mode, history and definitions of the last closed instance will be saved.</source>
        <translation type="vanished">Erlaubt, dass mehrere Instanzen des Hauptfensters von Qalculate! gleichzeitig geöffnet sein können. 

Beachten Sie, dass nur der Modus, der Verlauf und die Definitionen der zuletzt geschlossenen Instanz gespeichert werden.</translation>
    </message>
    <message>
        <source>Notify when a new version is available</source>
        <translation type="vanished">Benachrichtigen, wenn eine neue Version verfügbar ist</translation>
    </message>
    <message>
        <source>Use keyboard keys for RPN</source>
        <translation type="vanished">Tasten auf der Tastatur für RPN verwenden</translation>
    </message>
    <message>
        <source>Use keyboard operator keys for RPN operations (+-*/^).</source>
        <translation type="vanished">Tastaturoperatortasten für RPN-Operationen verwenden (+-*/^).</translation>
    </message>
    <message>
        <source>Use caret for bitwise XOR</source>
        <translation type="vanished">Einfügezeichen für bitweises XOR verwenden</translation>
    </message>
    <message>
        <source>Input XOR (⊻) using caret (^) on keyboard (otherwise use Ctrl+^). The exponentiation operator (^) can always be input using Ctrl+*.</source>
        <translation type="vanished">Eingabe von XOR (⊻) mit Einfügezeichen (^) auf der Tastatur (sonst mit Strg+^). Der Potenzierungsoperator (^) kann immer mit Strg+* eingegeben werden.</translation>
    </message>
    <message>
        <source>Add calculate-as-you-type result to history</source>
        <translation type="vanished">Berechne-während-du-tippst-Ergebnis zur Historie hinzufügen</translation>
    </message>
    <message>
        <source>Delay:</source>
        <translation type="vanished">Verzögern:</translation>
    </message>
    <message>
        <source>Time limit for plot:</source>
        <translation type="vanished">Zeitlimit für Plot:</translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation type="vanished">Verhalten</translation>
    </message>
    <message>
        <source>Enable Unicode symbols</source>
        <translation type="vanished">Unicode-Symbole einschalten</translation>
    </message>
    <message>
        <source>Disable this if you have problems with some fancy characters</source>
        <translation type="vanished">Deaktivieren Sie dies, wenn Sie Probleme mit einigen ausgefallenen Zeichen haben</translation>
    </message>
    <message>
        <source>Ignore system language (requires restart)</source>
        <translation type="vanished">Systemsprache ignorieren (erfordert Neustart)</translation>
    </message>
    <message>
        <source>Use system tray icon</source>
        <translation type="vanished">Systemtray-Symbol verwenden</translation>
    </message>
    <message>
        <source>Hides the application in the system tray when the main window is closed</source>
        <translation type="vanished">Versteckt die Anwendung in der Taskleiste, wenn das Hauptfenster geschlossen wird</translation>
    </message>
    <message>
        <source>Hide on startup</source>
        <translation type="vanished">Beim Starten ausblenden</translation>
    </message>
    <message>
        <source>Remember window position</source>
        <translation type="vanished">Fensterposition merken</translation>
    </message>
    <message>
        <source>Application name</source>
        <translation type="vanished">Name der Anwendung</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="vanished">Ergebnis</translation>
    </message>
    <message>
        <source>Application name + result</source>
        <translation type="vanished">Anwendungsname + Ergebnis</translation>
    </message>
    <message>
        <source>Mode</source>
        <translation type="vanished">Modus</translation>
    </message>
    <message>
        <source>Application name + mode</source>
        <translation type="vanished">Anwendungsname + Modus</translation>
    </message>
    <message>
        <source>Window title</source>
        <translation type="vanished">Fenstertitel</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">Standard</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">Hell</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">Dunkel</translation>
    </message>
    <message>
        <source>High contrast</source>
        <translation type="vanished">Hoher Kontrast</translation>
    </message>
    <message>
        <source>Dark high contrast</source>
        <translation type="vanished">Dunkel Hochkontrast</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Design</translation>
    </message>
    <message>
        <source>Button padding</source>
        <translation type="vanished">Schaltflächen Abstand</translation>
    </message>
    <message>
        <source>/</source>
        <translation type="vanished">/</translation>
    </message>
    <message>
        <source>Number of expression lines</source>
        <translation type="vanished">Anzahl der Ausdruckszeilen</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="vanished">3</translation>
    </message>
    <message>
        <source>Display expression status</source>
        <translation type="vanished">Anzeige des Ausdrucksstatus</translation>
    </message>
    <message>
        <source>If as-you-type expression status shall be displayed below the expression entry</source>
        <translation type="vanished">Wenn der Status des Ausdrucks &quot;während-du-tippst&quot; unterhalb des Ausdruckseintrags angezeigt werden soll</translation>
    </message>
    <message>
        <source>Persistent keypad</source>
        <translation type="vanished">Beständiges tastenfeld</translation>
    </message>
    <message>
        <source>Look &amp; Feel</source>
        <translation type="vanished">Aussehen &amp; Bedienung</translation>
    </message>
    <message>
        <source>Binary two&apos;s complement representation</source>
        <translation type="vanished">Binäre Zweierkomplement-Darstellung</translation>
    </message>
    <message>
        <source>If two&apos;s complement representation shall be used for negative binary numbers.</source>
        <translation type="vanished">Ob die Zweierkomplement-Darstellung für negative Binärzahlen verwendet werden soll.</translation>
    </message>
    <message>
        <source>Hexadecimal two&apos;s complement representation</source>
        <translation type="vanished">Hexadezimale Zweierkomplement-Darstellung</translation>
    </message>
    <message>
        <source>If two&apos;s complement representation shall be used for negative hexadecimal numbers.</source>
        <translation type="vanished">Wenn die Zweierkomplement-Darstellung für negative Hexadezimalzahlen verwendet werden soll.</translation>
    </message>
    <message>
        <source>Use lower case letters in non-decimal numbers</source>
        <translation type="vanished">Kleinbuchstaben in Zahlen mit nicht-dezimaler Basis verwenden</translation>
    </message>
    <message>
        <source>If lower case letters should be used in numbers with non-decimal base</source>
        <translation type="vanished">Wenn Kleinbuchstaben in Zahlen mit nicht dezimaler Basis verwendet werden sollen</translation>
    </message>
    <message>
        <source>Alternative base prefixes</source>
        <translation type="vanished">Alternative Basis-Präfixe</translation>
    </message>
    <message>
        <source>If hexadecimal numbers shall be displayed with &quot;0x0&quot; and binary numbers with &quot;0b00&quot; as prefixes</source>
        <translation type="vanished">Wenn hexadezimale Zahlen mit &quot;0x0&quot; und binäre Zahlen mit &quot;0b00&quot; als Präfixe dargestellt werden sollen</translation>
    </message>
    <message>
        <source>Spell out logical operators</source>
        <translation type="vanished">Logische Operatoren ausbuchstabieren</translation>
    </message>
    <message>
        <source>If logical and/or shall be displayed as &quot;&amp;&amp;&quot;/&quot;||&quot; or &quot;and&quot;/&quot;or&quot;</source>
        <translation type="vanished">Wenn logisch und/oder als &quot;&amp;&amp;&quot;/&quot;||&quot; oder &quot;and&quot;/&quot;or&quot; dargestellt werden soll</translation>
    </message>
    <message>
        <source>If &quot;e&quot; shall be used instead of &quot;E&quot; in numbers</source>
        <translation type="vanished">Wenn &quot;e&quot; statt &quot;E&quot; in Zahlen verwendet werden soll</translation>
    </message>
    <message>
        <source>Use E-notation instead of 10&lt;sup&gt;&lt;i&gt;n&lt;/i&gt;&lt;/sup&gt;</source>
        <translation type="vanished">E-Notation anstelle von 10&lt;sup&gt;&lt;i&gt;n&lt;/i&gt;&lt;/sup&gt; verwenden</translation>
    </message>
    <message>
        <source>Use lower case &quot;e&quot; (as in 1e10)</source>
        <translation type="vanished">Kleinschreibung von &quot;e&quot; verwenden (wie in 1e10)</translation>
    </message>
    <message>
        <source>Use &apos;j&apos; as imaginary unit</source>
        <translation type="vanished">&apos;j&apos; als imaginäre Einheit verwenden</translation>
    </message>
    <message>
        <source>Use &apos;j&apos; (instead of &apos;i&apos;) as default symbol for the imaginary unit, and place it in front the imaginary part.</source>
        <translation type="vanished">Verwenden Sie &apos;j&apos; (anstelle von &apos;i&apos;) als Standardsymbol für die imaginäre Einheit, und platzieren Sie es vor dem Imaginärteil.</translation>
    </message>
    <message>
        <source>Use comma as decimal separator</source>
        <translation type="vanished">Komma als Dezimaltrennzeichen verwenden</translation>
    </message>
    <message>
        <source>Allow dots, &apos;.&apos;,  to be used as thousands separator instead of as an alternative decimal sign</source>
        <translation type="vanished">Punkt, &apos;.&apos;, als Tausendertrennzeichen anstelle eines alternativen Dezimalzeichens zulassen</translation>
    </message>
    <message>
        <source>Ignore comma in numbers</source>
        <translation type="vanished">Komma in Zahlen ignorieren</translation>
    </message>
    <message>
        <source>Allow commas, &apos;,&apos;,  to be used as thousands separator instead of as an function argument separator</source>
        <translation type="vanished">Erlaubt die Verwendung von Kommas, &apos;,&apos;, als Tausendertrennzeichen statt als Funktionsargumenttrennzeichen</translation>
    </message>
    <message>
        <source>Ignore dots in numbers</source>
        <translation type="vanished">Punkte in Zahlen ignorieren</translation>
    </message>
    <message>
        <source>Digit grouping</source>
        <translation type="vanished">Zifferngruppierung</translation>
    </message>
    <message>
        <source>off</source>
        <translation type="vanished">aus</translation>
    </message>
    <message>
        <source>standard</source>
        <translation type="vanished">Standard</translation>
    </message>
    <message>
        <source>local</source>
        <translation type="vanished">lokal</translation>
    </message>
    <message>
        <source>Multiplication sign</source>
        <translation type="vanished">Multiplikationszeichen</translation>
    </message>
    <message>
        <source>Division sign</source>
        <translation type="vanished">Divisionszeichen</translation>
    </message>
    <message>
        <source>Copy digit separator</source>
        <translation type="vanished">Zifferntrennzeichen kopieren</translation>
    </message>
    <message>
        <source>Deactivate to remove digit separator when copying result</source>
        <translation type="vanished">Deaktivieren, um Zifferntrennzeichen beim Kopieren des Ergebnisses zu entfernen</translation>
    </message>
    <message>
        <source>Numbers &amp; Operators</source>
        <translation type="vanished">Zahlen &amp; Operatoren</translation>
    </message>
    <message>
        <source>Use binary prefixes for information units</source>
        <translation type="vanished">Binäre Präfixe für Informationseinheiten verwenden</translation>
    </message>
    <message>
        <source>Use binary, instead of decimal, prefixes by default for information units (e.g. bytes).</source>
        <translation type="vanished">Verwenden Sie standardmäßig binäre, statt dezimale Präfixe für Informationseinheiten (z.B. Bytes).</translation>
    </message>
    <message>
        <source>Conversion to local currency</source>
        <translation type="vanished">Umrechnung in Landeswährung</translation>
    </message>
    <message>
        <source>Automatically convert to the local currency when optimal unit conversion is activated.</source>
        <translation type="vanished">Konvertieren Sie automatisch in die lokale Währung, wenn die optimale Einheitenumrechnung aktiviert ist.</translation>
    </message>
    <message>
        <source>Update exchange rates on start</source>
        <translation type="vanished">Wechselkurse beim Start aktualisieren</translation>
    </message>
    <message>
        <source>If current exchange rates shall be downloaded from the internet at program start</source>
        <translation type="vanished">Wenn die aktuellen Wechselkurse beim Programmstart aus dem Internet geladen werden sollen</translation>
    </message>
    <message>
        <source>Exchange rates updates</source>
        <translation type="vanished">Wechselkurse aktualisieren</translation>
    </message>
    <message>
        <source>Temperature calculation mode:</source>
        <translation type="vanished">Temperatur-Berechnungsmodus:</translation>
    </message>
    <message>
        <source>Absolute</source>
        <translation type="vanished">Absolut</translation>
    </message>
    <message>
        <source>Relative</source>
        <translation type="vanished">Relativ</translation>
    </message>
    <message>
        <source>Hybrid</source>
        <translation type="vanished">Hybrid</translation>
    </message>
    <message>
        <source>Units &amp; Currencies</source>
        <translation type="vanished">Einheiten &amp; Währungen</translation>
    </message>
    <message>
        <source>Show expression completion suggestions</source>
        <translation type="vanished">Vorschläge zur Vervollständigung von Ausdrücken anzeigen</translation>
    </message>
    <message>
        <source>Search titles and countries</source>
        <translation type="vanished">Titel und Länder suchen</translation>
    </message>
    <message>
        <source>Minimum characters</source>
        <translation type="vanished">Minimale Zeichen</translation>
    </message>
    <message>
        <source>Popup delay (ms)</source>
        <translation type="vanished">Popup-Verzögerung (ms)</translation>
    </message>
    <message>
        <source>Completion</source>
        <translation type="vanished">Vervollständigung</translation>
    </message>
    <message>
        <source>Status warning color</source>
        <translation type="vanished">Farbe der Status-Warnung</translation>
    </message>
    <message>
        <source>Status error color</source>
        <translation type="vanished">Farbe für Status-Fehler</translation>
    </message>
    <message>
        <source>Custom status font</source>
        <translation type="vanished">Benutzerdefinierte Schriftart für den Status</translation>
    </message>
    <message>
        <source>If you want to use a font other than the default in the status display below the expression entry</source>
        <translation type="vanished">Wenn Sie in der Statusanzeige unter dem Ausdruckseintrag eine andere als die Standardschriftart verwenden möchten</translation>
    </message>
    <message>
        <source>Custom expression font</source>
        <translation type="vanished">Benutzerdefinierte Ausdrucksschriftart</translation>
    </message>
    <message>
        <source>If you want to use a font other than the default in the expression entry</source>
        <translation type="vanished">Wenn Sie in der Ausdruckseingabe eine andere als die Standardschriftart verwenden möchten</translation>
    </message>
    <message>
        <source>Custom result font</source>
        <translation type="vanished">Benutzerdefinierte Ergebnisschriftart</translation>
    </message>
    <message>
        <source>If you want to use a font other than the default in the result display</source>
        <translation type="vanished">Wenn Sie in der Ergebnisanzeige eine andere Schriftart als die Standardschriftart verwenden möchten</translation>
    </message>
    <message>
        <source>Custom keypad font</source>
        <translation type="vanished">Benutzerdefinierte Tastenfeldschriftart</translation>
    </message>
    <message>
        <source>If you want to use a font other than the default in the keypad</source>
        <translation type="vanished">Wenn Sie eine andere Schriftart als die Standardschriftart im Tastenfeld verwenden möchten</translation>
    </message>
    <message>
        <source>Custom application font</source>
        <translation type="vanished">Benutzerdefinierte Anwendungsschriftart</translation>
    </message>
    <message>
        <source>If you want to use a font other than the default for the whole application</source>
        <translation type="vanished">Wenn Sie eine andere als die Standardschriftart für die gesamte Anwendung verwenden möchten</translation>
    </message>
    <message>
        <source>Text color</source>
        <translation type="vanished">Textfarbe</translation>
    </message>
    <message>
        <source>Fonts &amp; Colors</source>
        <translation type="vanished">Schriftarten und Farben</translation>
    </message>
    <message>
        <source>Other:</source>
        <translation type="vanished">Andere:</translation>
    </message>
    <message>
        <source>Bijective base-26</source>
        <translation type="vanished">Bijektive Basis-26</translation>
    </message>
    <message>
        <source>&lt;b&gt;Result Base&lt;/b&gt;</source>
        <translation type="vanished">&lt;b&gt;Ergebnisbasis&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Expression Base&lt;/b&gt;</source>
        <translation type="vanished">&lt;b&gt;Ausdrucksbasis&lt;/b&gt;</translation>
    </message>
    <message>
        <source>New Keyboard Shortcut</source>
        <translation type="vanished">Neues Tastaturkürzel</translation>
    </message>
    <message>
        <source>Edit Variable</source>
        <translation type="vanished">Variable bearbeiten</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Erweitert</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this variable</source>
        <translation type="vanished">Erstellen/Ändern dieser Variable übernehmen.</translation>
    </message>
    <message>
        <source>x, y, z</source>
        <translation type="vanished">x, y, z</translation>
    </message>
    <message>
        <source>Use x, y and z for 1st, 2nd and 3rd function argument, respectively.</source>
        <translation type="vanished">Verwenden Sie x, y und z jeweils für das erste, zweite und dritte Funktionsargument.</translation>
    </message>
    <message>
        <source>\x, \y, \z</source>
        <translation type="vanished">\x, \y, \z</translation>
    </message>
    <message>
        <source>Use \x, \y and \z for 1st, 2nd and 3rd function argument, respectively. This avoids potential conflicts with variables, functions and units.</source>
        <translation type="vanished">Verwenden Sie \x, \y und \z für das erste, zweite bzw. dritte Funktionsargument. Dadurch werden mögliche Konflikte mit Variablen, Funktionen und Einheiten vermieden.</translation>
    </message>
    <message>
        <source>Edit Unit</source>
        <translation type="vanished">Einheit bearbeiten</translation>
    </message>
    <message>
        <source>Do not create/modify this unit</source>
        <translation type="vanished">Diese Einheit nicht erstellen/verändern</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this unit</source>
        <translation type="vanished">Erstellen/Ändern dieser Einheit übernehmen.</translation>
    </message>
    <message>
        <source>Singular form of this unit&apos;s name</source>
        <translation type="vanished">Singularform des Einheitennamens</translation>
    </message>
    <message>
        <source>Title displayed in menus and in unit manager</source>
        <translation type="vanished">Titel wird in Menüs und im Einheitenmanager angezeigt</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">System</translation>
    </message>
    <message>
        <source>Imperial</source>
        <translation type="vanished">Imperial</translation>
    </message>
    <message>
        <source>US Survey</source>
        <translation type="vanished">US-Umfrage</translation>
    </message>
    <message>
        <source>Hide unit</source>
        <translation type="vanished">Einheit ausblenden</translation>
    </message>
    <message>
        <source>If this unit shall be hidden in menus</source>
        <translation type="vanished">Wenn diese Einheit in Menüs ausgeblendet werden soll</translation>
    </message>
    <message>
        <source>Class</source>
        <translation type="vanished">Klasse</translation>
    </message>
    <message>
        <source>The class that this unit belongs to. Named derived units are defined in relation to a single other unit, with an optional exponent, while (unnamed) derived units are defined by a unit expression with one or multiple units.</source>
        <translation type="vanished">Die Klasse, zu der diese Einheit gehört. Benannte abgeleitete Einheiten werden in Bezug auf eine einzelne andere Einheit definiert, mit einem optionalen Exponenten, während (unbenannte) abgeleitete Einheiten durch einen Einheitenausdruck mit einer oder mehreren Einheiten definiert werden.</translation>
    </message>
    <message>
        <source>Base unit</source>
        <translation type="vanished">Basiseinheit</translation>
    </message>
    <message>
        <source>Named derived unit</source>
        <translation type="vanished">Benannte abgeleitete Einheit</translation>
    </message>
    <message>
        <source>Derived unit</source>
        <translation type="vanished">Abgeleitete Einheit</translation>
    </message>
    <message>
        <source>Base unit(s)</source>
        <translation type="vanished">Basiseinheit(en)</translation>
    </message>
    <message>
        <source>Unit (for named derived unit) or unit expression (for unnamed derived unit) that this unit as defined in relation to</source>
        <translation type="vanished">Einheit (für benannte abgeleitete Einheit) oder Einheitenausdruck (für unbenannte abgeleitete Einheit), die diese Einheit in Bezug auf </translation>
    </message>
    <message>
        <source>Exponent</source>
        <translation type="vanished">Exponent</translation>
    </message>
    <message>
        <source>Exponent of the base unit</source>
        <translation type="vanished">Exponent der Basiseinheit</translation>
    </message>
    <message>
        <source>Relation</source>
        <translation type="vanished">Relation</translation>
    </message>
    <message>
        <source>Relation to the base unit. For linear relations this should just be a number.

For non-linear relations use \x for the factor and \y for the exponent (e.g. &quot;\x + 273.15&quot; for the relation between degrees Celsius and Kelvin).</source>
        <translation type="vanished">Relation zur Basiseinheit. Für lineare Beziehungen sollte dies einfach eine Zahl sein.

Für nicht lineare Beziehungen verwenden Sie \x für den Faktor und \y für den Exponenten (z.B. &quot;\x + 273,15&quot; für die Beziehung zwischen Grad Celsius und Kelvin).</translation>
    </message>
    <message>
        <source>Relation is exact</source>
        <translation type="vanished">Relation ist exakt</translation>
    </message>
    <message>
        <source>If the relation is precise</source>
        <translation type="vanished">Wenn die Relation exakt ist</translation>
    </message>
    <message>
        <source>Inverse relation</source>
        <translation type="vanished">Inverse Relation</translation>
    </message>
    <message>
        <source>Specify for non-linear relation, for conversion back to the base unit.</source>
        <translation type="vanished">Bei nicht-linearer Relation angeben, zur Umrechnung zurück in die Basiseinheit.</translation>
    </message>
    <message>
        <source>Mix with base unit</source>
        <translation type="vanished">Mit Basiseinheit mischen</translation>
    </message>
    <message>
        <source>- Decides which units the base unit is mixed with if multple options exist.
- The original unit will not be mixed with units with lower priority.
- A lower value means higher priority.</source>
        <translation type="vanished">- Entscheidet, mit welchen Einheiten die Basiseinheit gemischt wird, wenn mehrere Optionen vorhanden sind.
- Die ursprüngliche Einheit wird nicht mit Einheiten mit niedrigerer Priorität gemischt.
- Ein niedrigerer Wert bedeutet höhere Priorität.</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation type="vanished">Vorrangig</translation>
    </message>
    <message>
        <source>Minimum base unit number</source>
        <translation type="vanished">Minimale Nummer der Basiseinheit</translation>
    </message>
    <message>
        <source>Use with prefixes by default</source>
        <translation type="vanished">Standardmäßig mit Präfixen verwenden</translation>
    </message>
    <message>
        <source>Create a new unit</source>
        <translation type="vanished">Eine neue Einheit erstellen</translation>
    </message>
    <message>
        <source>Edit the selected unit</source>
        <translation type="vanished">Die ausgewählte Einheit bearbeiten</translation>
    </message>
    <message>
        <source>Insert the selected unit into the expression entry</source>
        <translation type="vanished">Einfügen der ausgewählten Einheit in den Ausdruckseintrag</translation>
    </message>
    <message>
        <source>C_onvert</source>
        <translation type="vanished">U_mrechnen</translation>
    </message>
    <message>
        <source>Convert the result to the selected unit</source>
        <translation type="vanished">Das Ergebnis in die ausgewählte Einheit umrechnen</translation>
    </message>
    <message>
        <source>Delete the selected unit</source>
        <translation type="vanished">Löschen der selektierten Einheit</translation>
    </message>
    <message>
        <source>(De)activate the selected unit</source>
        <translation type="vanished">Selektierte Einheit (de)aktivieren</translation>
    </message>
    <message>
        <source>_Unit</source>
        <translation type="vanished">_Einheit</translation>
    </message>
    <message>
        <source>Convert between units</source>
        <translation type="vanished">Zwischen Einheiten umrechnen</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>Conver_sion</source>
        <translation type="vanished">Umrech_nung</translation>
    </message>
    <message>
        <source>Converted value</source>
        <translation type="vanished">Umgerechneter Wert</translation>
    </message>
    <message>
        <source>Value to convert from</source>
        <translation type="vanished">Umzurechnender Wert von</translation>
    </message>
    <message>
        <source>Type anywhere</source>
        <translation type="vanished">Eingabe irgendwo</translation>
    </message>
    <message>
        <source>Edit Unknown Variable</source>
        <translation type="vanished">Unbekannte Variable bearbeiten</translation>
    </message>
    <message>
        <source>Do not create/modify this unknown variable</source>
        <translation type="vanished">Diese unbekannte Variable nicht erstellen/verändern</translation>
    </message>
    <message>
        <source>Accept the creation/modification of this unknown variable</source>
        <translation type="vanished">Erstellung/Änderung dieser unbekannten Variable übernehmen</translation>
    </message>
    <message>
        <source>Name used to reference this unknown variable in expressions</source>
        <translation type="vanished">Name, der verwendet wird, um diese unbekannte Variable in Ausdrücken zu referenzieren</translation>
    </message>
    <message>
        <source>Use custom assumptions</source>
        <translation type="vanished">Benutzerdefinierte Annahmen verwenden</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Typ</translation>
    </message>
    <message>
        <source>Real Number</source>
        <translation type="vanished">Reelle Zahl</translation>
    </message>
    <message>
        <source>Rational Number</source>
        <translation type="vanished">Rationale Zahl</translation>
    </message>
    <message>
        <source>Sign</source>
        <translation type="vanished">Vorzeichen</translation>
    </message>
    <message>
        <source>The category this unknown variable belongs to</source>
        <translation type="vanished">Die Kategorie, zu der diese unbekannte Variable gehört</translation>
    </message>
    <message>
        <source>Value of this variable (expression)</source>
        <translation type="vanished">Wert der Variable (Ausdruck)</translation>
    </message>
    <message>
        <source>value is exact</source>
        <translation type="vanished">Wert ist genau</translation>
    </message>
    <message>
        <source>If the value is precise</source>
        <translation type="vanished">Wenn der Wert genau ist</translation>
    </message>
    <message>
        <source>The category this variable belongs to</source>
        <translation type="vanished">Die Kategorie, zu der diese Variable gehört</translation>
    </message>
    <message>
        <source>Create a new variable</source>
        <translation type="vanished">Eine neue Variable erstellen</translation>
    </message>
    <message>
        <source>Edit the selected variable</source>
        <translation type="vanished">Bearbeiten Sie die ausgewählte Variable</translation>
    </message>
    <message>
        <source>Insert the selected variable into the expression entry</source>
        <translation type="vanished">Einfügen der ausgewählten Variable in den Ausdruckseintrag</translation>
    </message>
    <message>
        <source>Delete the selected variable</source>
        <translation type="vanished">Löschen der ausgewählten Variable</translation>
    </message>
    <message>
        <source>(De)activate the selected variable</source>
        <translation type="vanished">Ausgewählte Variable (de)aktivieren</translation>
    </message>
    <message>
        <source>E_xport</source>
        <translation type="vanished">E_xportieren</translation>
    </message>
    <message>
        <source>_Variable</source>
        <translation type="vanished">_Variable</translation>
    </message>
    <message>
        <source>Execute expressions and commands from a file</source>
        <translation type="vanished">Ausdrücke und Befehle aus einer Datei ausführen</translation>
    </message>
    <message>
        <source>Start a new instance of the application</source>
        <translation type="vanished">Eine neue Instanz der Applikation starten</translation>
    </message>
    <message>
        <source>Display the application version</source>
        <translation type="vanished">Anzeigen der Applikationsversion</translation>
    </message>
    <message>
        <source>Specify the window title</source>
        <translation type="vanished">Festlegen des Fenstertitels</translation>
    </message>
    <message>
        <source>TITLE</source>
        <translation type="vanished">TITEL</translation>
    </message>
    <message>
        <source>Expression to calculate</source>
        <translation type="vanished">Zu berechnender Ausdruck</translation>
    </message>
    <message>
        <source>[EXPRESSION]</source>
        <translation type="vanished">[AUSDRUCK]</translation>
    </message>
    <message>
        <source>Type a mathematical expression above, e.g. &quot;5 + 2 / 3&quot;,
and press the enter key.</source>
        <translation type="vanished">Geben Sie oben einen mathematischen Ausdruck ein, z. B. &quot;5 + 2 / 3&quot;,
und drücken Sie die Eingabetaste.</translation>
    </message>
    <message>
        <source>ans</source>
        <translation type="vanished">ans</translation>
    </message>
    <message>
        <source>Last Answer</source>
        <translation type="vanished">Letzte Antwort</translation>
    </message>
    <message>
        <source>answer</source>
        <translation type="vanished">antwort</translation>
    </message>
    <message>
        <source>Answer 2</source>
        <translation type="vanished">Antwort 2</translation>
    </message>
    <message>
        <source>Answer 3</source>
        <translation type="vanished">Antwort 3</translation>
    </message>
    <message>
        <source>Answer 4</source>
        <translation type="vanished">Antwort 4</translation>
    </message>
    <message>
        <source>Answer 5</source>
        <translation type="vanished">Antwort 5</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation type="vanished">Speicher</translation>
    </message>
    <message>
        <source>Failed to load global definitions!
</source>
        <translation type="vanished">Das Laden der globalen Definitionen ist fehlgeschlagen!
</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>All</source>
        <extracomment>if no category has been selected (previously selected has been renamed/deleted), select &quot;All&quot;</extracomment>
        <translation type="vanished">Alle</translation>
    </message>
    <message>
        <source>By default, only one instance (one main window) of %s is allowed.

If multiple instances are opened simultaneously, only the definitions (variables, functions, etc.), mode, preferences, and history of the last closed window will be saved.

Do you, despite this, want to change the default bahvior and allow multiple simultaneous instances?</source>
        <translation type="vanished">Standardmäßig ist nur eine Instanz (ein Hauptfenster) von %s erlaubt.

Wenn mehrere Instanzen gleichzeitig geöffnet werden, werden nur die Definitionen (Variablen, Funktionen usw.), der Modus, die Einstellungen und der Verlauf des zuletzt geschlossenen Fensters gespeichert.

Möchten Sie trotzdem die Standardvorgabe ändern und mehrere gleichzeitige Instanzen zulassen?</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Right-click/long press: %s</source>
        <translation type="vanished">Rechtsklick/Lang drücken: %s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Right-click: %s</source>
        <translation type="vanished">Rechtsklick: %s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Middle-click: %s</source>
        <translation type="vanished">Mittelklick: %s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Cycle through previous expression</source>
        <translation type="vanished">Durchlaufen vorheriger Ausdrücke</translation>
    </message>
    <message>
        <source>Move cursor left or right</source>
        <translation type="vanished">Mauszeiger nach links oder rechts bewegen</translation>
    </message>
    <message>
        <source>Move cursor to beginning or end</source>
        <translation type="vanished">Mauszeiger zum Anfang oder Ende bewegen</translation>
    </message>
    <message>
        <source>Uncertainty/interval</source>
        <translation type="vanished">Ungenauigkeit/Intervall</translation>
    </message>
    <message>
        <source>Relative error</source>
        <translation type="vanished">Relativer Fehler</translation>
    </message>
    <message>
        <source>Argument separator</source>
        <translation type="vanished">Argument-Trennzeichen</translation>
    </message>
    <message>
        <source>Blank space</source>
        <translation type="vanished">Leerzeichen</translation>
    </message>
    <message>
        <source>New line</source>
        <translation type="vanished">Neue Zeile</translation>
    </message>
    <message>
        <source>Smart parentheses</source>
        <translation type="vanished">Intelligente Klammern</translation>
    </message>
    <message>
        <source>Vector brackets</source>
        <translation type="vanished">Vektorielle Klammern</translation>
    </message>
    <message>
        <source>Left parenthesis</source>
        <translation type="vanished">Linke Klammer</translation>
    </message>
    <message>
        <source>Left vector bracket</source>
        <translation type="vanished">Linke Vektor-Klammer</translation>
    </message>
    <message>
        <source>Right parenthesis</source>
        <translation type="vanished">Rechte Klammer</translation>
    </message>
    <message>
        <source>Right vector bracket</source>
        <translation type="vanished">Rechte Vektor-Klammer</translation>
    </message>
    <message>
        <source>Decimal point</source>
        <translation type="vanished">Dezimalpunkt</translation>
    </message>
    <message>
        <source>Raise (Ctrl+*)</source>
        <translation type="vanished">Erhöhen (Strg+*)</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Hinzufügen</translation>
    </message>
    <message>
        <source>M+ (memory plus)</source>
        <translation type="vanished">M+ (Speicher plus)</translation>
    </message>
    <message>
        <source>MC (memory clear)</source>
        <translation type="vanished">MC (Speicher löschen)</translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation type="vanished">Rücktaste</translation>
    </message>
    <message>
        <source>M− (memory minus)</source>
        <translation type="vanished">M- (Speicher minus)</translation>
    </message>
    <message>
        <source>Previous result (static)</source>
        <translation type="vanished">Vorheriges Ergebnis (statisch)</translation>
    </message>
    <message>
        <source>Calculate expression</source>
        <translation type="vanished">Ausdruck berechnen</translation>
    </message>
    <message>
        <source>MR (memory recall)</source>
        <translation type="vanished">MR (Speicherabruf)</translation>
    </message>
    <message>
        <source>MS (memory store)</source>
        <translation type="vanished">MS (Speicher speichern)</translation>
    </message>
    <message>
        <source>Set unknowns</source>
        <translation type="vanished">Unbekannte setzen</translation>
    </message>
    <message>
        <source>more</source>
        <extracomment>Show further items in a submenu</extracomment>
        <translation type="vanished">mehr</translation>
    </message>
    <message>
        <source>Logical AND</source>
        <translation type="vanished">Logisches UND</translation>
    </message>
    <message>
        <source>Logical OR</source>
        <translation type="vanished">Logisches ODER</translation>
    </message>
    <message>
        <source>Logical NOT</source>
        <translation type="vanished">logisches NICHT</translation>
    </message>
    <message>
        <source>Toggle Result Base</source>
        <translation type="vanished">Ergebnisbasis umschalten</translation>
    </message>
    <message>
        <source>Open menu with stored variables</source>
        <translation type="vanished">Menü mit gespeicherten Variablen öffnen</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="vanished">Index</translation>
    </message>
    <message>
        <source>ENTER</source>
        <extracomment>RPN Enter (calculate and add to stack)</extracomment>
        <translation type="vanished">EINGABE</translation>
    </message>
    <message>
        <source>Calculate expression and add to stack</source>
        <translation type="vanished">Ausdruck berechnen und zum Stapel hinzufügen</translation>
    </message>
    <message>
        <source>Flag</source>
        <translation type="vanished">Flagge</translation>
    </message>
    <message>
        <source>Matrices</source>
        <translation type="vanished">Matrizen</translation>
    </message>
    <message>
        <source>Gregorian</source>
        <translation type="vanished">Gregorianisch</translation>
    </message>
    <message>
        <source>Revised Julian (Milanković)</source>
        <translation type="vanished">Neujulianisch (Milanković)</translation>
    </message>
    <message>
        <source>Julian</source>
        <translation type="vanished">Julianisch</translation>
    </message>
    <message>
        <source>Islamic (Hijri)</source>
        <translation type="vanished">Islamisch (Hijri)</translation>
    </message>
    <message>
        <source>Hebrew</source>
        <translation type="vanished">Hebräisch</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Chinesisch</translation>
    </message>
    <message>
        <source>Persian (Solar Hijri)</source>
        <translation type="vanished">Persisch (Solar Hijri)</translation>
    </message>
    <message>
        <source>Coptic</source>
        <translation type="vanished">Koptisch</translation>
    </message>
    <message>
        <source>Ethiopian</source>
        <translation type="vanished">Äthiopisch</translation>
    </message>
    <message>
        <source>Indian (National)</source>
        <translation type="vanished">Indisch (National)</translation>
    </message>
    <message>
        <source>Action</source>
        <translation type="vanished">Aktion</translation>
    </message>
    <message>
        <source>Key combination</source>
        <translation type="vanished">Tastenkombination</translation>
    </message>
    <message>
        <source>Raise</source>
        <translation type="vanished">Erhöhen</translation>
    </message>
    <message>
        <source>History Answer Value</source>
        <translation type="vanished">Verlauf Ergebniswert</translation>
    </message>
    <message>
        <source>History Index(es)</source>
        <translation type="vanished">Verlaufsindex(e)</translation>
    </message>
    <message>
        <source>History index %s does not exist.</source>
        <translation type="vanished">Verlaufsindex %s existiert nicht.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>expression</source>
        <translation type="vanished">ausdruck</translation>
    </message>
    <message>
        <source>History Parsed Expression</source>
        <translation type="vanished">Verlauf des analysierten Ausdrucks</translation>
    </message>
    <message>
        <source>Set Window Title</source>
        <translation type="vanished">Fenstertitel setzen</translation>
    </message>
    <message>
        <source>Failed to open %s.
%s</source>
        <translation type="vanished">Konnte %s. nicht öffnen
%s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Could not display help for Qalculate!.</source>
        <translation type="vanished">Konnte keine Hilfe für Qalculate! anzeigen.</translation>
    </message>
    <message>
        <source>Could not display help for Qalculate!.
%s</source>
        <translation type="vanished">Konnte keine Hilfe für Qalculate! anzeigen.
%s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>approx.</source>
        <translation type="vanished">ca.</translation>
    </message>
    <message>
        <source>Stop process</source>
        <translation type="vanished">Prozess anhalten</translation>
    </message>
    <message>
        <source>Clear expression</source>
        <translation type="vanished">Ausdruck löschen</translation>
    </message>
    <message>
        <source>EXACT</source>
        <translation type="vanished">EXAKT</translation>
    </message>
    <message>
        <source>APPROX</source>
        <translation type="vanished">CIRCA</translation>
    </message>
    <message>
        <source>RPN</source>
        <translation type="vanished">RPN</translation>
    </message>
    <message>
        <source>CHN</source>
        <extracomment>Chain mode</extracomment>
        <translation type="vanished">CHN</translation>
    </message>
    <message>
        <source>ROMAN</source>
        <translation type="vanished">RÖMISCH</translation>
    </message>
    <message>
        <source>DEG</source>
        <translation type="vanished">DEG</translation>
    </message>
    <message>
        <source>RAD</source>
        <translation type="vanished">RAD</translation>
    </message>
    <message>
        <source>GRA</source>
        <translation type="vanished">GRA</translation>
    </message>
    <message>
        <source>PREC</source>
        <translation type="vanished">PREZ</translation>
    </message>
    <message>
        <source>FUNC</source>
        <translation type="vanished">FUNK</translation>
    </message>
    <message>
        <source>UNIT</source>
        <translation type="vanished">EINHEIT</translation>
    </message>
    <message>
        <source>VAR</source>
        <translation type="vanished">VAR</translation>
    </message>
    <message>
        <source>INF</source>
        <translation type="vanished">INF</translation>
    </message>
    <message>
        <source>CPLX</source>
        <translation type="vanished">KPLX</translation>
    </message>
    <message>
        <source>Do you wish to update the exchange rates now?</source>
        <translation type="vanished">Möchten Sie die Wechselkurse jetzt aktualisieren?</translation>
    </message>
    <message numerus="yes">
        <source>It has been %s day since the exchange rates last were updated.</source>
        <translation type="vanished">
            <numerusform>Es ist %s Tag seit der letzten Aktualisierung der Wechselkurse vergangen.</numerusform>
            <numerusform>Es sind %s Tage seit der letzten Aktualisierung der Wechselkurse vergangen.</numerusform>
        </translation>
        <extra-po-flags>c-format</extra-po-flags>
        <extra-po-msgid_plural>It has been %s days since the exchange rates last were updated.</extra-po-msgid_plural>
    </message>
    <message>
        <source>Do not ask again</source>
        <translation type="vanished">Nicht erneut fragen</translation>
    </message>
    <message>
        <source>It took too long to generate the plot data.</source>
        <translation type="vanished">Es hat zu lange gedauert, die Plotdaten zu generieren.</translation>
    </message>
    <message>
        <source>It took too long to generate the plot data. Please decrease the sampling rate or increase the time limit in preferences.</source>
        <translation type="vanished">Es hat zu lange gedauert, die Plotdaten zu generieren. Bitte verringern Sie die Abtast-rate oder erhöhen Sie das Zeitlimit in den Einstellungen.</translation>
    </message>
    <message>
        <source>When errors, warnings and other information are generated during calculation, the icon in the upper right corner of the expression entry changes to reflect this. If you hold the pointer over or click the icon, the message will be shown.</source>
        <translation type="vanished">Wenn während der Berechnung Fehler, Warnungen und andere Informationen generiert werden, ändert sich das Symbol in der oberen rechten Ecke des Ausdruckseintrags, um dies anzuzeigen. Wenn Sie den Zeiger über das Symbol halten oder darauf klicken, wird die Meldung angezeigt.</translation>
    </message>
    <message>
        <source>Path of executable not found.</source>
        <translation type="vanished">Pfad der ausführbaren Datei nicht gefunden.</translation>
    </message>
    <message>
        <source>curl not found.</source>
        <translation type="vanished">curl nicht gefunden.</translation>
    </message>
    <message>
        <source>Failed to run update script.
%s</source>
        <translation type="vanished">Update-Skript konnte nicht ausgeführt werden.
%s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Failed to check for updates.</source>
        <translation type="vanished">Prüfung auf Updates fehlgeschlagen.</translation>
    </message>
    <message>
        <source>No updates found.</source>
        <translation type="vanished">Keine Updates gefunden.</translation>
    </message>
    <message>
        <source>A new version of %s is available at %s.

Do you wish to update to version %s.</source>
        <translation type="vanished">Eine neue Version von %s ist verfügbar unter %s.

Möchten Sie auf die Version %s aktualisieren.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>A new version of %s is available.

You can get version %s at %s.</source>
        <translation type="vanished">Eine neue Version von %s ist verfügbar.

Sie können die Version %s unter %s erhalten.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Too many arguments for %s().</source>
        <translation type="vanished">Zu viele Argumente für %s().</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>argument</source>
        <translation type="vanished">argument</translation>
    </message>
    <message>
        <source>Temperature Calculation Mode</source>
        <translation type="vanished">Temperatur-Berechnungsmodus</translation>
    </message>
    <message>
        <source>The expression is ambiguous.
Please select temperature calculation mode
(the mode can later be changed in preferences).</source>
        <translation type="vanished">Der Ausdruck ist mehrdeutig.
Bitte wählen Sie den Temperaturberechnungsmodus
(der Modus kann später in den Einstellungen geändert werden).</translation>
    </message>
    <message>
        <source>Interpretation of dots</source>
        <translation type="vanished">Interpretation von Punkten</translation>
    </message>
    <message>
        <source>Please select interpretation of dots (&quot;.&quot;)
(this can later be changed in preferences).</source>
        <translation type="vanished">Bitte wählen Sie die Interpretation der Punkte (&quot;.&quot;)
(dies kann später in den Einstellungen geändert werden).</translation>
    </message>
    <message>
        <source>Both dot and comma as decimal separators</source>
        <translation type="vanished">Sowohl Punkt als auch Komma als Dezimaltrennzeichen</translation>
    </message>
    <message>
        <source>Dot as thousands separator</source>
        <translation type="vanished">Punkt als Tausendertrennzeichen</translation>
    </message>
    <message>
        <source>Only dot as decimal separator</source>
        <translation type="vanished">Nur Punkt als Dezimaltrennzeichen</translation>
    </message>
    <message>
        <source>Uncategorized</source>
        <translation type="vanished">Nicht kategorisiert</translation>
    </message>
    <message>
        <source>hexadecimal</source>
        <translation type="vanished">hexadezimal</translation>
    </message>
    <message>
        <source>octal</source>
        <translation type="vanished">oktal</translation>
    </message>
    <message>
        <source>decimal</source>
        <translation type="vanished">dezimal</translation>
    </message>
    <message>
        <source>duodecimal</source>
        <translation type="vanished">duodezimal</translation>
    </message>
    <message>
        <source>binary</source>
        <translation type="vanished">binär</translation>
    </message>
    <message>
        <source>roman</source>
        <translation type="vanished">römisch</translation>
    </message>
    <message>
        <source>bijective</source>
        <translation type="vanished">bijektiv</translation>
    </message>
    <message>
        <source>sexagesimal</source>
        <translation type="vanished">sexagesimal</translation>
    </message>
    <message>
        <source>latitude</source>
        <translation type="vanished">breitengrad</translation>
    </message>
    <message>
        <source>longitude</source>
        <translation type="vanished">längengrad</translation>
    </message>
    <message>
        <source>time</source>
        <translation type="vanished">zeit</translation>
    </message>
    <message>
        <source>bases</source>
        <translation type="vanished">basen</translation>
    </message>
    <message>
        <source>calendars</source>
        <translation type="vanished">kalendarien</translation>
    </message>
    <message>
        <source>rectangular</source>
        <translation type="vanished">rechtwinklig</translation>
    </message>
    <message>
        <source>cartesian</source>
        <translation type="vanished">kartesisch</translation>
    </message>
    <message>
        <source>exponential</source>
        <translation type="vanished">exponentiell</translation>
    </message>
    <message>
        <source>polar</source>
        <translation type="vanished">polar</translation>
    </message>
    <message>
        <source>angle</source>
        <translation type="vanished">winkel</translation>
    </message>
    <message>
        <source>phasor</source>
        <translation type="vanished">phase</translation>
    </message>
    <message>
        <source>optimal</source>
        <translation type="vanished">optimal</translation>
    </message>
    <message>
        <source>base</source>
        <translation type="vanished">basis</translation>
    </message>
    <message>
        <source>mixed</source>
        <translation type="vanished">gemischt</translation>
    </message>
    <message>
        <source>fraction</source>
        <translation type="vanished">bruchteil</translation>
    </message>
    <message>
        <source>factors</source>
        <translation type="vanished">faktoren</translation>
    </message>
    <message>
        <source>partial fraction</source>
        <translation type="vanished">teilbruch</translation>
    </message>
    <message>
        <source>factorize</source>
        <translation type="vanished">faktorisieren</translation>
    </message>
    <message>
        <source>expand</source>
        <translation type="vanished">erweitern</translation>
    </message>
    <message>
        <source>hexadecimal number</source>
        <translation type="vanished">hexadezimale zahl</translation>
    </message>
    <message>
        <source>octal number</source>
        <translation type="vanished">oktalzahl</translation>
    </message>
    <message>
        <source>decimal number</source>
        <translation type="vanished">dezimalzahl</translation>
    </message>
    <message>
        <source>duodecimal number</source>
        <translation type="vanished">duodezimalzahl</translation>
    </message>
    <message>
        <source>binary number</source>
        <translation type="vanished">binärzahl</translation>
    </message>
    <message>
        <source>roman numerals</source>
        <translation type="vanished">römische Ziffern</translation>
    </message>
    <message>
        <source>bijective base-26</source>
        <translation type="vanished">bijektive basis-26</translation>
    </message>
    <message>
        <source>sexagesimal number</source>
        <translation type="vanished">sexagesimale Zahl</translation>
    </message>
    <message>
        <source>32-bit floating point</source>
        <translation type="vanished">32-Bit-Gleitkomma</translation>
    </message>
    <message>
        <source>64-bit floating point</source>
        <translation type="vanished">64-Bit-Gleitkomma</translation>
    </message>
    <message>
        <source>16-bit floating point</source>
        <translation type="vanished">16-Bit Gleitkomma</translation>
    </message>
    <message>
        <source>80-bit (x86) floating point</source>
        <translation type="vanished">80-Bit (x86) Gleitkomma</translation>
    </message>
    <message>
        <source>128-bit floating point</source>
        <translation type="vanished">128-Bit Gleitkomma</translation>
    </message>
    <message>
        <source>time format</source>
        <translation type="vanished">zeitformat</translation>
    </message>
    <message>
        <source>number bases</source>
        <translation type="vanished">zahlenbasen</translation>
    </message>
    <message>
        <source>optimal unit</source>
        <translation type="vanished">optimale einheit</translation>
    </message>
    <message>
        <source>base units</source>
        <translation type="vanished">basiseinheiten</translation>
    </message>
    <message>
        <source>mixed units</source>
        <translation type="vanished">gemischte einheiten</translation>
    </message>
    <message>
        <source>expanded partial fractions</source>
        <translation type="vanished">erweiterte teilbrüche</translation>
    </message>
    <message>
        <source>complex rectangular form</source>
        <translation type="vanished">komplexe rechteckform</translation>
    </message>
    <message>
        <source>complex exponential form</source>
        <translation type="vanished">komplexe exponentialform</translation>
    </message>
    <message>
        <source>complex polar form</source>
        <translation type="vanished">komplexe polarform</translation>
    </message>
    <message>
        <source>complex cis form</source>
        <translation type="vanished">komplexe cis-form</translation>
    </message>
    <message>
        <source>complex angle notation</source>
        <translation type="vanished">komplexe winkeldarstellung</translation>
    </message>
    <message>
        <source>complex phasor notation</source>
        <translation type="vanished">komplexe Phasenschreibweise</translation>
    </message>
    <message>
        <source>UTC time zone</source>
        <translation type="vanished">UTC-Zeitzone</translation>
    </message>
    <message>
        <source>number base %s</source>
        <translation type="vanished">zahlenbasis %s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Inactive</source>
        <translation type="vanished">Inaktiv</translation>
    </message>
    <message>
        <source>Retrieves data from the %s data set for a given object and property. If &quot;info&quot; is typed as property, a dialog window will pop up with all properties of the object.</source>
        <translation type="vanished">Ruft Daten aus dem %s-Datensatz für ein angegebenes Objekt und eine Eigenschaft ab. Wenn &quot;info&quot; als Eigenschaft eingegeben wird, wird ein Dialogfenster mit allen Eigenschaften des Objekts angezeigt.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Example:</source>
        <translation type="vanished">Beispiel:</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="vanished">Argumente</translation>
    </message>
    <message>
        <source>optional</source>
        <extracomment>optional argument</extracomment>
        <translation type="vanished">optional</translation>
    </message>
    <message>
        <source>default: </source>
        <extracomment>argument default, in description</extracomment>
        <translation type="vanished">standard: </translation>
    </message>
    <message>
        <source>Requirement</source>
        <translation type="vanished">Bedingung</translation>
    </message>
    <message>
        <source>key</source>
        <extracomment>indicating that the property is a data set key</extracomment>
        <translation type="vanished">Taste</translation>
    </message>
    <message>
        <source>Acti_vate</source>
        <translation type="vanished">Akti_vate</translation>
    </message>
    <message>
        <source>a previous result</source>
        <translation type="vanished">ein vorheriges Ergebnis</translation>
    </message>
    <message>
        <source>matrix</source>
        <translation type="vanished">matrix</translation>
    </message>
    <message>
        <source>vector</source>
        <translation type="vanished">vektor</translation>
    </message>
    <message>
        <source>positive</source>
        <translation type="vanished">positiv</translation>
    </message>
    <message>
        <source>non-positive</source>
        <translation type="vanished">nicht-positiv</translation>
    </message>
    <message>
        <source>negative</source>
        <translation type="vanished">negativ</translation>
    </message>
    <message>
        <source>non-negative</source>
        <translation type="vanished">nicht-negativ</translation>
    </message>
    <message>
        <source>non-zero</source>
        <translation type="vanished">nicht-null</translation>
    </message>
    <message>
        <source>boolean</source>
        <translation type="vanished">boolesch</translation>
    </message>
    <message>
        <source>integer</source>
        <translation type="vanished">ganzzahlig</translation>
    </message>
    <message>
        <source>rational</source>
        <translation type="vanished">rational</translation>
    </message>
    <message>
        <source>real</source>
        <translation type="vanished">reell</translation>
    </message>
    <message>
        <source>complex</source>
        <translation type="vanished">komplex</translation>
    </message>
    <message>
        <source>number</source>
        <translation type="vanished">zahl</translation>
    </message>
    <message>
        <source>(not matrix)</source>
        <translation type="vanished">(nicht Matrix)</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">unbekannt</translation>
    </message>
    <message>
        <source>default assumptions</source>
        <translation type="vanished">standardmäßige Annahmen</translation>
    </message>
    <message>
        <source>Variable does not exist anymore.</source>
        <translation type="vanished">Variable existiert nicht mehr.</translation>
    </message>
    <message>
        <source>Data Retrieval Function</source>
        <translation type="vanished">Funktion zum Abrufen von Daten</translation>
    </message>
    <message>
        <source>Insert function</source>
        <translation type="vanished">Funktion einfügen</translation>
    </message>
    <message>
        <source>Insert function (dialog)</source>
        <translation type="vanished">Funktion einfügen (Dialog)</translation>
    </message>
    <message>
        <source>Insert variable</source>
        <translation type="vanished">Variable einfügen</translation>
    </message>
    <message>
        <source>Insert unit</source>
        <translation type="vanished">Einheit einfügen</translation>
    </message>
    <message>
        <source>Insert text</source>
        <translation type="vanished">Text einfügen</translation>
    </message>
    <message>
        <source>Insert date</source>
        <translation type="vanished">Datum einfügen</translation>
    </message>
    <message>
        <source>Insert vector</source>
        <translation type="vanished">Vektor einfügen</translation>
    </message>
    <message>
        <source>Insert matrix</source>
        <translation type="vanished">Matrix einfügen</translation>
    </message>
    <message>
        <source>Insert smart parentheses</source>
        <translation type="vanished">Intelligente Klammern einfügen</translation>
    </message>
    <message>
        <source>Convert to unit</source>
        <translation type="vanished">In Einheit umrechnen</translation>
    </message>
    <message>
        <source>Convert to unit (entry)</source>
        <translation type="vanished">In Einheit umrechnen (Eingabe)</translation>
    </message>
    <message>
        <source>Convert to optimal unit</source>
        <translation type="vanished">In optimale Einheit umrechnen</translation>
    </message>
    <message>
        <source>Convert to base units</source>
        <translation type="vanished">In Basiseinheiten umrechnen</translation>
    </message>
    <message>
        <source>Convert to optimal prefix</source>
        <translation type="vanished">In optimales Präfix umrechnen</translation>
    </message>
    <message>
        <source>Convert to number base</source>
        <translation type="vanished">In Zahlenbasis umrechnen</translation>
    </message>
    <message>
        <source>Factorize result</source>
        <translation type="vanished">Ergebnis faktorisieren</translation>
    </message>
    <message>
        <source>Expand result</source>
        <translation type="vanished">Expandieren des Ergebnisses</translation>
    </message>
    <message>
        <source>Expand partial fractions</source>
        <translation type="vanished">Expandieren von Teilbrüchen</translation>
    </message>
    <message>
        <source>RPN: down</source>
        <translation type="vanished">RPN: abwärts</translation>
    </message>
    <message>
        <source>RPN: up</source>
        <translation type="vanished">RPN: aufwärts</translation>
    </message>
    <message>
        <source>RPN: swap</source>
        <translation type="vanished">RPN: tauschen</translation>
    </message>
    <message>
        <source>RPN: copy</source>
        <translation type="vanished">RPN: kopieren</translation>
    </message>
    <message>
        <source>RPN: lastx</source>
        <translation type="vanished">RPN: lastx</translation>
    </message>
    <message>
        <source>RPN: delete register</source>
        <translation type="vanished">RPN: Register löschen</translation>
    </message>
    <message>
        <source>RPN: clear stack</source>
        <translation type="vanished">RPN: Stapel löschen</translation>
    </message>
    <message>
        <source>Load meta mode</source>
        <translation type="vanished">Metamodus laden</translation>
    </message>
    <message>
        <source>Set expression base</source>
        <translation type="vanished">Ausdrucksbasis einstellen</translation>
    </message>
    <message>
        <source>Set result base</source>
        <translation type="vanished">Ergebnisbasis einstellen</translation>
    </message>
    <message>
        <source>Toggle exact mode</source>
        <translation type="vanished">Exakten Modus einstellen</translation>
    </message>
    <message>
        <source>Set angle unit to degrees</source>
        <translation type="vanished">Winkeleinheit auf Grad stellen</translation>
    </message>
    <message>
        <source>Set angle unit to radians</source>
        <translation type="vanished">Winkeleinheit auf Bogenmaß stellen</translation>
    </message>
    <message>
        <source>Set angle unit to gradians</source>
        <translation type="vanished">Winkeleinheit auf Gradienten stellen</translation>
    </message>
    <message>
        <source>Toggle simple fractions</source>
        <translation type="vanished">Einfache Brüche umschalten</translation>
    </message>
    <message>
        <source>Toggle mixed fractions</source>
        <translation type="vanished">Gemischte Brüche umschalten</translation>
    </message>
    <message>
        <source>Toggle scientific notation</source>
        <translation type="vanished">Umschalten der wissenschaftlichen Schreibweise</translation>
    </message>
    <message>
        <source>Toggle simple notation</source>
        <translation type="vanished">Umschalten der einfachen Schreibweise</translation>
    </message>
    <message>
        <source>Toggle RPN mode</source>
        <translation type="vanished">RPN-Modus umschalten</translation>
    </message>
    <message>
        <source>Toggle calculate as you type</source>
        <translation type="vanished">Umschalten zwischen Rechnen während der Eingabe</translation>
    </message>
    <message>
        <source>Toggle programming keypad</source>
        <translation type="vanished">Programmiertastatur ein- und ausschalten</translation>
    </message>
    <message>
        <source>Show keypad</source>
        <translation type="vanished">Tastenfeld anzeigen</translation>
    </message>
    <message>
        <source>Show history</source>
        <translation type="vanished">Verlauf anzeigen</translation>
    </message>
    <message>
        <source>Search history</source>
        <translation type="vanished">Verlauf suchen</translation>
    </message>
    <message>
        <source>Show conversion</source>
        <translation type="vanished">Umrechnung anzeigen</translation>
    </message>
    <message>
        <source>Show RPN stack</source>
        <translation type="vanished">RPN-Stack anzeigen</translation>
    </message>
    <message>
        <source>Manage variables</source>
        <translation type="vanished">Variablen verwalten</translation>
    </message>
    <message>
        <source>Manage functions</source>
        <translation type="vanished">Funktionen verwalten</translation>
    </message>
    <message>
        <source>Manage data sets</source>
        <translation type="vanished">Datensätze verwalten</translation>
    </message>
    <message>
        <source>New variable</source>
        <translation type="vanished">Neue Variable</translation>
    </message>
    <message>
        <source>New function</source>
        <translation type="vanished">Neue Funktion</translation>
    </message>
    <message>
        <source>Open plot functions/data</source>
        <translation type="vanished">Plotfunktionen/Daten öffnen</translation>
    </message>
    <message>
        <source>Open convert number bases</source>
        <translation type="vanished">Aufruf Zahlenbasis konvertieren</translation>
    </message>
    <message>
        <source>Open floating point conversion</source>
        <translation type="vanished">Gleitkomma-Konvertierung öffnen</translation>
    </message>
    <message>
        <source>Open calender conversion</source>
        <translation type="vanished">Kalenderkonvertierung öffnen</translation>
    </message>
    <message>
        <source>Open percentage calculation tool</source>
        <translation type="vanished">Prozentrechnungs-Tool öffnen</translation>
    </message>
    <message>
        <source>Open periodic table</source>
        <translation type="vanished">Periodensystem öffnen</translation>
    </message>
    <message>
        <source>Update exchange rates</source>
        <translation type="vanished">Wechselkurse aktualisieren</translation>
    </message>
    <message>
        <source>Copy result</source>
        <translation type="vanished">Ergebnis kopieren</translation>
    </message>
    <message>
        <source>Insert result</source>
        <translation type="obsolete">Text einfügen</translation>
    </message>
    <message>
        <source>Save result image</source>
        <translation type="vanished">Ergebnisbild speichern</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hilfe</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Beenden</translation>
    </message>
    <message>
        <source>Toggle chain mode</source>
        <translation type="vanished">Schalte um auf Methodenverkettung</translation>
    </message>
    <message>
        <source>Toggle keep above</source>
        <translation type="obsolete">Exakten Modus einstellen</translation>
    </message>
    <message>
        <source>Show/hide completion</source>
        <translation type="obsolete">Keine Fertigstellung</translation>
    </message>
    <message>
        <source>Prefixes</source>
        <translation type="vanished">Präfixe</translation>
    </message>
    <message>
        <source>No Prefix</source>
        <translation type="vanished">Kein Präfix</translation>
    </message>
    <message>
        <source>Optimal Prefix</source>
        <translation type="vanished">Optimales Präfix</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation type="vanished">Präfix</translation>
    </message>
    <message>
        <source>Complex angle/phasor notation</source>
        <translation type="vanished">Komplexe Winkel-/Phasenschreibweise</translation>
    </message>
    <message>
        <source>Number bases</source>
        <translation type="vanished">Zahlenbasen</translation>
    </message>
    <message>
        <source>Base units</source>
        <translation type="vanished">Basiseinheiten</translation>
    </message>
    <message>
        <source>Binary number</source>
        <translation type="vanished">Binäre Zahlen</translation>
    </message>
    <message>
        <source>Calendars</source>
        <translation type="vanished">Kalendarien</translation>
    </message>
    <message>
        <source>Complex cis form</source>
        <translation type="vanished">Komplexe cis-Form</translation>
    </message>
    <message>
        <source>Decimal number</source>
        <translation type="vanished">Dezimalzahl</translation>
    </message>
    <message>
        <source>Duodecimal number</source>
        <translation type="vanished">Duodezimale Zahl</translation>
    </message>
    <message>
        <source>Complex exponential form</source>
        <translation type="vanished">Komplexe Exponentialform</translation>
    </message>
    <message>
        <source>Factors</source>
        <translation type="vanished">Faktoren</translation>
    </message>
    <message>
        <source>16-bit floating point binary format</source>
        <translation type="vanished">16-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <source>32-bit floating point binary format</source>
        <translation type="vanished">32-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <source>64-bit floating point binary format</source>
        <translation type="vanished">64-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <source>80-bit (x86) floating point binary format</source>
        <translation type="vanished">80-Bit (x86) Fließkomma-Binärformat</translation>
    </message>
    <message>
        <source>128-bit floating point binary format</source>
        <translation type="vanished">128-Bit Fließkomma-Binärformat</translation>
    </message>
    <message>
        <source>Hexadecimal number</source>
        <translation type="vanished">Hexadezimalzahl</translation>
    </message>
    <message>
        <source>Latitude</source>
        <translation type="vanished">Breitengrad</translation>
    </message>
    <message>
        <source>Longitude</source>
        <translation type="vanished">Längengrad</translation>
    </message>
    <message>
        <source>Mixed units</source>
        <translation type="vanished">Gemischte Einheiten</translation>
    </message>
    <message>
        <source>Octal number</source>
        <translation type="vanished">Oktalzahl</translation>
    </message>
    <message>
        <source>Optimal units</source>
        <translation type="vanished">Optimale Einheiten</translation>
    </message>
    <message>
        <source>Expanded partial fractions</source>
        <translation type="vanished">Erweiterte Teilbrüche</translation>
    </message>
    <message>
        <source>Complex polar form</source>
        <translation type="vanished">Komplexe Polarform</translation>
    </message>
    <message>
        <source>Complex rectangular form</source>
        <translation type="vanished">Komplexe Rechteckform</translation>
    </message>
    <message>
        <source>Sexagesimal number</source>
        <translation type="vanished">Sexagesimalzahl</translation>
    </message>
    <message>
        <source>and</source>
        <translation type="vanished">und</translation>
    </message>
    <message>
        <source>or</source>
        <translation type="vanished">oder</translation>
    </message>
    <message>
        <source>undefined</source>
        <translation type="vanished">undefiniert</translation>
    </message>
    <message>
        <source>result is too long
see history</source>
        <translation type="vanished">Ergebnis ist zu lang
siehe Verlauf</translation>
    </message>
    <message>
        <source>calculation was aborted</source>
        <translation type="vanished">Berechnung wurde abgebrochen</translation>
    </message>
    <message>
        <source>RPN Register Moved</source>
        <translation type="vanished">RPN-Register verschoben</translation>
    </message>
    <message>
        <source>RPN Operation</source>
        <translation type="vanished">RPN-Operation</translation>
    </message>
    <message>
        <source>Processing…</source>
        <translation type="vanished">Verarbeitung...</translation>
    </message>
    <message>
        <source>result processing was aborted</source>
        <translation type="vanished">ergebnisverarbeitung wurde abgebrochen</translation>
    </message>
    <message>
        <source>Factorizing…</source>
        <translation type="vanished">Faktorisieren...</translation>
    </message>
    <message>
        <source>Expanding partial fractions…</source>
        <translation type="vanished">Expandieren von Teilbrüchen...</translation>
    </message>
    <message>
        <source>Expanding…</source>
        <translation type="vanished">Expandieren...</translation>
    </message>
    <message>
        <source>Calculating…</source>
        <translation type="vanished">Berechnen...</translation>
    </message>
    <message>
        <source>Converting…</source>
        <translation type="vanished">Konvertieren...</translation>
    </message>
    <message>
        <source>Fetching exchange rates.</source>
        <translation type="vanished">Abrufen von Wechselkursen.</translation>
    </message>
    <message>
        <source>Time zone parsing failed.</source>
        <translation type="vanished">Zeitzonenanalyse fehlgeschlagen.</translation>
    </message>
    <message>
        <source>Keep open</source>
        <translation type="vanished">Offen halten</translation>
    </message>
    <message>
        <source>Enter</source>
        <extracomment>RPN Enter (calculate and add to stack)</extracomment>
        <translation type="vanished">eingeben</translation>
    </message>
    <message>
        <source>C_alculate</source>
        <translation type="vanished">B_erechnen</translation>
    </message>
    <message>
        <source>Apply to Stack</source>
        <translation type="vanished">Auf Stapel anwenden</translation>
    </message>
    <message>
        <source>Argument</source>
        <translation type="vanished">Argument</translation>
    </message>
    <message>
        <source>True</source>
        <translation type="vanished">Wahr</translation>
    </message>
    <message>
        <source>False</source>
        <translation type="vanished">Falsch</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="vanished">Info</translation>
    </message>
    <message>
        <source>Edit Unit (global)</source>
        <translation type="vanished">Einheit bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Unit</source>
        <translation type="vanished">Neue Einheit</translation>
    </message>
    <message>
        <source>Empty name field.</source>
        <translation type="vanished">Leeres Namensfeld.</translation>
    </message>
    <message>
        <source>A variable or unit with the same name already exists.
Do you want to overwrite it?</source>
        <extracomment>unit with the same name exists -- overwrite or open the dialog again</extracomment>
        <translation type="vanished">Eine Variable oder Einheit mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <source>Base unit does not exist.</source>
        <translation type="vanished">Basiseinheit ist nicht vorhanden.</translation>
    </message>
    <message>
        <source>Edit Function (global)</source>
        <translation type="vanished">Funktion bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Function</source>
        <translation type="vanished">Neue Funktion</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Nein</translation>
    </message>
    <message>
        <source>Empty expression field.</source>
        <translation type="vanished">Leeres Ausdrucksfeld.</translation>
    </message>
    <message>
        <source>A function with the same name already exists.
Do you want to overwrite the function?</source>
        <extracomment>function with the same name exists -- overwrite or open the dialog againdataset with the same name exists -- overwrite or open the dialog again</extracomment>
        <translation type="vanished">Eine Funktion mit demselben Namen existiert bereits.
Möchten Sie die Funktion überschreiben?</translation>
    </message>
    <message>
        <source>Unit does not exist</source>
        <translation type="vanished">Einheit ist nicht vorhanden</translation>
    </message>
    <message>
        <source>Edit Unknown Variable (global)</source>
        <translation type="vanished">Unbekannte Variable bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Unknown Variable</source>
        <translation type="vanished">Neue Unbekannte Variable</translation>
    </message>
    <message>
        <source>An unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <extracomment>unknown with the same name exists -- overwrite or open dialog againvariable with the same name exists -- overwrite or open dialog again</extracomment>
        <translation type="vanished">Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <source>Edit Variable (global)</source>
        <translation type="vanished">Variable bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Variable</source>
        <translation type="vanished">Neue Variable</translation>
    </message>
    <message>
        <source>Empty value field.</source>
        <translation type="vanished">Leeres Wertefeld.</translation>
    </message>
    <message>
        <source>Edit Vector</source>
        <translation type="vanished">Vektor bearbeiten</translation>
    </message>
    <message>
        <source>Edit Vector (global)</source>
        <translation type="vanished">Vektor bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Vector</source>
        <translation type="vanished">Neuer Vektor</translation>
    </message>
    <message>
        <source>Edit Matrix (global)</source>
        <translation type="vanished">Matrix bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Matrix</source>
        <translation type="vanished">Neue Matrix</translation>
    </message>
    <message>
        <source>Vector Result</source>
        <translation type="vanished">Vektor Ergebnis</translation>
    </message>
    <message>
        <source>Matrix Result</source>
        <translation type="vanished">Matrix-Ergebnis</translation>
    </message>
    <message>
        <source>New Data Object</source>
        <translation type="vanished">Neues Datenobjekt</translation>
    </message>
    <message>
        <source>text</source>
        <translation type="vanished">text</translation>
    </message>
    <message>
        <source>approximate</source>
        <translation type="vanished">ungefähr</translation>
    </message>
    <message>
        <source>Edit Data Set (global)</source>
        <translation type="vanished">Datensatz bearbeiten (global)</translation>
    </message>
    <message>
        <source>New Data Set</source>
        <translation type="vanished">Neuer Datensatz</translation>
    </message>
    <message>
        <source>info</source>
        <translation type="vanished">info</translation>
    </message>
    <message>
        <source>Property</source>
        <translation type="vanished">Eigenschaft</translation>
    </message>
    <message>
        <source>No file name entered.</source>
        <translation type="vanished">Kein Dateiname eingegeben.</translation>
    </message>
    <message>
        <source>No delimiter selected.</source>
        <translation type="vanished">Kein Begrenzungszeichen ausgewählt.</translation>
    </message>
    <message>
        <source>Could not import from file 
%s</source>
        <translation type="vanished">Konnte nicht aus Datei importieren
%s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>No variable name entered.</source>
        <translation type="vanished">Es wurde kein Variablenname eingegeben.</translation>
    </message>
    <message>
        <source>No known variable with entered name found.</source>
        <translation type="vanished">Keine bekannte Variable mit eingegebenem Namen gefunden.</translation>
    </message>
    <message>
        <source>Could not export to file 
%s</source>
        <translation type="vanished">Konnte nicht in eine Datei exportieren 
%s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>My Variables</source>
        <translation type="vanished">Meine Variablen</translation>
    </message>
    <message>
        <source>Couldn&apos;t write definitions</source>
        <translation type="vanished">Definitionen konnten nicht geschrieben werden</translation>
    </message>
    <message>
        <source>Preset</source>
        <translation type="vanished">Voreinstellung</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="vanished">Abbrechen</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="vanished">Rückgängig</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="vanished">Wiederholen</translation>
    </message>
    <message>
        <source>Completion Mode</source>
        <translation type="vanished">Fertigstellungsmodus</translation>
    </message>
    <message>
        <source>Limited strict completion</source>
        <translation type="vanished">Eingeschränkte strenge Fertigstellung</translation>
    </message>
    <message>
        <source>Strict completion</source>
        <translation type="vanished">Strenge Fertigstellung</translation>
    </message>
    <message>
        <source>Limited full completion</source>
        <translation type="vanished">Eingeschränkte vollständige Fertigstellung</translation>
    </message>
    <message>
        <source>Full completion</source>
        <translation type="vanished">Vollständige Fertigstellung</translation>
    </message>
    <message>
        <source>No completion</source>
        <translation type="vanished">Keine Fertigstellung</translation>
    </message>
    <message>
        <source>Delayed completion</source>
        <translation type="vanished">Verzögertes Fertigstellung</translation>
    </message>
    <message>
        <source>Customize completion…</source>
        <translation type="vanished">Fertigstellung anpassen...</translation>
    </message>
    <message>
        <source>Save Mode</source>
        <translation type="vanished">Modus speichern</translation>
    </message>
    <message>
        <source>Preset mode cannot be overwritten.</source>
        <translation type="vanished">Der voreingestellte Modus kann nicht überschrieben werden.</translation>
    </message>
    <message>
        <source>Delete Mode</source>
        <translation type="vanished">Modus löschen</translation>
    </message>
    <message>
        <source>Couldn&apos;t write preferences to
%s</source>
        <translation type="vanished">Konnte Einstellungen nicht schreiben in
%s</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>never</source>
        <translation type="vanished">nie</translation>
    </message>
    <message>
        <source>ask</source>
        <translation type="vanished">fragen</translation>
    </message>
    <message numerus="yes">
        <source>%i day</source>
        <translation type="vanished">
            <numerusform>%i Tag</numerusform>
            <numerusform>%i Tage</numerusform>
        </translation>
        <extra-po-flags>c-format</extra-po-flags>
        <extra-po-msgid_plural>%i days</extra-po-msgid_plural>
    </message>
    <message>
        <source>Copied</source>
        <extracomment>Result was copied</extracomment>
        <translation type="vanished">Kopiert</translation>
    </message>
    <message>
        <source>log10 function not found.</source>
        <translation type="vanished">log10 Funktion nicht gefunden.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Suche</translation>
    </message>
    <message>
        <source>_Search</source>
        <translation type="vanished">_Suche</translation>
    </message>
    <message>
        <source>Remove Bookmark</source>
        <translation type="vanished">Lesezeichen entfernen</translation>
    </message>
    <message>
        <source>Add Bookmark</source>
        <translation type="vanished">Lesezeichen hinzufügen</translation>
    </message>
    <message>
        <source>A bookmark with the selected name already exists.
Do you want to overwrite it?</source>
        <translation type="vanished">Es existiert bereits ein Lesezeichen mit dem gewählten Namen.
Möchten Sie es überschreiben?</translation>
    </message>
    <message>
        <source>No items found</source>
        <translation type="vanished">Keine Einträge gefunden</translation>
    </message>
    <message>
        <source>Select date</source>
        <translation type="vanished">Datum auswählen</translation>
    </message>
    <message>
        <source>Rectangular form</source>
        <translation type="vanished">Rechteckige Form</translation>
    </message>
    <message>
        <source>Exponential form</source>
        <translation type="vanished">Exponentialform</translation>
    </message>
    <message>
        <source>Polar form</source>
        <translation type="vanished">Polarform</translation>
    </message>
    <message>
        <source>Angle/phasor notation</source>
        <translation type="vanished">Winkel/Phasenschreibweise</translation>
    </message>
    <message>
        <source>Optimal unit</source>
        <translation type="vanished">Optimale Einheit</translation>
    </message>
    <message>
        <source>Optimal prefix</source>
        <translation type="vanished">Optimales Präfix</translation>
    </message>
    <message>
        <source>All functions</source>
        <translation type="vanished">Alle Funktionen</translation>
    </message>
    <message>
        <source>All variables</source>
        <translation type="vanished">Alle Variablen</translation>
    </message>
    <message>
        <source>Select definitions file</source>
        <translation type="vanished">Definitionsdatei auswählen</translation>
    </message>
    <message>
        <source>_Import</source>
        <translation type="vanished">_Importieren</translation>
    </message>
    <message>
        <source>Could not copy %s to %s.</source>
        <translation type="vanished">Konnte %s nicht nach %s kopieren.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Could not read %s.</source>
        <translation type="vanished">Konnte %s nicht lesen.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Could not copy file to %s.</source>
        <translation type="vanished">Konnte Datei nicht nach %s kopieren.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Unsupported base.</source>
        <translation type="vanished">Nicht unterstützte Basis.</translation>
    </message>
    <message>
        <source>The selected Chinese year does not exist.</source>
        <translation type="vanished">Das ausgewählte chinesische Jahr existiert nicht.</translation>
    </message>
    <message>
        <source>Conversion to Gregorian calendar failed.</source>
        <translation type="vanished">Die Konvertierung in den gregorianischen Kalender ist fehlgeschlagen.</translation>
    </message>
    <message>
        <source>Calendar conversion failed for: %s.</source>
        <translation type="vanished">Kalenderkonvertierung fehlgeschlagen für: %s.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Gnuplot was not found.</source>
        <translation type="vanished">Gnuplot wurde nicht gefunden.</translation>
    </message>
    <message>
        <source>%s (%s) needs to be installed separately, and found in the executable search path, for plotting to work.</source>
        <translation type="vanished">%s (%s) muss separat installiert werden und im Such-pfad für ausführbare Dateien gefunden werden, damit das Plotten funktioniert.</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Select file to save PNG image to</source>
        <translation type="vanished">Datei zum Speichern des PNG-Bildes auswählen</translation>
    </message>
    <message>
        <source>Allowed File Types</source>
        <translation type="vanished">Erlaubte Dateitypen</translation>
    </message>
    <message>
        <source>All Files</source>
        <translation type="vanished">Alle Dateien</translation>
    </message>
    <message>
        <source>Cannot delete unit as it is needed by other units.</source>
        <extracomment>do not delete units that are used by other units</extracomment>
        <translation type="vanished">Kann Einheit nicht löschen, da sie von anderen Einheiten benötigt wird.</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="vanished">keine</translation>
    </message>
    <message>
        <source>result is too long</source>
        <translation type="vanished">Ergebnis ist zu lang</translation>
    </message>
    <message>
        <source>translator-credits</source>
        <translation type="vanished">Alexander Krause &lt;akay.devel@protonmail.com&gt;
leveltrauma &lt;rhade_tm@t-online.de&gt;</translation>
    </message>
    <message>
        <source>Mode not found.</source>
        <translation type="vanished">Modus nicht gefunden.</translation>
    </message>
    <message>
        <source>Elements (in horizontal order)</source>
        <translation type="vanished">Elemente (in horizontaler Reihenfolge)</translation>
    </message>
    <message>
        <source>Select file to import</source>
        <translation type="vanished">Datei zum Importieren auswählen</translation>
    </message>
    <message>
        <source>_Open</source>
        <translation type="vanished">_Öffnen</translation>
    </message>
    <message>
        <source>Select file to export to</source>
        <translation type="vanished">Datei zum Exportieren wählen</translation>
    </message>
    <message>
        <source>A conflicting object with the same name exists. If you proceed and save changes, the conflicting object will be overwritten or deactivated.
Do you want to proceed?</source>
        <translation type="vanished">Es existiert ein kollidierendes Objekt mit demselben Namen. Wenn Sie fortfahren und die Änderungen speichern, wird das kollidierende Objekt überschrieben oder deaktiviert.
Möchten Sie fortfahren?</translation>
    </message>
    <message>
        <source>Set key combination</source>
        <translation type="vanished">Tastenkombination einstellen</translation>
    </message>
    <message>
        <source>Press the key combination you wish to use for the action
(press Escape to cancel).</source>
        <extracomment>Make the line reasonably long, but not to short (at least around 40 characters)</extracomment>
        <translation type="vanished">Drücken Sie die Tastenkombination, die Sie für die Aktion verwenden möchten
(zum Abbrechen Escape drücken).</translation>
    </message>
    <message>
        <source>No keys</source>
        <translation type="vanished">Keine Tasten</translation>
    </message>
    <message>
        <source>Empty value.</source>
        <translation type="vanished">Leerer Wert.</translation>
    </message>
    <message>
        <source>Function not found.</source>
        <translation type="vanished">Funktion nicht gefunden.</translation>
    </message>
    <message>
        <source>Variable not found.</source>
        <translation type="vanished">Variable nicht gefunden.</translation>
    </message>
    <message>
        <source>Unit not found.</source>
        <translation type="vanished">Einheit nicht gefunden.</translation>
    </message>
    <message>
        <source>The key combination is already in use.
Do you wish to replace the current action?</source>
        <translation type="vanished">Die Tastenkombination ist bereits in Gebrauch.
Möchten Sie die aktuelle Aktion ersetzen?</translation>
    </message>
    <message>
        <source>Select file to export</source>
        <translation type="vanished">Datei zum Exportieren auswählen</translation>
    </message>
    <message>
        <source>Empty expression.</source>
        <translation type="vanished">Leerer Ausdruck.</translation>
    </message>
    <message>
        <source>Empty x variable.</source>
        <translation type="vanished">Leere Variable x.</translation>
    </message>
    <message>
        <source>Element Data</source>
        <translation type="vanished">Element Daten</translation>
    </message>
    <message>
        <source>Classification</source>
        <translation type="vanished">Klassifizierung</translation>
    </message>
    <message>
        <source>Alkali Metal</source>
        <translation type="vanished">Alkalimetall</translation>
    </message>
    <message>
        <source>Alkaline-Earth Metal</source>
        <translation type="vanished">Erdalkalimetall</translation>
    </message>
    <message>
        <source>Lanthanide</source>
        <translation type="vanished">Lanthanid</translation>
    </message>
    <message>
        <source>Actinide</source>
        <translation type="vanished">Aktinid</translation>
    </message>
    <message>
        <source>Transition Metal</source>
        <translation type="vanished">Übergangsmetall</translation>
    </message>
    <message>
        <source>Metal</source>
        <translation type="vanished">Metall</translation>
    </message>
    <message>
        <source>Metalloid</source>
        <translation type="vanished">Halbmetall</translation>
    </message>
    <message>
        <source>Polyatomic Non-Metal</source>
        <translation type="vanished">Polyatomares Nicht-Metall</translation>
    </message>
    <message>
        <source>Diatomic Non-Metal</source>
        <translation type="vanished">Diatomares Nichtmetall</translation>
    </message>
    <message>
        <source>Noble Gas</source>
        <translation type="vanished">Edelgas</translation>
    </message>
    <message>
        <source>Unknown chemical properties</source>
        <translation type="vanished">Unbekannte chemische Eigenschaften</translation>
    </message>
    <message>
        <source>No unknowns in result.</source>
        <translation type="vanished">Keine Unbekannten im Ergebnis.</translation>
    </message>
    <message>
        <source>Set Unknowns</source>
        <translation type="vanished">Unbekannte setzen</translation>
    </message>
    <message>
        <source>Copy result to clipboard</source>
        <translation type="vanished">Ergebnis in die Zwischenablage kopieren</translation>
    </message>
</context>
<context>
    <name>CSVDialog</name>
    <message>
        <location filename="../src/csvdialog.cpp" line="33"/>
        <source>Import CSV File</source>
        <translation>CSV-Datei importieren</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="33"/>
        <source>Export CSV File</source>
        <translation>CSV-Datei exportieren</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="41"/>
        <source>Current result</source>
        <translation>Aktuelles Ergebnis</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="44"/>
        <source>Matrix/vector variable:</source>
        <translation>Matrix/Vektor-Variable:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="56"/>
        <source>File:</source>
        <translation>Datei:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="66"/>
        <source>Import as</source>
        <translation>Importieren als</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="70"/>
        <source>matrix</source>
        <translation>Matrix</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="72"/>
        <source>vectors</source>
        <translation>Vektoren</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="75"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="78"/>
        <source>First row:</source>
        <translation>Erste Zeile:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="82"/>
        <source>Includes headings</source>
        <translation>Enthält Überschriften</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="85"/>
        <source>Delimiter:</source>
        <translation>Begrenzungszeichen:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="87"/>
        <source>Comma</source>
        <translation>Komma</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="88"/>
        <source>Tabulator</source>
        <translation>Tabulator</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="89"/>
        <source>Semicolon</source>
        <translation>Semikolon</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="90"/>
        <source>Space</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="91"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="149"/>
        <source>Question</source>
        <translation type="unfinished">Frage</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="149"/>
        <source>A unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation>Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <source>An unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation type="vanished">Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="155"/>
        <location filename="../src/csvdialog.cpp" line="169"/>
        <location filename="../src/csvdialog.cpp" line="177"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="155"/>
        <source>Could not import from file 
%1</source>
        <translation>Konnte nicht aus Datei importieren
%1</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="169"/>
        <source>No matrix or vector variable with the entered name was found.</source>
        <translation type="unfinished">Keine Matrix- oder Vektorvariable mit eingegebenem Namen gefunden.</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="177"/>
        <source>Could not export to file 
%1</source>
        <translation>Konnte nicht in eine Datei exportieren 
%1</translation>
    </message>
</context>
<context>
    <name>CalendarConversionDialog</name>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="27"/>
        <source>Calendar Conversion</source>
        <translation>Kalender Konvertierung</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="33"/>
        <location filename="../src/calendarconversiondialog.cpp" line="130"/>
        <source>Gregorian</source>
        <translation>Gregorianisch</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="34"/>
        <location filename="../src/calendarconversiondialog.cpp" line="131"/>
        <source>Hebrew</source>
        <translation>Hebräisch</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="35"/>
        <location filename="../src/calendarconversiondialog.cpp" line="132"/>
        <source>Islamic (Hijri)</source>
        <translation>Islamisch (Hijri)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="36"/>
        <location filename="../src/calendarconversiondialog.cpp" line="133"/>
        <source>Persian (Solar Hijri)</source>
        <translation>Persisch (Solar Hijri)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="37"/>
        <location filename="../src/calendarconversiondialog.cpp" line="134"/>
        <source>Indian (National)</source>
        <translation>Indisch (National)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="38"/>
        <location filename="../src/calendarconversiondialog.cpp" line="135"/>
        <source>Chinese</source>
        <translation>Chinesisch</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="39"/>
        <location filename="../src/calendarconversiondialog.cpp" line="136"/>
        <source>Julian</source>
        <translation>Julianisch</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="40"/>
        <location filename="../src/calendarconversiondialog.cpp" line="137"/>
        <source>Revised Julian (Milanković)</source>
        <translation>Neujulianisch (Milanković)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="41"/>
        <location filename="../src/calendarconversiondialog.cpp" line="138"/>
        <source>Coptic</source>
        <translation>Koptisch</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="42"/>
        <location filename="../src/calendarconversiondialog.cpp" line="139"/>
        <source>Ethiopian</source>
        <translation>Äthiopisch</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="86"/>
        <location filename="../src/calendarconversiondialog.cpp" line="98"/>
        <location filename="../src/calendarconversiondialog.cpp" line="143"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="86"/>
        <source>The selected Chinese year does not exist.</source>
        <translation>Das ausgewählte chinesische Jahr existiert nicht.</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="98"/>
        <source>Conversion to Gregorian calendar failed.</source>
        <translation>Die Konvertierung in den gregorianischen Kalender ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="143"/>
        <source>Calendar conversion failed for: %1.</source>
        <translation>Kalenderkonvertierung fehlgeschlagen für: %1.</translation>
    </message>
</context>
<context>
    <name>ExpressionEdit</name>
    <message>
        <location filename="../src/expressionedit.cpp" line="1035"/>
        <source>matrix</source>
        <translation>matrix</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1037"/>
        <source>vector</source>
        <translation>vektor</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1048"/>
        <source>positive</source>
        <translation>positiv</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1049"/>
        <source>non-positive</source>
        <translation>nicht-positiv</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1050"/>
        <source>negative</source>
        <translation>negativ</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1051"/>
        <source>non-negative</source>
        <translation>nicht-negativ</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1052"/>
        <source>non-zero</source>
        <translation>nicht-null</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1057"/>
        <source>boolean</source>
        <translation>boolesch</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1058"/>
        <source>integer</source>
        <translation>ganzzahlig</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1059"/>
        <source>rational</source>
        <translation>rational</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1060"/>
        <source>real</source>
        <translation>reell</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1061"/>
        <source>complex</source>
        <translation>komplex</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1062"/>
        <source>number</source>
        <translation>zahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1063"/>
        <source>(not matrix)</source>
        <translation>(nicht Matrix)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1066"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1068"/>
        <source>default assumptions</source>
        <translation>standardmäßige Annahmen</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation type="vanished">Präfix</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1182"/>
        <source>Prefix:</source>
        <translation>Präfix:</translation>
    </message>
    <message>
        <source>Complex angle/phasor notation</source>
        <translation type="vanished">Komplexe Winkel-/Phasenschreibweise</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1240"/>
        <source>Base units</source>
        <translation>Basiseinheiten</translation>
    </message>
    <message>
        <source>Number base</source>
        <translation type="vanished">Zahlenbasis</translation>
    </message>
    <message>
        <source>Bijective base-26</source>
        <translation type="vanished">Bijektive Basis-26</translation>
    </message>
    <message>
        <source>Binary number</source>
        <translation type="obsolete">Binäre Zahlen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1248"/>
        <source>Calendars</source>
        <translation>Kalendarien</translation>
    </message>
    <message>
        <source>Complex cis form</source>
        <translation type="vanished">Komplexe cis-Form</translation>
    </message>
    <message>
        <source>Decimal number</source>
        <translation type="vanished">Dezimalzahl</translation>
    </message>
    <message>
        <source>Duodecimal number</source>
        <translation type="vanished">Duodezimale Zahl</translation>
    </message>
    <message>
        <source>Complex exponential form</source>
        <translation type="vanished">Komplexe Exponentialform</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1258"/>
        <source>Factors</source>
        <translation>Faktoren</translation>
    </message>
    <message>
        <source>16-bit floating point binary format</source>
        <translation type="vanished">16-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <source>32-bit floating point binary format</source>
        <translation type="vanished">32-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <source>64-bit floating point binary format</source>
        <translation type="vanished">64-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <source>80-bit (x86) floating point binary format</source>
        <translation type="vanished">80-Bit (x86) Fließkomma-Binärformat</translation>
    </message>
    <message>
        <source>128-bit floating point binary format</source>
        <translation type="vanished">128-Bit Fließkomma-Binärformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1270"/>
        <source>Fraction</source>
        <translation>Bruchteil</translation>
    </message>
    <message>
        <source>Hexadecimal number</source>
        <translation type="vanished">Hexadezimalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1274"/>
        <source>Latitude</source>
        <translation>Breitengrad</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1276"/>
        <source>Longitude</source>
        <translation>Längengrad</translation>
    </message>
    <message>
        <source>Mixed units</source>
        <translation type="vanished">Gemischte Einheiten</translation>
    </message>
    <message>
        <source>Octal number</source>
        <translation type="vanished">Oktalzahl</translation>
    </message>
    <message>
        <source>Optimal units</source>
        <translation type="vanished">Optimale Einheiten</translation>
    </message>
    <message>
        <source>Expanded partial fractions</source>
        <translation type="vanished">Erweiterte Teilbrüche</translation>
    </message>
    <message>
        <source>Complex polar form</source>
        <translation type="vanished">Komplexe Polarform</translation>
    </message>
    <message>
        <source>Complex rectangular form</source>
        <translation type="vanished">Komplexe Rechteckform</translation>
    </message>
    <message>
        <source>Roman numerals</source>
        <translation type="vanished">Römische Ziffern</translation>
    </message>
    <message>
        <source>Sexagesimal number</source>
        <translation type="vanished">Sexagesimalzahl</translation>
    </message>
    <message>
        <source>Time format</source>
        <translation type="vanished">Zeitformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1296"/>
        <location filename="../src/expressionedit.cpp" line="2131"/>
        <source>Unicode</source>
        <translation>Unicode</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1656"/>
        <source>Use input method</source>
        <translation type="unfinished">Eingabemethode verwenden</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2161"/>
        <source>UTC time zone</source>
        <translation>UTC-Zeitzone</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1606"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1236"/>
        <source>Complex Angle/Phasor Notation</source>
        <translation>Komplexe Winkel-/Phasenschreibweise</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1242"/>
        <source>Number Base</source>
        <translation>Zahlenbasis</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1244"/>
        <source>Bijective Base-26</source>
        <translation>Bijektive Basis-26</translation>
    </message>
    <message>
        <source>Binary Bumber</source>
        <translation type="obsolete">Binäre Zahlen</translation>
    </message>
    <message>
        <source>Complex Cis Form</source>
        <translation type="vanished">Komplexe cis-Form</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1246"/>
        <source>Binary Number</source>
        <translation type="unfinished">Binärezahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1250"/>
        <source>Complex cis Form</source>
        <translation>Komplexe cis-Form</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1252"/>
        <source>Decimal Number</source>
        <translation type="unfinished">Dezimalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1254"/>
        <source>Duodecimal Number</source>
        <translation>Duodezimale Zahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1256"/>
        <source>Complex Exponential Form</source>
        <translation>Komplexe Exponentialform</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1260"/>
        <source>16-bit Floating Point Binary Format</source>
        <translation>16-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1262"/>
        <source>32-bit Floating Point Binary Format</source>
        <translation>32-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1264"/>
        <source>64-bit Floating Point Binary Format</source>
        <translation>64-Bit-Gleitkomma-Binärformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1266"/>
        <source>80-bit (x86) Floating Point Binary Format</source>
        <translation>80-Bit (x86) Fließkomma-Binärformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1268"/>
        <source>128-bit Floating Point Binary Format</source>
        <translation>128-Bit Fließkomma-Binärformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1272"/>
        <source>Hexadecimal Number</source>
        <translation type="unfinished">Hexadezimalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1278"/>
        <source>Mixed Units</source>
        <translation>Gemischte Einheiten</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1280"/>
        <source>Octal Number</source>
        <translation type="unfinished">Oktalzahl</translation>
    </message>
    <message>
        <source>Optimal Units</source>
        <translation type="vanished">Optimale Einheiten</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1282"/>
        <source>Optimal Unit</source>
        <translation type="unfinished">Optimale Einheit</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1284"/>
        <source>Expanded Partial Fractions</source>
        <translation>Erweiterte Teilbrüche</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1286"/>
        <source>Complex Polar Form</source>
        <translation>Komplexe Polarform</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1288"/>
        <source>Complex Rectangular Form</source>
        <translation type="unfinished">Komplexe Rechtwinklige Form</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1290"/>
        <source>Roman Numerals</source>
        <translation>Römische Ziffern</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1292"/>
        <source>Sexagesimal Number</source>
        <translation type="unfinished">Sexagesimalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1294"/>
        <source>Time Format</source>
        <translation>Zeitformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1298"/>
        <source>UTC Time Zone</source>
        <translation>UTC-Zeitzone</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1609"/>
        <source>Redo</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1613"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1616"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1619"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1622"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1626"/>
        <source>Insert Date…</source>
        <translation>Datum einfügen...</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1627"/>
        <source>Insert Matrix…</source>
        <translation>Matrix einfügen...</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1629"/>
        <source>Select All</source>
        <translation>Alles markieren</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1632"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1636"/>
        <source>Completion</source>
        <translation>Vervollständigung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1648"/>
        <source>No completion</source>
        <translation>Keine Fertigstellung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1649"/>
        <source>Limited strict completion</source>
        <translation>Eingeschränkte strenge Fertigstellung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1650"/>
        <source>Strict completion</source>
        <translation>Strenge Fertigstellung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1651"/>
        <source>Limited full completion</source>
        <translation>Eingeschränkte vollständige Fertigstellung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1652"/>
        <source>Full completion</source>
        <translation>Vollständige Fertigstellung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1654"/>
        <source>Delayed completion</source>
        <translation>Verzögertes Fertigstellung</translation>
    </message>
    <message>
        <source>Enable input method</source>
        <translation type="obsolete">Eingabemethode aktivieren</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1689"/>
        <source>Matrix</source>
        <translation>Matrix</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1833"/>
        <source>Too many arguments for %1().</source>
        <translation>Zu viele Argumente für %1().</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1858"/>
        <source>argument</source>
        <translation>argument</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1875"/>
        <source>%1:</source>
        <translation type="unfinished">%1:</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1929"/>
        <source>MC (memory clear)</source>
        <translation>MC (Speicher löschen)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1932"/>
        <source>MS (memory store)</source>
        <translation>MS (Speicher speichern)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1935"/>
        <source>M+ (memory plus)</source>
        <translation>M+ (Speicher plus)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1938"/>
        <source>M− (memory minus)</source>
        <translation>M- (Speicher minus)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1946"/>
        <location filename="../src/expressionedit.cpp" line="1948"/>
        <source>factorize</source>
        <translation>faktorisieren</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1949"/>
        <location filename="../src/expressionedit.cpp" line="1951"/>
        <source>expand</source>
        <translation>erweitern</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2098"/>
        <source>hexadecimal</source>
        <translation>hexadezimal</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2099"/>
        <location filename="../src/expressionedit.cpp" line="2209"/>
        <source>hexadecimal number</source>
        <translation>hexadezimale zahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2100"/>
        <source>octal</source>
        <translation>oktal</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2101"/>
        <source>octal number</source>
        <translation>oktalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2102"/>
        <source>decimal</source>
        <translation>dezimal</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2103"/>
        <source>decimal number</source>
        <translation>dezimalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2104"/>
        <source>duodecimal</source>
        <translation>duodezimal</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2105"/>
        <source>duodecimal number</source>
        <translation>duodezimalzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2106"/>
        <source>binary</source>
        <translation>binär</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2107"/>
        <location filename="../src/expressionedit.cpp" line="2203"/>
        <source>binary number</source>
        <translation>binärzahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2108"/>
        <source>roman</source>
        <translation>römisch</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2109"/>
        <source>roman numerals</source>
        <translation>römische Ziffern</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2110"/>
        <source>bijective</source>
        <translation>bijektiv</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2111"/>
        <source>bijective base-26</source>
        <translation>bijektive basis-26</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2112"/>
        <source>sexagesimal</source>
        <translation>sexagesimal</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2113"/>
        <source>sexagesimal number</source>
        <translation>sexagesimale Zahl</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2114"/>
        <location filename="../src/expressionedit.cpp" line="2115"/>
        <source>latitude</source>
        <translation>breitengrad</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2116"/>
        <location filename="../src/expressionedit.cpp" line="2117"/>
        <source>longitude</source>
        <translation>längengrad</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2119"/>
        <source>32-bit floating point</source>
        <translation>32-Bit-Gleitkomma</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2121"/>
        <source>64-bit floating point</source>
        <translation>64-Bit-Gleitkomma</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2123"/>
        <source>16-bit floating point</source>
        <translation>16-Bit Gleitkomma</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2125"/>
        <source>80-bit (x86) floating point</source>
        <translation>80-Bit (x86) Gleitkomma</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2127"/>
        <source>128-bit floating point</source>
        <translation>128-Bit Gleitkomma</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2128"/>
        <source>time</source>
        <translation>zeit</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2129"/>
        <source>time format</source>
        <translation>zeitformat</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2132"/>
        <source>bases</source>
        <translation>basen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2133"/>
        <source>number bases</source>
        <translation>zahlenbasen</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2134"/>
        <location filename="../src/expressionedit.cpp" line="2135"/>
        <source>calendars</source>
        <translation>kalendarien</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2136"/>
        <source>optimal</source>
        <translation>optimal</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2137"/>
        <source>optimal unit</source>
        <translation>optimale einheit</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2138"/>
        <location filename="../src/expressionedit.cpp" line="2213"/>
        <source>base</source>
        <translation>basis</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2139"/>
        <source>base units</source>
        <translation>basiseinheiten</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2140"/>
        <source>mixed</source>
        <translation>gemischt</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2141"/>
        <source>mixed units</source>
        <translation>gemischte einheiten</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2142"/>
        <location filename="../src/expressionedit.cpp" line="2143"/>
        <source>fraction</source>
        <translation>bruchteil</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2144"/>
        <location filename="../src/expressionedit.cpp" line="2145"/>
        <source>factors</source>
        <translation>faktoren</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2146"/>
        <source>partial fraction</source>
        <translation>teilbruch</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2147"/>
        <source>expanded partial fractions</source>
        <translation>erweiterte teilbrüche</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2148"/>
        <source>rectangular</source>
        <translation>rechtwinklig</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2148"/>
        <source>cartesian</source>
        <translation>kartesisch</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2149"/>
        <source>complex rectangular form</source>
        <translation>komplexe rechteckform</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2150"/>
        <source>exponential</source>
        <translation>exponentiell</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2151"/>
        <source>complex exponential form</source>
        <translation>komplexe exponentialform</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2152"/>
        <source>polar</source>
        <translation>polar</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2153"/>
        <source>complex polar form</source>
        <translation>komplexe polarform</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2155"/>
        <source>complex cis form</source>
        <translation>komplexe cis-form</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2156"/>
        <source>angle</source>
        <translation>winkel</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2157"/>
        <source>complex angle notation</source>
        <translation>komplexe winkeldarstellung</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2158"/>
        <source>phasor</source>
        <translation>phase</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2159"/>
        <source>complex phasor notation</source>
        <translation>komplexe Phasenschreibweise</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2214"/>
        <source>number base %1</source>
        <translation>zahlenbasis %1</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2476"/>
        <source>Data object</source>
        <translation>Daten-Objekt</translation>
    </message>
</context>
<context>
    <name>FPConversionDialog</name>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="28"/>
        <source>Floating Point Conversion (IEEE 754)</source>
        <translation>Gleitkomma-Konvertierung (IEEE 754)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="31"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="33"/>
        <source>16-bit (half precision)</source>
        <translation>16-Bit (halbe Genauigkeit)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="34"/>
        <source>32-bit (single precision)</source>
        <translation>32-Bit (einfache Genauigkeit)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="35"/>
        <source>64-bit (double precision)</source>
        <translation>64-Bit (doppelte Genauigkeit)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="36"/>
        <source>80-bit (x86 extended format)</source>
        <translation>80-Bit (erweitertes x86-Format)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="37"/>
        <source>128-bit (quadruple precision)</source>
        <translation>128-Bit (vierfache Genauigkeit)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="39"/>
        <source>Decimal value</source>
        <translation>Dezimaler Wert</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="41"/>
        <source>Binary representation</source>
        <translation>Binäre Darstellung</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="56"/>
        <source>Hexadecimal representation</source>
        <translation>Hexadezimale Darstellung</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="58"/>
        <source>Floating point value</source>
        <translation>Gleitkommawert</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="61"/>
        <source>Conversion error</source>
        <translation>Konvertierungsfehler</translation>
    </message>
</context>
<context>
    <name>FunctionEditDialog</name>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="84"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="87"/>
        <source>Expression:</source>
        <translation>Ausdruck:</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="92"/>
        <source>Argument references:</source>
        <translation type="unfinished">Argumentreferenzen:</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="93"/>
        <source>x, y, z</source>
        <translation>x, y, z</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="95"/>
        <source>\x, \y, \z, \a, \b, …</source>
        <translation>\x, \y, \z, \a, \b, …</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="115"/>
        <location filename="../src/functioneditdialog.cpp" line="148"/>
        <source>Question</source>
        <translation type="unfinished">Frage</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="115"/>
        <location filename="../src/functioneditdialog.cpp" line="148"/>
        <source>A function with the same name already exists.
Do you want to overwrite the function?</source>
        <translation>Eine Funktion mit demselben Namen existiert bereits.
Möchten Sie die Funktion überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="222"/>
        <source>Edit Function</source>
        <translation>Bearbeite Funktion</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="235"/>
        <source>New Function</source>
        <translation>Neue Funktion</translation>
    </message>
</context>
<context>
    <name>FunctionsDialog</name>
    <message>
        <location filename="../src/functionsdialog.cpp" line="34"/>
        <source>Functions</source>
        <translation>Funktionen</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="43"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="60"/>
        <location filename="../src/functionsdialog.cpp" line="496"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="81"/>
        <source>New…</source>
        <translation>Neu…</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="82"/>
        <source>Edit…</source>
        <translation>Bearbeiten…</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="83"/>
        <location filename="../src/functionsdialog.cpp" line="428"/>
        <location filename="../src/functionsdialog.cpp" line="431"/>
        <source>Deactivate</source>
        <translation>Deaktivieren</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="84"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="86"/>
        <source>Calculate…</source>
        <translation>Berechnen…</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="88"/>
        <source>Apply</source>
        <translation type="unfinished">Anwenden</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="89"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="313"/>
        <source>argument</source>
        <translation>argument</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="336"/>
        <source>Retrieves data from the %1 data set for a given object and property. If &quot;info&quot; is typed as property, a dialog window will pop up with all properties of the object.</source>
        <translation>Ruft Daten aus dem %1-Datensatz für ein angegebenes Objekt und eine Eigenschaft ab. Wenn &quot;info&quot; als Eigenschaft eingegeben wird, wird ein Dialogfenster mit allen Eigenschaften des Objekts angezeigt.</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="346"/>
        <source>Example:</source>
        <translation>Beispiel:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="358"/>
        <source>Arguments</source>
        <translation>Argumente</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="375"/>
        <source>optional</source>
        <comment>optional argument</comment>
        <translation>optional</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="378"/>
        <source>default:</source>
        <comment>argument default</comment>
        <translation type="unfinished">standard:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="390"/>
        <source>Requirement:</source>
        <comment>Required condition for function</comment>
        <translation type="unfinished">Bedingung:</translation>
    </message>
    <message>
        <source>default: </source>
        <comment>argument default</comment>
        <translation type="vanished">standard: </translation>
    </message>
    <message>
        <source>Requirement</source>
        <translation type="vanished">Bedingung</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="398"/>
        <source>Properties</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="405"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="415"/>
        <source>key</source>
        <extracomment>indicating that the property is a data set key</extracomment>
        <translation type="unfinished">Schlüssel</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="433"/>
        <source>Activate</source>
        <translation type="unfinished">Aktivieren</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="553"/>
        <source>All</source>
        <comment>All functions</comment>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="596"/>
        <source>Uncategorized</source>
        <translation type="unfinished">Nicht kategorisierte</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="602"/>
        <source>User functions</source>
        <translation type="unfinished">Benutzerfunktionen</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="277"/>
        <location filename="../src/functionsdialog.cpp" line="609"/>
        <source>Inactive</source>
        <translation type="unfinished">Inaktive</translation>
    </message>
</context>
<context>
    <name>HistoryView</name>
    <message>
        <location filename="../src/historyview.cpp" line="657"/>
        <source>Insert Value</source>
        <translation type="unfinished">Wert einfügen</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="658"/>
        <source>Insert Text</source>
        <translation type="unfinished">Text einfügen</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="659"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="662"/>
        <source>Copy Formatted Text</source>
        <translation type="unfinished">Formatierten Text kopieren</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="663"/>
        <source>Select All</source>
        <translation type="unfinished">Alles markieren</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="666"/>
        <source>Search…</source>
        <translation type="unfinished">Suchen...</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="670"/>
        <source>Protect</source>
        <translation type="unfinished">Schützen</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="672"/>
        <source>Move to Top</source>
        <translation type="unfinished">Nach oben verschieben</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="674"/>
        <source>Remove</source>
        <translation type="unfinished">Entfernen</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="675"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="726"/>
        <source>Text:</source>
        <translation type="unfinished">Text:</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="724"/>
        <location filename="../src/historyview.cpp" line="731"/>
        <source>Search</source>
        <translation type="unfinished">Suche</translation>
    </message>
</context>
<context>
    <name>KeypadButton</name>
    <message>
        <location filename="../src/keypadwidget.cpp" line="508"/>
        <source>&lt;i&gt;Right-click/long press&lt;/i&gt;: %1</source>
        <translation>&lt;i&gt;Rechtsklick/Lang drücken&lt;/i&gt;: %1</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="509"/>
        <source>&lt;i&gt;Right-click&lt;/i&gt;: %1</source>
        <translation>&lt;i&gt;Rechtsklick&lt;/i&gt;: %1</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="513"/>
        <source>&lt;i&gt;Middle-click&lt;/i&gt;: %1</source>
        <translation>&lt;i&gt;Mittelklick&lt;/i&gt;: %1</translation>
    </message>
</context>
<context>
    <name>KeypadWidget</name>
    <message>
        <location filename="../src/keypadwidget.cpp" line="158"/>
        <source>Memory store</source>
        <translation type="unfinished">Speicher speichern</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="161"/>
        <source>Memory clear</source>
        <translation type="unfinished">Speicher löschen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="167"/>
        <source>Memory recall</source>
        <translation type="unfinished">Speicherabruf</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="173"/>
        <source>Memory add</source>
        <translation type="unfinished">Speicher plus</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="173"/>
        <source>Memory subtract</source>
        <translation type="unfinished">Speicher minus</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="183"/>
        <source>sin</source>
        <translation>sin</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="185"/>
        <source>cos</source>
        <translation>cos</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="187"/>
        <source>tan</source>
        <translation>tan</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="192"/>
        <source>Exponentiation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="222"/>
        <source>Percent or remainder</source>
        <translation type="unfinished">Prozent oder Rest</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="224"/>
        <source>Uncertainty/interval</source>
        <translation type="unfinished">Ungenauigkeit/Intervall</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="224"/>
        <source>Relative error</source>
        <translation type="unfinished">Relativer Fehler</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="224"/>
        <source>Interval</source>
        <translation type="unfinished">Intervall</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="226"/>
        <source>Move cursor left</source>
        <translation type="unfinished">Mauszeiger nach links bewegen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="226"/>
        <source>Move cursor to start</source>
        <translation type="unfinished">Mauszeiger zum Anfang</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="232"/>
        <source>Move cursor right</source>
        <translation type="unfinished">Mauszeiger nach rechts bewegen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="232"/>
        <source>Move cursor to end</source>
        <translation type="unfinished">Mauszeiger zum Ende bewegen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="240"/>
        <source>Left parenthesis</source>
        <translation>Linke Klammer</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="240"/>
        <source>Left vector bracket</source>
        <translation>Linke Vektor-Klammer</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="242"/>
        <source>Right parenthesis</source>
        <translation>Rechte Klammer</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="242"/>
        <source>Right vector bracket</source>
        <translation>Rechte Vektor-Klammer</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="244"/>
        <source>Smart parentheses</source>
        <translation>Intelligente Klammern</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="244"/>
        <source>Vector brackets</source>
        <translation>Vektorielle Klammern</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="250"/>
        <source>Argument separator</source>
        <translation type="unfinished">Argument-Trennzeichen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="250"/>
        <location filename="../src/keypadwidget.cpp" line="269"/>
        <source>Blank space</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="250"/>
        <location filename="../src/keypadwidget.cpp" line="269"/>
        <source>New line</source>
        <translation>Neue Zeile</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="269"/>
        <source>Decimal point</source>
        <translation>Dezimalpunkt</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="297"/>
        <source>Previous result (static)</source>
        <translation type="unfinished">Vorheriges Ergebnis (statisch)</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="303"/>
        <source>Multiplication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="303"/>
        <source>Bitwise AND</source>
        <translation type="unfinished">Bitweise UND</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="303"/>
        <source>Bitwise Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="309"/>
        <source>Delete</source>
        <translation type="unfinished">Löschen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="309"/>
        <source>Backspace</source>
        <translation type="unfinished">Rücktaste</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="312"/>
        <source>Addition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="312"/>
        <source>Plus</source>
        <translation type="unfinished">Plus</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="317"/>
        <location filename="../src/keypadwidget.cpp" line="320"/>
        <source>Subtraction</source>
        <translation type="unfinished">Subtraktion</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="317"/>
        <location filename="../src/keypadwidget.cpp" line="320"/>
        <source>Minus</source>
        <translation type="unfinished">Minus</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="324"/>
        <source>Division</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="324"/>
        <source>Bitwise OR</source>
        <translation type="unfinished">Bitweises ODER</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="324"/>
        <source>Bitwise NOT</source>
        <translation type="unfinished">Bitweises NICHT</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="326"/>
        <source>Clear expression</source>
        <translation>Ausdruck löschen</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="332"/>
        <source>Calculate expression</source>
        <translation>Ausdruck berechnen</translation>
    </message>
</context>
<context>
    <name>PlotDialog</name>
    <message>
        <location filename="../src/plotdialog.cpp" line="45"/>
        <source>Plot</source>
        <translation>Plotten</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="54"/>
        <source>Data</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="56"/>
        <location filename="../src/plotdialog.cpp" line="147"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="58"/>
        <source>Expression:</source>
        <translation>Ausdruck:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="63"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="64"/>
        <source>Vector/matrix</source>
        <translation>Vektor/Matrix</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="65"/>
        <source>Paired matrix</source>
        <translation>Gepaarte Matrix</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="68"/>
        <source>Rows</source>
        <translation>Zeilen</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="69"/>
        <source>X variable:</source>
        <translation>X Variable:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="74"/>
        <source>Style:</source>
        <translation>Stil:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="76"/>
        <source>Line</source>
        <translation>Zeile</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="77"/>
        <source>Points</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="78"/>
        <source>Line with points</source>
        <translation>Linie mit Punkten</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="79"/>
        <source>Boxes/bars</source>
        <translation>Boxen/Balken</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="80"/>
        <source>Histogram</source>
        <translation>Histogramm</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="81"/>
        <source>Steps</source>
        <translation>Stufen</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="82"/>
        <source>Candlesticks</source>
        <translation>Kerzenständer</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="83"/>
        <source>Dots</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="85"/>
        <source>Smoothing:</source>
        <translation type="unfinished">Glättung:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="87"/>
        <location filename="../src/plotdialog.cpp" line="182"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="88"/>
        <source>Monotonic</source>
        <translation>Monoton</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="89"/>
        <source>Natural cubic splines</source>
        <translation>Natürliche kubische Splines</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="90"/>
        <source>Bezier</source>
        <translation>Bézier</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="91"/>
        <source>Bezier (monotonic)</source>
        <translation>Bézier (monoton)</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="93"/>
        <source>Y-axis:</source>
        <translation>Y-Achse:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="95"/>
        <source>Primary</source>
        <translation>Primär</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="96"/>
        <source>Secondary</source>
        <translation>Sekundär</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="101"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="104"/>
        <location filename="../src/plotdialog.cpp" line="140"/>
        <location filename="../src/plotdialog.cpp" line="190"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="107"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="vanished">Kategorie</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="114"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="114"/>
        <source>Expression</source>
        <translation>Ausdruck</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="120"/>
        <source>Function Range</source>
        <translation>Funktionsbereich</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="122"/>
        <source>Minimum x value:</source>
        <translation>Minimaler x-Wert:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="125"/>
        <source>Maximum x value:</source>
        <translation>Maximaler x-Wert:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="129"/>
        <source>Sampling rate:</source>
        <translation>Abtastrate:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="134"/>
        <source>Step size:</source>
        <translation>Schrittweite:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="145"/>
        <source>Appearance</source>
        <translation>Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="149"/>
        <source>Display grid</source>
        <translation>Raster anzeigen</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="151"/>
        <source>Display full border</source>
        <translation>Vollen Rand anzeigen</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="153"/>
        <source>Minimum y value:</source>
        <translation>Minimaler y-Wert:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="158"/>
        <source>Maximum y value:</source>
        <translation>Maximaler y-Wert:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="163"/>
        <source>Logarithmic x scale:</source>
        <translation>Logarithmische x-Skala:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="168"/>
        <source>Logarithmic y scale:</source>
        <translation>Logarithmische y-Skala:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="350"/>
        <location filename="../src/plotdialog.cpp" line="351"/>
        <source>Calculating…</source>
        <translation>Berechnen...</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="350"/>
        <location filename="../src/plotdialog.cpp" line="497"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="497"/>
        <source>Processing…</source>
        <translation>Verarbeitung...</translation>
    </message>
    <message>
        <source>Minimum y value</source>
        <translation type="vanished">Minimaler y-Wert</translation>
    </message>
    <message>
        <source>Maximum y value</source>
        <translation type="vanished">Maximaler y-Wert</translation>
    </message>
    <message>
        <source>Logarithmic x scale</source>
        <translation type="vanished">Logarithmische x-Skala</translation>
    </message>
    <message>
        <source>Logarithmic y scale</source>
        <translation type="vanished">Logarithmische y-Skala</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="173"/>
        <source>X-axis label:</source>
        <translation>X-Achsen-Beschriftung:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="175"/>
        <source>Y-axis label:</source>
        <translation>Y-Achsen-Beschriftung:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="177"/>
        <source>Line width:</source>
        <translation>Linienbreite:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="180"/>
        <source>Legend placement:</source>
        <translation>Platzierung der Legende:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="183"/>
        <source>Top-left</source>
        <translation>Oben-links</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="184"/>
        <source>Top-right</source>
        <translation>Oben-rechts</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="185"/>
        <source>Bottom-left</source>
        <translation>Unten-links</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="186"/>
        <source>Bottom-right</source>
        <translation>Unten-rechts</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="187"/>
        <source>Below</source>
        <translation>Unterhalb</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="188"/>
        <source>Outside</source>
        <translation>Außerhalb</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>It took too long to generate the plot data.</source>
        <translation type="vanished">Es hat zu lange gedauert, die Plotdaten zu generieren.</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="54"/>
        <source>Look &amp;&amp; Feel</source>
        <translation type="unfinished">Aussehen &amp;&amp; Bedienung</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="55"/>
        <source>Numbers &amp;&amp; Operators</source>
        <translation type="unfinished">Zahlen &amp;&amp; Operatoren</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="56"/>
        <source>Units &amp;&amp; Currencies</source>
        <translation type="unfinished">Einheiten &amp;&amp; Währungen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="57"/>
        <source>Parsing &amp;&amp; Calculation</source>
        <translation type="unfinished">Analysierung &amp;&amp; Berechnung</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="61"/>
        <source>Ignore system language (requires restart)</source>
        <translation>Systemsprache ignorieren (erfordert Neustart)</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="62"/>
        <source>Allow multiple instances</source>
        <translation>Mehrere Instanzen zulassen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="63"/>
        <source>Clear history on exit</source>
        <translation>Verlauf beim Beenden löschen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="64"/>
        <source>Keep above other windows</source>
        <translation type="unfinished">Fenster immer im Vordergrund halten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="65"/>
        <source>Window title:</source>
        <translation type="unfinished">Fenstertitel:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="67"/>
        <source>Application name</source>
        <translation>Name der Anwendung</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="68"/>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="69"/>
        <source>Application name + result</source>
        <translation>Anwendungsname + Ergebnis</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="73"/>
        <source>Style:</source>
        <translation>Stil:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="76"/>
        <source>Default (requires restart)</source>
        <comment>Default style</comment>
        <translation type="unfinished">Standard (erfordert Neustart)</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="81"/>
        <source>Dark mode</source>
        <translation type="unfinished">Dunkelmodus</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="82"/>
        <source>Colorize result</source>
        <translation type="unfinished">Ergebnis einfärben</translation>
    </message>
    <message>
        <source>Custom result font</source>
        <translation type="obsolete">Benutzerdefinierte Ergebnisschriftart</translation>
    </message>
    <message>
        <source>Custom expression font</source>
        <translation type="obsolete">Benutzerdefinierte Ausdrucksschriftart</translation>
    </message>
    <message>
        <source>Custom keypad font</source>
        <translation type="obsolete">Benutzerdefinierte Tastenfeldschriftart</translation>
    </message>
    <message>
        <source>Custom application font</source>
        <translation type="obsolete">Benutzerdefinierte Anwendungsschriftart</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="83"/>
        <source>Custom result font:</source>
        <translation>Benutzerdefinierte Ergebnisschriftart:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="86"/>
        <source>Custom expression font:</source>
        <translation>Benutzerdefinierte Ausdrucksschriftart:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="89"/>
        <source>Custom keypad font:</source>
        <translation>Benutzerdefinierte Tastenfeldschriftart:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="92"/>
        <source>Custom application font:</source>
        <translation>Benutzerdefinierte Anwendungsschriftart:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="98"/>
        <source>Display expression status</source>
        <translation type="unfinished">Anzeige des Ausdrucksstatus</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="101"/>
        <source>Delay:</source>
        <translation type="unfinished">Verzögern:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="111"/>
        <source>Expression after calculation:</source>
        <translation type="unfinished">Ausdruck nach Berechnung:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="113"/>
        <source>Keep expression</source>
        <translation type="unfinished">Ausdruck behalten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="114"/>
        <source>Clear expression</source>
        <translation type="unfinished">Ausdruck löschen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="115"/>
        <source>Replace with result</source>
        <translation type="unfinished">Durch Ergebnisse ersetzen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="116"/>
        <source>Replace with result if shorter</source>
        <translation type="unfinished">Durch Ergebnis ersetzen, falls kürzer</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="120"/>
        <source>Use keyboard keys for RPN</source>
        <translation type="unfinished">Tasten auf der Tastatur für RPN verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="121"/>
        <source>Parsing mode:</source>
        <translation type="unfinished">Analyse-Modus:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="123"/>
        <location filename="../src/preferencesdialog.cpp" line="172"/>
        <source>Adaptive</source>
        <translation type="unfinished">Adaptiv</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="124"/>
        <source>Conventional</source>
        <translation type="unfinished">Konventionell</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="125"/>
        <source>Implicit multiplication first</source>
        <translation type="unfinished">Implizite Multiplikation zuerst</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="126"/>
        <source>Chain</source>
        <translation type="unfinished">Verketteter</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="127"/>
        <source>RPN</source>
        <translation type="unfinished">RPN</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="132"/>
        <source>Read precision</source>
        <translation type="unfinished">Genauigkeit lesen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="133"/>
        <source>Limit implicit multiplication</source>
        <translation type="unfinished">Implizite Multiplikation begrenzen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="134"/>
        <source>Interval calculation:</source>
        <translation type="unfinished">Intervall-berechnung:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="136"/>
        <source>Variance formula</source>
        <translation type="unfinished">Varianz-Formel</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="137"/>
        <source>Interval arithmetic</source>
        <translation type="unfinished">Intervall-Arithmetik</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="141"/>
        <source>Factorize result</source>
        <translation type="unfinished">Ergebnis faktorisieren</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="144"/>
        <source>Binary two&apos;s complement representation</source>
        <translation type="unfinished">Binäre Zweierkomplement-Darstellung</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="145"/>
        <source>Hexadecimal two&apos;s complement representation</source>
        <translation type="unfinished">Hexadezimale Zweierkomplement-Darstellung</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="146"/>
        <source>Use lower case letters in non-decimal numbers</source>
        <translation type="unfinished">Kleinbuchstaben in Zahlen mit nicht-dezimaler Basis verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="149"/>
        <source>Spell out logical operators</source>
        <translation type="unfinished">Logische Operatoren ausbuchstabieren</translation>
    </message>
    <message>
        <source>Use E-notation instead of 10^x</source>
        <translation type="obsolete">E-Notation anstelle von 10^x verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="45"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="106"/>
        <source>ms</source>
        <extracomment>milliseconds</extracomment>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="147"/>
        <source>Use dot as multiplication sign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="148"/>
        <source>Use Unicode division slash in output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="150"/>
        <source>Use E-notation instead of 10^n</source>
        <translation type="unfinished">E-Notation anstelle von 10^n verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="151"/>
        <source>Use &apos;j&apos; as imaginary unit</source>
        <translation type="unfinished">&apos;j&apos; als imaginäre Einheit verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="152"/>
        <source>Use comma as decimal separator</source>
        <translation type="unfinished">Komma als Dezimaltrennzeichen verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="154"/>
        <source>Ignore comma in numbers</source>
        <translation type="unfinished">Komma in Zahlen ignorieren</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="155"/>
        <source>Ignore dots in numbers</source>
        <translation type="unfinished">Punkte in Zahlen ignorieren</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="156"/>
        <source>Round halfway numbers to even</source>
        <translation type="unfinished">Halbe Zahlen auf gerade Zahlen runden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="157"/>
        <source>Indicate repeating decimals</source>
        <translation>Wiederholte Dezimalstellen anzeigen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="162"/>
        <source>Digit grouping:</source>
        <translation type="unfinished">Zifferngruppierung:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="164"/>
        <source>None</source>
        <translation type="unfinished">Keine</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="165"/>
        <source>Standard</source>
        <translation type="unfinished">Standard</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="166"/>
        <source>Local</source>
        <translation type="unfinished">Lokal</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="170"/>
        <source>Interval display:</source>
        <translation type="unfinished">Intervall-anzeige:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="173"/>
        <source>Significant digits</source>
        <translation type="unfinished">Signifikante Ziffern</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="174"/>
        <source>Interval</source>
        <translation type="unfinished">Intervall</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="175"/>
        <source>Plus/minus</source>
        <translation type="unfinished">Plus/Minus</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="176"/>
        <source>Midpoint</source>
        <translation type="unfinished">Mittelwert</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="177"/>
        <source>Lower</source>
        <translation type="unfinished">Untere</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="178"/>
        <source>Upper</source>
        <translation type="unfinished">Obere</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="183"/>
        <source>Complex number form:</source>
        <translation type="unfinished">Komplexe form:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="185"/>
        <source>Rectangular</source>
        <translation type="unfinished">Algebraischen Form</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="186"/>
        <source>Exponential</source>
        <translation type="unfinished">Exponentialform</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="187"/>
        <source>Polar</source>
        <translation type="unfinished">Polarform</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="188"/>
        <source>Angle/phasor</source>
        <translation type="unfinished">Winkel/Phasenschreibweise</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="196"/>
        <source>Abbreviate names</source>
        <translation>Namen abkürzen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="197"/>
        <source>Use binary prefixes for information units</source>
        <translation type="unfinished">Binäre Präfixe für Informationseinheiten verwenden</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="198"/>
        <source>Automatic unit conversion:</source>
        <translation type="unfinished">Automatische Einheitenumrechnung:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="200"/>
        <source>No conversion</source>
        <translation type="unfinished">Keine Umrechning</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="201"/>
        <source>Base units</source>
        <translation type="unfinished">Basiseinheiten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="202"/>
        <source>Optimal units</source>
        <translation type="unfinished">Optimale Einheiten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="203"/>
        <source>Optimal SI units</source>
        <translation type="unfinished">Optimale SI-Einheiten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="207"/>
        <source>Convert to mixed units</source>
        <translation type="unfinished">In gemischte Einheiten umrechnen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="208"/>
        <source>Automatic unit prefixes:</source>
        <translation type="unfinished">Automatische Einheitenpräfixe:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="210"/>
        <source>Default</source>
        <translation type="unfinished">Standard</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="211"/>
        <source>No prefixes</source>
        <translation type="unfinished">Keine Präfixe</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="212"/>
        <source>Prefixes for some units</source>
        <translation type="unfinished">Präfixe für einige Einheiten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="213"/>
        <source>Prefixes also for currencies</source>
        <translation type="unfinished">Präfixe auch für Währungen</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="214"/>
        <source>Prefixes for all units</source>
        <translation type="unfinished">Präfixe für alle Einheiten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="222"/>
        <source>Enable all SI-prefixes</source>
        <translation type="unfinished">Alle SI-Präfixe einschalten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="223"/>
        <source>Enable denominator prefixes</source>
        <translation type="unfinished">Nenner-Präfixe einschalten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="224"/>
        <source>Enable units in physical constants</source>
        <translation type="unfinished">Einheiten in physikalischen Konstanten einschalten</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="225"/>
        <source>Temperature calculation:</source>
        <translation type="unfinished">Temperatur-berechnung:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="227"/>
        <source>Absolute</source>
        <translation type="unfinished">Absolut</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="228"/>
        <source>Relative</source>
        <translation type="unfinished">Relativ</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="229"/>
        <source>Hybrid</source>
        <translation type="unfinished">Hybrid</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="234"/>
        <source>Exchange rates updates:</source>
        <translation type="unfinished">Wechselkurse aktualisieren:</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/preferencesdialog.cpp" line="237"/>
        <location filename="../src/preferencesdialog.cpp" line="372"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n Tag</numerusform>
            <numerusform>%n Tage</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>days</source>
        <translation type="vanished">
            <numerusform>Tag</numerusform>
            <numerusform>Tage</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="71"/>
        <source>Execute expressions and commands from a file</source>
        <translation type="unfinished">Ausdrücke und Befehle aus einer Datei ausführen</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="71"/>
        <source>FILE</source>
        <translation type="unfinished">DATEI</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="73"/>
        <source>Start a new instance of the application</source>
        <translation type="unfinished">Eine neue Instanz der Applikation starten</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="75"/>
        <source>Specify the window title</source>
        <translation type="unfinished">Festlegen des Fenstertitels</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="75"/>
        <source>TITLE</source>
        <translation type="unfinished">TITEL</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="77"/>
        <source>Display the application version</source>
        <translation type="unfinished">Anzeigen der Applikationsversion</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="79"/>
        <source>Expression to calculate</source>
        <translation type="unfinished">Zu berechnender Ausdruck</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="79"/>
        <source>[EXPRESSION]</source>
        <translation type="unfinished">[AUSDRUCK]</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="95"/>
        <source>By default, only one instance (one main window) of %1 is allowed.

If multiple instances are opened simultaneously, only the definitions (variables, functions, etc.), mode, preferences, and history of the last closed window will be saved.

Do you, despite this, want to change the default behavior and allow multiple simultaneous instances?</source>
        <translation>Standardmäßig ist nur eine Instanz (ein Hauptfenster) von %1 erlaubt.

Wenn mehrere Instanzen gleichzeitig geöffnet werden, werden nur die Definitionen (Variablen, Funktionen usw.), der Modus, die Einstellungen und der Verlauf des zuletzt geschlossenen Fensters gespeichert.

Möchten Sie trotzdem die Standardvorgabe ändern und mehrere gleichzeitige Instanzen zulassen?</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="109"/>
        <source>%1 is already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="149"/>
        <source>Failed to load global definitions!
</source>
        <translation type="unfinished">Das Laden der globalen Definitionen ist fehlgeschlagen!
</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="63"/>
        <location filename="../src/qalculateqtsettings.cpp" line="64"/>
        <location filename="../src/qalculateqtsettings.cpp" line="687"/>
        <source>answer</source>
        <translation type="unfinished">antwort</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="63"/>
        <source>History Answer Value</source>
        <translation type="unfinished">Verlauf Ergebniswert</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="65"/>
        <source>History Index(es)</source>
        <translation type="unfinished">Verlaufsindex(e)</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="76"/>
        <source>History index %s does not exist.</source>
        <translation type="unfinished">Verlaufsindex %s existiert nicht.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="686"/>
        <source>Last Answer</source>
        <translation type="unfinished">Letzte Antwort</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="689"/>
        <source>Answer 2</source>
        <translation type="unfinished">Antwort 2</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="690"/>
        <source>Answer 3</source>
        <translation type="unfinished">Antwort 3</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="691"/>
        <source>Answer 4</source>
        <translation type="unfinished">Antwort 4</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="692"/>
        <source>Answer 5</source>
        <translation type="unfinished">Antwort 5</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="693"/>
        <source>Memory</source>
        <translation type="unfinished">Speicher</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="777"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="777"/>
        <source>Couldn&apos;t write preferences to
%1</source>
        <translation>Konnte Einstellungen nicht schreiben in
%1</translation>
    </message>
</context>
<context>
    <name>QalculateQtSettings</name>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1062"/>
        <source>Update exchange rates?</source>
        <translation type="unfinished">Wechselkurse aktualisieren?</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/qalculateqtsettings.cpp" line="1062"/>
        <source>It has been %n day(s) since the exchange rates last were updated.

Do you wish to update the exchange rates now?</source>
        <translation type="unfinished">
            <numerusform>Es ist %n Tag seit der letzten Aktualisierung der Wechselkurse vergangen.

Möchten Sie die Wechselkurse jetzt aktualisieren?</numerusform>
            <numerusform>Es sind %n Tage seit der letzten Aktualisierung der Wechselkurse vergangen.

Möchten Sie die Wechselkurse jetzt aktualisieren?</numerusform>
        </translation>
    </message>
    <message>
        <source>Fetching exchange rates.</source>
        <translation type="obsolete">Abrufen von Wechselkursen.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1090"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1091"/>
        <source>Fetching exchange rates…</source>
        <translation type="unfinished">Abrufen von Wechselkursen…</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1124"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1137"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1146"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1197"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1220"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1125"/>
        <source>Warning</source>
        <translation type="unfinished">Warnung</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1126"/>
        <source>Information</source>
        <translation type="unfinished">Information</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1137"/>
        <source>Path of executable not found.</source>
        <translation type="unfinished">Pfad der ausführbaren Datei nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1146"/>
        <source>curl not found.</source>
        <translation type="unfinished">curl nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1197"/>
        <source>Failed to run update script.
%1</source>
        <translation type="unfinished">Update-Skript konnte nicht ausgeführt werden.
%1</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1220"/>
        <source>Failed to check for updates.</source>
        <translation type="unfinished">Prüfung auf Updates fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1221"/>
        <source>No updates found.</source>
        <translation type="unfinished">Keine Updates gefunden.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1227"/>
        <source>A new version of %1 is available at %2.

Do you wish to update to version %3?</source>
        <translation type="unfinished">Eine neue Version von %1 ist verfügbar unter %2.

Möchten Sie auf die Version %3 aktualisieren?</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1231"/>
        <source>A new version of %1 is available.

You can get version %3 at %2.</source>
        <translation type="unfinished">Eine neue Version von %1 ist verfügbar.

Sie können die Version %3 unter %2 erhalten.</translation>
    </message>
</context>
<context>
    <name>QalculateTranslator</name>
    <message>
        <location filename="../src/main.cpp" line="197"/>
        <source>OK</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="199"/>
        <source>Cancel</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished">Abbruch</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>Close</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="203"/>
        <source>&amp;Yes</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="205"/>
        <source>&amp;No</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="207"/>
        <source>&amp;Open</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="209"/>
        <source>&amp;Save</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="211"/>
        <source>&amp;Select All</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="213"/>
        <source>Look in:</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="215"/>
        <source>File &amp;name:</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="217"/>
        <source>Files of type:</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QalculateWindow</name>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="292"/>
        <source>Menu</source>
        <translation type="unfinished">Menü</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="293"/>
        <source>Menu (%1)</source>
        <translation type="unfinished">Menü (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="298"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="299"/>
        <source>Function…</source>
        <translation type="unfinished">Funktion…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="300"/>
        <source>Variable/Constant…</source>
        <translation type="unfinished">Variable/Konstante…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="301"/>
        <source>Unknown Variable…</source>
        <translation type="unfinished">Unbekannte Variable…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="302"/>
        <source>Matrix…</source>
        <translation>Matrix…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="305"/>
        <source>Import CSV File…</source>
        <translation>CSV-Datei importieren...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="306"/>
        <source>Export CSV File…</source>
        <translation>CSV-Datei exportieren...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="308"/>
        <location filename="../src/qalculatewindow.cpp" line="552"/>
        <source>Functions</source>
        <translation>Funktionen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="309"/>
        <source>Variables and Constants</source>
        <translation type="unfinished">Variablen und Konstanten</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="310"/>
        <source>Units</source>
        <translation>Einheiten</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="312"/>
        <source>Plot Functions/Data</source>
        <translation>Funktionen/Daten plotten</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="313"/>
        <source>Floating Point Conversion (IEEE 754)</source>
        <translation>Gleitkomma-Konvertierung (IEEE 754)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="314"/>
        <source>Calendar Conversion</source>
        <translation>Kalender Konvertierung</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="316"/>
        <source>Update Exchange Rates</source>
        <translation>Wechselkurse aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="319"/>
        <source>Normal Mode</source>
        <translation type="unfinished">Normal Modus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="320"/>
        <source>RPN Mode</source>
        <translation type="unfinished">RPN-Modus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="321"/>
        <source>Chain Mode</source>
        <translation type="unfinished">Methodenverkettung</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="323"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="325"/>
        <source>Help</source>
        <translation type="unfinished">Hilfe</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="326"/>
        <source>Report a Bug</source>
        <translation>Einen Fehler melden</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="327"/>
        <source>Check for Updates</source>
        <translation>Nach Updates suchen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="328"/>
        <location filename="../src/qalculatewindow.cpp" line="329"/>
        <location filename="../src/qalculatewindow.cpp" line="892"/>
        <source>About %1</source>
        <translation type="unfinished">Über %1</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="331"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="334"/>
        <source>Mode</source>
        <translation type="unfinished">Modus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="335"/>
        <source>Mode (%1)</source>
        <translation type="unfinished">Modus (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="340"/>
        <location filename="../src/qalculatewindow.cpp" line="343"/>
        <source>General Display Mode</source>
        <translation type="unfinished">Allgemeiner Anzeigemodus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="345"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="348"/>
        <source>Scientific</source>
        <translation>Wissenschaftlich</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="351"/>
        <source>Engineering</source>
        <translation>Technisch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="354"/>
        <source>Simple</source>
        <translation>Einfach</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="358"/>
        <location filename="../src/qalculatewindow.cpp" line="359"/>
        <source>Angle Unit</source>
        <translation>Winkeleinheit</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="361"/>
        <source>Radians</source>
        <translation>Bogenmaß</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="363"/>
        <source>Degrees</source>
        <translation>Grad</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="365"/>
        <source>Gradians</source>
        <translation>Neugrad</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="368"/>
        <location filename="../src/qalculatewindow.cpp" line="369"/>
        <source>Approximation</source>
        <translation>Annäherung</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="371"/>
        <source>Automatic</source>
        <comment>Automatic approximation</comment>
        <translation type="unfinished">Automatisch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="373"/>
        <source>Dual</source>
        <comment>Dual approximation</comment>
        <translation type="unfinished">Zweifach</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="375"/>
        <source>Exact</source>
        <comment>Exact approximation</comment>
        <translation type="unfinished">Genau</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="377"/>
        <source>Approximate</source>
        <translation type="unfinished">Annähern</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="382"/>
        <source>Assumptions</source>
        <translation>Annahmen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="383"/>
        <source>Type</source>
        <comment>Assumptions type</comment>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="385"/>
        <source>Number</source>
        <translation>Zahl</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="387"/>
        <source>Real</source>
        <translation>Real</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="389"/>
        <source>Rational</source>
        <translation>Rational</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="391"/>
        <source>Integer</source>
        <translation>Ganzzahl</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="393"/>
        <source>Boolean</source>
        <translation type="unfinished">Boolescher Wert</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="395"/>
        <source>Sign</source>
        <comment>Assumptions sign</comment>
        <translation type="unfinished">Vorzeichen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="397"/>
        <source>Unknown</source>
        <comment>Unknown assumptions sign</comment>
        <translation type="unfinished">Unbekannt</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="399"/>
        <source>Non-zero</source>
        <translation type="unfinished">Nicht-Null</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="401"/>
        <source>Positive</source>
        <translation type="unfinished">Positiv</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="403"/>
        <source>Non-negative</source>
        <translation type="unfinished">Nicht-Negativ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="405"/>
        <source>Negative</source>
        <translation type="unfinished">Negativ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="407"/>
        <source>Non-positive</source>
        <translation type="unfinished">Nicht-Positiv</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="411"/>
        <location filename="../src/qalculatewindow.cpp" line="412"/>
        <source>Result Base</source>
        <translation type="unfinished">Ergebnisbasi</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="415"/>
        <location filename="../src/qalculatewindow.cpp" line="470"/>
        <source>Binary</source>
        <translation>Binär</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="417"/>
        <location filename="../src/qalculatewindow.cpp" line="472"/>
        <source>Octal</source>
        <translation>Oktal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="419"/>
        <location filename="../src/qalculatewindow.cpp" line="474"/>
        <source>Decimal</source>
        <translation>Dezimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="421"/>
        <location filename="../src/qalculatewindow.cpp" line="476"/>
        <source>Hexadecimal</source>
        <translation>Hexadezimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="424"/>
        <location filename="../src/qalculatewindow.cpp" line="479"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="425"/>
        <location filename="../src/qalculatewindow.cpp" line="480"/>
        <source>Duodecimal</source>
        <translation>Duodezimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="427"/>
        <source>Sexagesimal</source>
        <translation>Sexagesimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="429"/>
        <source>Time format</source>
        <translation>Zeitformat</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="431"/>
        <location filename="../src/qalculatewindow.cpp" line="482"/>
        <source>Roman numerals</source>
        <translation>Römische Ziffern</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="433"/>
        <location filename="../src/qalculatewindow.cpp" line="484"/>
        <source>Unicode</source>
        <translation>Unicode</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="435"/>
        <location filename="../src/qalculatewindow.cpp" line="486"/>
        <source>Bijective base-26</source>
        <translation>Bijektive Basis-26</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="498"/>
        <source>Custom:</source>
        <comment>Number base</comment>
        <translation type="unfinished">Benutzerdefiniert:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="466"/>
        <location filename="../src/qalculatewindow.cpp" line="467"/>
        <source>Expression Base</source>
        <translation type="unfinished">Ausdrucksbasis</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="451"/>
        <source>Other:</source>
        <comment>Number base</comment>
        <translation type="unfinished">Andere:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="518"/>
        <source>Precision:</source>
        <translation type="unfinished">Genauigkeit:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="525"/>
        <source>Min decimals:</source>
        <translation>Min Dezimalen:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="532"/>
        <source>Max decimals:</source>
        <translation>Max Dezimalen:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="535"/>
        <source>off</source>
        <comment>Max decimals</comment>
        <translation type="unfinished">aus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="545"/>
        <source>Convert</source>
        <translation type="unfinished">Umrechnen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="546"/>
        <source>Convert (%1)</source>
        <translation type="unfinished">Umrechnen (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="549"/>
        <source>Store</source>
        <translation type="unfinished">Sichern</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="549"/>
        <source>Store (%1)</source>
        <translation type="unfinished">Sichern (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="552"/>
        <source>Functions (%1)</source>
        <translation>Funktionen (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="560"/>
        <location filename="../src/qalculatewindow.cpp" line="637"/>
        <source>Keypad</source>
        <translation>Tastatur</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="561"/>
        <source>Keypad (%1)</source>
        <translation>Tastatur (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="555"/>
        <location filename="../src/qalculatewindow.cpp" line="587"/>
        <source>Number bases</source>
        <translation>Zahlenbasen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="556"/>
        <source>Number Bases (%1)</source>
        <translation>Zahlenbasen (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="591"/>
        <source>Binary:</source>
        <translation>Binär:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="593"/>
        <source>Octal:</source>
        <translation>Oktal:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="595"/>
        <source>Decimal:</source>
        <translation>Dezimal:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="597"/>
        <source>Hexadecimal:</source>
        <translation>Hexadezimal:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="644"/>
        <source>RPN Stack</source>
        <translation>RPN-Stack</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="663"/>
        <source>Rotate the stack or move the selected register up (%1)</source>
        <translation>Drehen des Stapels oder Verschieben des ausgewählten Register nach oben (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="666"/>
        <source>Rotate the stack or move the selected register down (%1)</source>
        <translation>Drehen des Stapels oder Verschieben des ausgewählten Register nach unten (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="669"/>
        <source>Swap the top two values or move the selected value to the top of the stack (%1)</source>
        <translation>Vertauschen Sie die beiden oberen Werte oder verschieben Sie den ausgewählten Wert an die Spitze des Stapels (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="672"/>
        <source>Copy the selected or top value to the top of the stack (%1)</source>
        <translation>Kopieren des ausgewählten oder obersten Wertes an die Spitze des Stapels (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="675"/>
        <source>Enter the top value from before the last numeric operation (%1)</source>
        <translation>Eingabe des obersten Wertes von vor der letzten numerischen Operation (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="678"/>
        <source>Delete the top or selected value (%1)</source>
        <translation>Löschen des oberen oder ausgewählten Wertes (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="681"/>
        <source>Clear the RPN stack (%1)</source>
        <translation>Löschen des RPN-Stack (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="892"/>
        <source>Powerful and easy to use calculator</source>
        <translation>Leistungsstarker und einfach zu bedienender Taschenrechner</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="892"/>
        <source>License: GNU General Public License version 2 or later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2209"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2209"/>
        <source>Couldn&apos;t write definitions</source>
        <translation>Definitionen konnten nicht geschrieben werden</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2717"/>
        <source>hexadecimal</source>
        <translation>hexadezimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2720"/>
        <source>octal</source>
        <translation>oktal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2723"/>
        <source>decimal</source>
        <translation>dezimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2726"/>
        <source>duodecimal</source>
        <translation>duodezimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2729"/>
        <source>binary</source>
        <translation>binär</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2732"/>
        <source>roman</source>
        <translation>römisch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2735"/>
        <source>bijective</source>
        <translation>bijektiv</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2738"/>
        <location filename="../src/qalculatewindow.cpp" line="2741"/>
        <location filename="../src/qalculatewindow.cpp" line="2744"/>
        <source>sexagesimal</source>
        <translation>sexagesimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2747"/>
        <location filename="../src/qalculatewindow.cpp" line="2750"/>
        <source>latitude</source>
        <translation>breitengrad</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2753"/>
        <location filename="../src/qalculatewindow.cpp" line="2756"/>
        <source>longitude</source>
        <translation>längengrad</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2774"/>
        <source>time</source>
        <translation>zeit</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2825"/>
        <source>Time zone parsing failed.</source>
        <translation>Zeitzonenanalyse fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2848"/>
        <source>bases</source>
        <translation>basen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2857"/>
        <source>calendars</source>
        <translation>kalendarien</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2866"/>
        <source>rectangular</source>
        <translation type="unfinished">rechtwinklig</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2866"/>
        <source>cartesian</source>
        <translation type="unfinished">kartesisch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2877"/>
        <source>exponential</source>
        <translation type="unfinished">exponential</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2888"/>
        <source>polar</source>
        <translation type="unfinished">polar</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2912"/>
        <source>phasor</source>
        <translation type="unfinished">phase</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2912"/>
        <source>angle</source>
        <translation type="unfinished">winkel</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2923"/>
        <source>optimal</source>
        <translation type="unfinished">optimal</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2934"/>
        <location filename="../src/qalculatewindow.cpp" line="2980"/>
        <source>base</source>
        <translation type="unfinished">basis</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2945"/>
        <source>mixed</source>
        <translation type="unfinished">gemischt</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2959"/>
        <source>fraction</source>
        <translation type="unfinished">bruchteil</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2962"/>
        <source>factors</source>
        <translation type="unfinished">faktoren</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2971"/>
        <source>partial fraction</source>
        <translation type="unfinished">teilbruch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3021"/>
        <source>factorize</source>
        <translation type="unfinished">faktorisieren</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3024"/>
        <source>expand</source>
        <translation type="unfinished">erweitern</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3195"/>
        <location filename="../src/qalculatewindow.cpp" line="3196"/>
        <location filename="../src/qalculatewindow.cpp" line="3197"/>
        <location filename="../src/qalculatewindow.cpp" line="3574"/>
        <source>Calculating…</source>
        <translation type="unfinished">Berechnen...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3196"/>
        <location filename="../src/qalculatewindow.cpp" line="3583"/>
        <location filename="../src/qalculatewindow.cpp" line="4081"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3250"/>
        <location filename="../src/qalculatewindow.cpp" line="3944"/>
        <source>RPN Operation</source>
        <translation>RPN-Operation</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3562"/>
        <source>Factorizing…</source>
        <translation>Faktorisieren...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3566"/>
        <source>Expanding partial fractions…</source>
        <translation>Expandieren von Teilbrüchen...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3570"/>
        <source>Expanding…</source>
        <translation>Expandieren...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3578"/>
        <source>Converting…</source>
        <translation>Konvertieren...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3940"/>
        <source>RPN Register Moved</source>
        <translation>RPN-Register verschoben</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4080"/>
        <location filename="../src/qalculatewindow.cpp" line="4081"/>
        <location filename="../src/qalculatewindow.cpp" line="4082"/>
        <source>Processing…</source>
        <translation>Verarbeitung...</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4187"/>
        <location filename="../src/qalculatewindow.cpp" line="5453"/>
        <source>Matrix</source>
        <translation>Matrix</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4288"/>
        <source>Temperature Calculation Mode</source>
        <translation>Temperatur-Berechnungsmodus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4294"/>
        <source>The expression is ambiguous.
Please select temperature calculation mode
(the mode can later be changed in preferences).</source>
        <translation>Der Ausdruck ist mehrdeutig.
Bitte wählen Sie den Temperaturberechnungsmodus
(der Modus kann später in den Einstellungen geändert werden).</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4297"/>
        <source>Absolute</source>
        <translation>Absolut</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4301"/>
        <source>Relative</source>
        <translation>Relativ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4305"/>
        <source>Hybrid</source>
        <translation>Hybrid</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4346"/>
        <source>Interpretation of dots</source>
        <translation>Interpretation von Punkten</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4352"/>
        <source>Please select interpretation of dots (&quot;.&quot;)
(this can later be changed in preferences).</source>
        <translation>Bitte wählen Sie die Interpretation der Punkte (&quot;.&quot;)
(dies kann später in den Einstellungen geändert werden).</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4355"/>
        <source>Both dot and comma as decimal separators</source>
        <translation>Sowohl Punkt als auch Komma als Dezimaltrennzeichen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4359"/>
        <source>Dot as thousands separator</source>
        <translation>Punkt als Tausendertrennzeichen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4363"/>
        <source>Only dot as decimal separator</source>
        <translation>Nur Punkt als Dezimaltrennzeichen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4391"/>
        <source>Parsing Mode</source>
        <translation type="unfinished">Analyse-Modus</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4397"/>
        <source>The expression is ambiguous.
Please select interpretation of expressions with implicit multiplication
(this can later be changed in preferences).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4400"/>
        <source>Implicit multiplication first</source>
        <translation type="unfinished">Implizite Multiplikation zuerst</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4405"/>
        <source>Conventional</source>
        <translation type="unfinished">Konventionell</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4410"/>
        <source>Adaptive</source>
        <translation type="unfinished">Adaptiv</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4925"/>
        <source>Gnuplot was not found</source>
        <translation>Gnuplot wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4925"/>
        <source>%1 (%2) needs to be installed separately, and found in the executable search path, for plotting to work.</source>
        <translation>%1 (%2) muss separat installiert werden und im Such-pfad für ausführbare Dateien gefunden werden, damit das Plotten funktioniert.</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5107"/>
        <source>Example:</source>
        <comment>Example of function usage</comment>
        <translation>Beispiel:</translation>
    </message>
    <message>
        <source>Example:</source>
        <translation type="obsolete">Beispiel:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5145"/>
        <source>Enter</source>
        <comment>RPN Enter</comment>
        <translation>eingeben</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5145"/>
        <source>Calculate</source>
        <translation>Berechnen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5146"/>
        <source>Apply to Stack</source>
        <translation>Auf Stapel anwenden</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5146"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5137"/>
        <source>Keep open</source>
        <translation>Offen halten</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5162"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5164"/>
        <source>Argument</source>
        <translation>Argument</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5177"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5232"/>
        <source>True</source>
        <translation>Wahr</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5235"/>
        <source>False</source>
        <translation>Falsch</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5261"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5296"/>
        <location filename="../src/qalculatewindow.cpp" line="5304"/>
        <source>optional</source>
        <comment>optional argument</comment>
        <translation>optional</translation>
    </message>
    <message>
        <source>optional</source>
        <comment>optional parameter</comment>
        <translation type="obsolete">optional</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5646"/>
        <source>Failed to open %1.
%2</source>
        <translation>Konnte %1. nicht öffnen
%2</translation>
    </message>
</context>
<context>
    <name>UnitsDialog</name>
    <message>
        <location filename="../src/unitsdialog.cpp" line="37"/>
        <source>Units</source>
        <translation>Einheiten</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="46"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="63"/>
        <location filename="../src/unitsdialog.cpp" line="537"/>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="84"/>
        <source>New…</source>
        <translation>Neu…</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="85"/>
        <source>Edit…</source>
        <translation>Bearbeiten…</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="86"/>
        <location filename="../src/unitsdialog.cpp" line="422"/>
        <location filename="../src/unitsdialog.cpp" line="425"/>
        <source>Deactivate</source>
        <translation>Deaktivieren</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="87"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="89"/>
        <source>Convert</source>
        <translation type="unfinished">Umrechnen</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="90"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="427"/>
        <source>Activate</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="613"/>
        <source>All</source>
        <comment>All units</comment>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="656"/>
        <source>Uncategorized</source>
        <translation type="unfinished">Nicht kategorisiert</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="662"/>
        <source>User units</source>
        <translation type="unfinished">Benutzereinheiten</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="332"/>
        <location filename="../src/unitsdialog.cpp" line="669"/>
        <source>Inactive</source>
        <translation type="unfinished">Inaktiv</translation>
    </message>
</context>
<context>
    <name>UnknownEditDialog</name>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="30"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="33"/>
        <source>Custom assumptions</source>
        <translation type="unfinished">Benutzerdefinierte Annahmen</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="36"/>
        <source>Type:</source>
        <translation type="unfinished">Typ:</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="44"/>
        <source>Sign:</source>
        <translation type="unfinished">Vorzeichen:</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="97"/>
        <location filename="../src/unknowneditdialog.cpp" line="129"/>
        <source>Question</source>
        <translation type="unfinished">Frage</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="97"/>
        <location filename="../src/unknowneditdialog.cpp" line="129"/>
        <source>A unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation>Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <source>An unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation type="obsolete">Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="171"/>
        <source>Edit Unknown Variable</source>
        <translation>Unbekannte Variable bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="184"/>
        <source>New Unknown Variable</source>
        <translation>Neue Unbekannte Variable</translation>
    </message>
</context>
<context>
    <name>VariableEditDialog</name>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="30"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="45"/>
        <source>Temporary</source>
        <translation type="unfinished">Temporär</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="39"/>
        <source>Value:</source>
        <translation>Wert:</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="41"/>
        <source>current result</source>
        <translation type="unfinished">Aktuelles Ergebnis</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="77"/>
        <location filename="../src/variableeditdialog.cpp" line="109"/>
        <source>Question</source>
        <translation type="unfinished">Frage</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="77"/>
        <location filename="../src/variableeditdialog.cpp" line="109"/>
        <source>A unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation>Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <source>An unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation type="obsolete">Eine Einheit oder Variable mit demselben Namen ist bereits vorhanden.
Möchten Sie sie überschreiben?</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="181"/>
        <source>Edit Variable</source>
        <translation>Variable bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="195"/>
        <location filename="../src/variableeditdialog.cpp" line="229"/>
        <source>New Variable</source>
        <translation>Neue Variable</translation>
    </message>
</context>
<context>
    <name>VariablesDialog</name>
    <message>
        <location filename="../src/variablesdialog.cpp" line="37"/>
        <source>Variables</source>
        <translation>Variablen</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="46"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="63"/>
        <location filename="../src/variablesdialog.cpp" line="452"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="84"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="86"/>
        <source>Variable/Constant…</source>
        <translation type="unfinished">Variable/Konstante…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="87"/>
        <source>Unknown Variable…</source>
        <translation type="unfinished">Unbekannte Variable…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="88"/>
        <source>Matrix…</source>
        <translation type="unfinished">Matrix…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="90"/>
        <source>Edit…</source>
        <translation>Bearbeiten…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="91"/>
        <source>Export…</source>
        <translation>Exportieren…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="92"/>
        <location filename="../src/variablesdialog.cpp" line="386"/>
        <location filename="../src/variablesdialog.cpp" line="389"/>
        <source>Deactivate</source>
        <translation>Deaktivieren</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="93"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="95"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="337"/>
        <source>a matrix</source>
        <translation type="unfinished">eine Matrix</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="339"/>
        <source>a vector</source>
        <translation type="unfinished">ein Vektor</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="357"/>
        <source>positive</source>
        <translation>positiv</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="358"/>
        <source>non-positive</source>
        <translation>nicht-positiv</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="359"/>
        <source>negative</source>
        <translation>negativ</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="360"/>
        <source>non-negative</source>
        <translation>nicht-negativ</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="361"/>
        <source>non-zero</source>
        <translation>nicht-null</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="367"/>
        <source>integer</source>
        <translation>ganzzahlig</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="368"/>
        <source>boolean</source>
        <translation>boolesch</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="369"/>
        <source>rational</source>
        <translation>rational</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="370"/>
        <source>real</source>
        <translation>reell</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="371"/>
        <source>complex</source>
        <translation>komplex</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="372"/>
        <source>number</source>
        <translation>zahl</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="373"/>
        <source>not matrix</source>
        <translation type="unfinished">nicht Matrix</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="376"/>
        <source>unknown</source>
        <translation type="unfinished">unbekannt</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="379"/>
        <source>Default assumptions</source>
        <translation>Standardannahmen</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="391"/>
        <source>Activate</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="509"/>
        <source>All</source>
        <comment>All variables</comment>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="552"/>
        <source>Uncategorized</source>
        <translation type="unfinished">Nicht kategorisiert</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="558"/>
        <source>User variables</source>
        <translation type="unfinished">Benutzervariablen</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="311"/>
        <location filename="../src/variablesdialog.cpp" line="565"/>
        <source>Inactive</source>
        <translation type="unfinished">Inaktiv</translation>
    </message>
</context>
</TS>
