<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>CSVDialog</name>
    <message>
        <location filename="../src/csvdialog.cpp" line="33"/>
        <source>Import CSV File</source>
        <translation>Загрузить файл формата CSV</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="33"/>
        <source>Export CSV File</source>
        <translation>Экспорт в файл типа CSV</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="41"/>
        <source>Current result</source>
        <translation>Текущий результат</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="44"/>
        <source>Matrix/vector variable:</source>
        <translation>Матричная/векторная переменная:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="56"/>
        <source>File:</source>
        <translation>Файл:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="66"/>
        <source>Import as</source>
        <translation>Импортировать как</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="70"/>
        <source>matrix</source>
        <translation>матрица</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="72"/>
        <source>vectors</source>
        <translation>векторы</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="75"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="78"/>
        <source>First row:</source>
        <translation>Первая строка:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="82"/>
        <source>Includes headings</source>
        <translation>Включает заголовки</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="85"/>
        <source>Delimiter:</source>
        <translation>Разделитель:</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="87"/>
        <source>Comma</source>
        <translation>Запятая</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="88"/>
        <source>Tabulator</source>
        <translation>Табуляция</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="89"/>
        <source>Semicolon</source>
        <translation>Точка с запятой</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="90"/>
        <source>Space</source>
        <translation>Пробел</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="91"/>
        <source>Other</source>
        <translation>Другой</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="149"/>
        <source>A unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation>Единица измерения или переменная с таким именем уже существует.
Вы хотите её перезаписать?</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="155"/>
        <source>Could not import from file 
%1</source>
        <translation>Не удалось импортировать из файла 
%1</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="177"/>
        <source>Could not export to file 
%1</source>
        <translation>Не удалось экспортировать в файл 
%1</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="149"/>
        <source>Question</source>
        <translation>Вопрос</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="155"/>
        <location filename="../src/csvdialog.cpp" line="169"/>
        <location filename="../src/csvdialog.cpp" line="177"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/csvdialog.cpp" line="169"/>
        <source>No matrix or vector variable with the entered name was found.</source>
        <translation>Матричная или векторная переменная с указанным именем не найдена.</translation>
    </message>
</context>
<context>
    <name>CalendarConversionDialog</name>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="27"/>
        <source>Calendar Conversion</source>
        <translation>Преобразование календаря</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="33"/>
        <location filename="../src/calendarconversiondialog.cpp" line="130"/>
        <source>Gregorian</source>
        <translation>Григорианский</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="34"/>
        <location filename="../src/calendarconversiondialog.cpp" line="131"/>
        <source>Hebrew</source>
        <translation>Еврейский</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="35"/>
        <location filename="../src/calendarconversiondialog.cpp" line="132"/>
        <source>Islamic (Hijri)</source>
        <translation>Исламский (от Хиджры)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="36"/>
        <location filename="../src/calendarconversiondialog.cpp" line="133"/>
        <source>Persian (Solar Hijri)</source>
        <translation>Иранский (Солнечная хиджра)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="37"/>
        <location filename="../src/calendarconversiondialog.cpp" line="134"/>
        <source>Indian (National)</source>
        <translation>Индийский (национальный)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="38"/>
        <location filename="../src/calendarconversiondialog.cpp" line="135"/>
        <source>Chinese</source>
        <translation>Китайский</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="39"/>
        <location filename="../src/calendarconversiondialog.cpp" line="136"/>
        <source>Julian</source>
        <translation>Юлианский</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="40"/>
        <location filename="../src/calendarconversiondialog.cpp" line="137"/>
        <source>Revised Julian (Milanković)</source>
        <translation>Пересмотренный юлианский (Миланковича)</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="41"/>
        <location filename="../src/calendarconversiondialog.cpp" line="138"/>
        <source>Coptic</source>
        <translation>Коптский</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="42"/>
        <location filename="../src/calendarconversiondialog.cpp" line="139"/>
        <source>Ethiopian</source>
        <translation>Эфиопский</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="86"/>
        <source>The selected Chinese year does not exist.</source>
        <translation>Выбранный китайский год не существует.</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="98"/>
        <source>Conversion to Gregorian calendar failed.</source>
        <translation>Преобразование в григорианский календарь не удалось.</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="143"/>
        <source>Calendar conversion failed for: %1.</source>
        <translation>Ошибка преобразования календаря для: %1.</translation>
    </message>
    <message>
        <location filename="../src/calendarconversiondialog.cpp" line="86"/>
        <location filename="../src/calendarconversiondialog.cpp" line="98"/>
        <location filename="../src/calendarconversiondialog.cpp" line="143"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>ExpressionEdit</name>
    <message>
        <location filename="../src/expressionedit.cpp" line="1035"/>
        <source>matrix</source>
        <translation>матрица</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1037"/>
        <source>vector</source>
        <translation>вектор</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1048"/>
        <source>positive</source>
        <translation>положительное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1049"/>
        <source>non-positive</source>
        <translation>не положительное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1050"/>
        <source>negative</source>
        <translation>отрицательное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1051"/>
        <source>non-negative</source>
        <translation>неотрицательное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1052"/>
        <source>non-zero</source>
        <translation>ненулевое</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1057"/>
        <source>boolean</source>
        <translation>логическое</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1058"/>
        <source>integer</source>
        <translation>целое</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1059"/>
        <source>rational</source>
        <translation>рациональное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1060"/>
        <source>real</source>
        <translation>вещественное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1061"/>
        <source>complex</source>
        <translation>комплексное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1062"/>
        <source>number</source>
        <translation>число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1063"/>
        <source>(not matrix)</source>
        <translation>(не матрица)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1066"/>
        <source>unknown</source>
        <translation>неизвестное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1068"/>
        <source>default assumptions</source>
        <translation>предположения по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1182"/>
        <source>Prefix:</source>
        <translation>Префикс:</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1240"/>
        <source>Base units</source>
        <translation>Основные единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1248"/>
        <source>Calendars</source>
        <translation>Календари</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1258"/>
        <source>Factors</source>
        <translation>Множители</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1270"/>
        <source>Fraction</source>
        <translation>Дробь</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1274"/>
        <source>Latitude</source>
        <translation>Широта</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1276"/>
        <source>Longitude</source>
        <translation>Долгота</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1296"/>
        <location filename="../src/expressionedit.cpp" line="2131"/>
        <source>Unicode</source>
        <translation>Юникод</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2161"/>
        <source>UTC time zone</source>
        <translation>Часовой пояс UTC</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1606"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1236"/>
        <source>Complex Angle/Phasor Notation</source>
        <translation>Обозначение угла/вектора комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1242"/>
        <source>Number Base</source>
        <translation>Основание системы счисления</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1244"/>
        <source>Bijective Base-26</source>
        <translation>Биективное основание-26</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1246"/>
        <source>Binary Number</source>
        <translation>Двоичное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1250"/>
        <source>Complex cis Form</source>
        <translation>Сисоидная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1252"/>
        <source>Decimal Number</source>
        <translation>Десятичное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1254"/>
        <source>Duodecimal Number</source>
        <translation>Двенадцатеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1256"/>
        <source>Complex Exponential Form</source>
        <translation>Экспоненциальная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1260"/>
        <source>16-bit Floating Point Binary Format</source>
        <translation>16-битное в двоичном формате с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1262"/>
        <source>32-bit Floating Point Binary Format</source>
        <translation>32-битное в двоичном формате с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1264"/>
        <source>64-bit Floating Point Binary Format</source>
        <translation>64-битное в двоичном формате с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1266"/>
        <source>80-bit (x86) Floating Point Binary Format</source>
        <translation>80-битное (x86) в двоичном формате с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1268"/>
        <source>128-bit Floating Point Binary Format</source>
        <translation>128-битное в двоичном формате с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1272"/>
        <source>Hexadecimal Number</source>
        <translation>Шестнадцатеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1278"/>
        <source>Mixed Units</source>
        <translation>Смешанные единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1280"/>
        <source>Octal Number</source>
        <translation>Восьмеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1282"/>
        <source>Optimal Unit</source>
        <translation>Оптимальная единица измерения</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1284"/>
        <source>Expanded Partial Fractions</source>
        <translation>Расширенные частичные дроби</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1286"/>
        <source>Complex Polar Form</source>
        <translation>Полярная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1288"/>
        <source>Complex Rectangular Form</source>
        <translation>Прямоугольная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1290"/>
        <source>Roman Numerals</source>
        <translation>Римские цифры</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1292"/>
        <source>Sexagesimal Number</source>
        <translation>Шестидесятеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1294"/>
        <source>Time Format</source>
        <translation>Формат времени</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1298"/>
        <source>UTC Time Zone</source>
        <translation>Часовой пояс UTC</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1609"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1613"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1616"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1619"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1622"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1626"/>
        <source>Insert Date…</source>
        <translation>Вставить дату…</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1627"/>
        <source>Insert Matrix…</source>
        <translation>Вставить матрицу…</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1629"/>
        <source>Select All</source>
        <translation>Выделите всё</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1632"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1636"/>
        <source>Completion</source>
        <translation>Завершение</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1648"/>
        <source>No completion</source>
        <translation>Без завершения</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1649"/>
        <source>Limited strict completion</source>
        <translation>Ограниченное строгое завершение</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1650"/>
        <source>Strict completion</source>
        <translation>Строгое завершение</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1651"/>
        <source>Limited full completion</source>
        <translation>Ограниченное полное завершение</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1652"/>
        <source>Full completion</source>
        <translation>Полное завершение</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1654"/>
        <source>Delayed completion</source>
        <translation>Отложенное завершение</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1689"/>
        <source>Matrix</source>
        <translation>Матрица</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1833"/>
        <source>Too many arguments for %1().</source>
        <translation>Слишком много аргументов для %1().</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1858"/>
        <source>argument</source>
        <translation>аргумент</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1875"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1929"/>
        <source>MC (memory clear)</source>
        <translation>MC (отчистить память)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1932"/>
        <source>MS (memory store)</source>
        <translation>MS (сохранить в памяти)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1935"/>
        <source>M+ (memory plus)</source>
        <translation>M+ (прибавить к значению в памяти)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1938"/>
        <source>M− (memory minus)</source>
        <translation>M− (отнять от значения в памяти)</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1946"/>
        <location filename="../src/expressionedit.cpp" line="1948"/>
        <source>factorize</source>
        <translation>разложить на множители</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1949"/>
        <location filename="../src/expressionedit.cpp" line="1951"/>
        <source>expand</source>
        <translation>раскрывать</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2098"/>
        <source>hexadecimal</source>
        <translation>шестнадцатеричное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2099"/>
        <location filename="../src/expressionedit.cpp" line="2209"/>
        <source>hexadecimal number</source>
        <translation>шестнадцатеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2100"/>
        <source>octal</source>
        <translation>восьмеричное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2101"/>
        <source>octal number</source>
        <translation>восьмеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2102"/>
        <source>decimal</source>
        <translation>десятеричное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2103"/>
        <source>decimal number</source>
        <translation>десятичное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2104"/>
        <source>duodecimal</source>
        <translation>двенадцатеричное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2105"/>
        <source>duodecimal number</source>
        <translation>двенадцатеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2106"/>
        <source>binary</source>
        <translation>двоичное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2107"/>
        <location filename="../src/expressionedit.cpp" line="2203"/>
        <source>binary number</source>
        <translation>двоичное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2108"/>
        <source>roman</source>
        <translation>римское число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2109"/>
        <source>roman numerals</source>
        <translation>римские цифры</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2110"/>
        <source>bijective</source>
        <translation>биективное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2111"/>
        <source>bijective base-26</source>
        <translation>биективное основание-26</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2112"/>
        <source>sexagesimal</source>
        <translation>шестидесятеричное</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2113"/>
        <source>sexagesimal number</source>
        <translation>шестидесятеричное число</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2114"/>
        <location filename="../src/expressionedit.cpp" line="2115"/>
        <source>latitude</source>
        <translation>широта</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2116"/>
        <location filename="../src/expressionedit.cpp" line="2117"/>
        <source>longitude</source>
        <translation>долгота</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2119"/>
        <source>32-bit floating point</source>
        <translation>32-битное с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2121"/>
        <source>64-bit floating point</source>
        <translation>64-битное с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2123"/>
        <source>16-bit floating point</source>
        <translation>16-битное с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2125"/>
        <source>80-bit (x86) floating point</source>
        <translation>80-битное (x86) с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2127"/>
        <source>128-bit floating point</source>
        <translation>128-битное с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2128"/>
        <source>time</source>
        <translation>время</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2129"/>
        <source>time format</source>
        <translation>формат времени</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2132"/>
        <source>bases</source>
        <translation>основания</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2133"/>
        <source>number bases</source>
        <translation>основания систем счисления</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2134"/>
        <location filename="../src/expressionedit.cpp" line="2135"/>
        <source>calendars</source>
        <translation>календари</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2136"/>
        <source>optimal</source>
        <translation>оптимально</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2137"/>
        <source>optimal unit</source>
        <translation>оптимальная единица измерения</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2138"/>
        <location filename="../src/expressionedit.cpp" line="2213"/>
        <source>base</source>
        <translation>основание</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2139"/>
        <source>base units</source>
        <translation>основные единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2140"/>
        <source>mixed</source>
        <translation>смешано</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2141"/>
        <source>mixed units</source>
        <translation>смешанные единицы</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2142"/>
        <location filename="../src/expressionedit.cpp" line="2143"/>
        <source>fraction</source>
        <translation>дробь</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2144"/>
        <location filename="../src/expressionedit.cpp" line="2145"/>
        <source>factors</source>
        <translation>множители</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2146"/>
        <source>partial fraction</source>
        <translation>частичная дробь</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2147"/>
        <source>expanded partial fractions</source>
        <translation>расширенные дробные числа</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2148"/>
        <source>rectangular</source>
        <translation>прямоугоная</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2148"/>
        <source>cartesian</source>
        <translation>декартова</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2149"/>
        <source>complex rectangular form</source>
        <translation>прямоугольная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2150"/>
        <source>exponential</source>
        <translation>экспоненциальная</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2151"/>
        <source>complex exponential form</source>
        <translation>экспоненциальная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2152"/>
        <source>polar</source>
        <translation>полярная</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2153"/>
        <source>complex polar form</source>
        <translation>полярная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2155"/>
        <source>complex cis form</source>
        <translation>сисоидная форма комплексных чисел</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2156"/>
        <source>angle</source>
        <translation>угловая</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2157"/>
        <source>complex angle notation</source>
        <translation>комплексные числа в обозначении угла</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2158"/>
        <source>phasor</source>
        <translation>фазовая</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2159"/>
        <source>complex phasor notation</source>
        <translation>Комплексные числа в обозначении вектора</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2214"/>
        <source>number base %1</source>
        <translation>основание системы счисления %1</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="2476"/>
        <source>Data object</source>
        <translation>Объект данных</translation>
    </message>
    <message>
        <location filename="../src/expressionedit.cpp" line="1656"/>
        <source>Use input method</source>
        <translation>Использовать метод ввода</translation>
    </message>
</context>
<context>
    <name>FPConversionDialog</name>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="28"/>
        <source>Floating Point Conversion (IEEE 754)</source>
        <translation>Преобразование чисел с плавающей запятой (IEEE 754)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="31"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="33"/>
        <source>16-bit (half precision)</source>
        <translation>128-битное (половинная точность)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="34"/>
        <source>32-bit (single precision)</source>
        <translation>32-битное (одинарная точность)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="35"/>
        <source>64-bit (double precision)</source>
        <translation>64-битное (двойная точность)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="36"/>
        <source>80-bit (x86 extended format)</source>
        <translation>80-битное (x86 расширенный формат)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="37"/>
        <source>128-bit (quadruple precision)</source>
        <translation>128 -битное (четырехкратная точность)</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="39"/>
        <source>Decimal value</source>
        <translation>Десятичное значение</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="41"/>
        <source>Binary representation</source>
        <translation>Двоичное представление</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="56"/>
        <source>Hexadecimal representation</source>
        <translation>Шестнадцатеричное представление</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="58"/>
        <source>Floating point value</source>
        <translation>Значение числа с плавающей запятой</translation>
    </message>
    <message>
        <location filename="../src/fpconversiondialog.cpp" line="61"/>
        <source>Conversion error</source>
        <translation>Ошибка преобразования</translation>
    </message>
</context>
<context>
    <name>FunctionEditDialog</name>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="84"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="87"/>
        <source>Expression:</source>
        <translation>Выражение:</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="93"/>
        <source>x, y, z</source>
        <translation>x, y, z</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="95"/>
        <source>\x, \y, \z, \a, \b, …</source>
        <translation>\x, \y, \z, \a, \b, …</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="115"/>
        <location filename="../src/functioneditdialog.cpp" line="148"/>
        <source>A function with the same name already exists.
Do you want to overwrite the function?</source>
        <translation>Функция с таким именем уже существует.
Хотите перезаписать функцию?</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="222"/>
        <source>Edit Function</source>
        <translation>Изменить функцию</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="235"/>
        <source>New Function</source>
        <translation>Новая функция</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="92"/>
        <source>Argument references:</source>
        <translation>Ссылки на аргументы:</translation>
    </message>
    <message>
        <location filename="../src/functioneditdialog.cpp" line="115"/>
        <location filename="../src/functioneditdialog.cpp" line="148"/>
        <source>Question</source>
        <translation>Вопрос</translation>
    </message>
</context>
<context>
    <name>FunctionsDialog</name>
    <message>
        <location filename="../src/functionsdialog.cpp" line="34"/>
        <source>Functions</source>
        <translation>Функции</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="43"/>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="60"/>
        <location filename="../src/functionsdialog.cpp" line="496"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="81"/>
        <source>New…</source>
        <translation>Новая…</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="82"/>
        <source>Edit…</source>
        <translation>Правка…</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="83"/>
        <location filename="../src/functionsdialog.cpp" line="428"/>
        <location filename="../src/functionsdialog.cpp" line="431"/>
        <source>Deactivate</source>
        <translation>Деактивировать</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="84"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="86"/>
        <source>Calculate…</source>
        <translation>Рассчитать…</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="88"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="89"/>
        <source>Insert</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="313"/>
        <source>argument</source>
        <translation>аргумент</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="336"/>
        <source>Retrieves data from the %1 data set for a given object and property. If &quot;info&quot; is typed as property, a dialog window will pop up with all properties of the object.</source>
        <translation>Извлекает данные из набора данных %1 для заданного объекта и свойства. Если «инфо» введено как свойство, появится диалоговое окно со всеми свойствами объекта.</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="346"/>
        <source>Example:</source>
        <translation>Пример:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="358"/>
        <source>Arguments</source>
        <translation>Аргументы</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="375"/>
        <source>optional</source>
        <comment>optional argument</comment>
        <translation>необязательный</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="378"/>
        <source>default:</source>
        <comment>argument default</comment>
        <translation>по умолчанию:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="390"/>
        <source>Requirement:</source>
        <comment>Required condition for function</comment>
        <translation>Требование:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="398"/>
        <source>Properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="405"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="415"/>
        <source>key</source>
        <extracomment>indicating that the property is a data set key</extracomment>
        <translation>ключ</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="433"/>
        <source>Activate</source>
        <translation>Активировать</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="553"/>
        <source>All</source>
        <comment>All functions</comment>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="596"/>
        <source>Uncategorized</source>
        <translation>Без категорий</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="602"/>
        <source>User functions</source>
        <translation>Пользовательские функции</translation>
    </message>
    <message>
        <location filename="../src/functionsdialog.cpp" line="277"/>
        <location filename="../src/functionsdialog.cpp" line="609"/>
        <source>Inactive</source>
        <translation>Неактивные</translation>
    </message>
</context>
<context>
    <name>HistoryView</name>
    <message>
        <location filename="../src/historyview.cpp" line="659"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="662"/>
        <source>Copy Formatted Text</source>
        <translation>Копировать отформатированный текст</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="663"/>
        <source>Select All</source>
        <translation>Выделите всё</translation>
    </message>
    <message>
        <source>Find…</source>
        <translation type="vanished">Найти…</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="657"/>
        <source>Insert Value</source>
        <translation type="unfinished">Вставить значение</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="658"/>
        <source>Insert Text</source>
        <translation type="unfinished">Вставить текст</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="666"/>
        <source>Search…</source>
        <translation type="unfinished">Поиск…</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="670"/>
        <source>Protect</source>
        <translation>Защитить</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="672"/>
        <source>Move to Top</source>
        <translation>Передвинуть наверх</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="674"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="675"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <source>Find</source>
        <translation type="vanished">Найти</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="726"/>
        <source>Text:</source>
        <translation>Текст:</translation>
    </message>
    <message>
        <location filename="../src/historyview.cpp" line="724"/>
        <location filename="../src/historyview.cpp" line="731"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
</context>
<context>
    <name>KeypadButton</name>
    <message>
        <location filename="../src/keypadwidget.cpp" line="508"/>
        <source>&lt;i&gt;Right-click/long press&lt;/i&gt;: %1</source>
        <translation>&lt;i&gt;Щелчок правой кнопкой/долгое нажатие&lt;/i&gt;: %1</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="509"/>
        <source>&lt;i&gt;Right-click&lt;/i&gt;: %1</source>
        <translation>&lt;i&gt;Щелчок правой кнопкой&lt;/i&gt;: %1</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="513"/>
        <source>&lt;i&gt;Middle-click&lt;/i&gt;: %1</source>
        <translation>&lt;i&gt;Щелчок средней кнопкой&lt;/i&gt;: %1</translation>
    </message>
</context>
<context>
    <name>KeypadWidget</name>
    <message>
        <location filename="../src/keypadwidget.cpp" line="158"/>
        <source>Memory store</source>
        <translation>Сохранить в памяти</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="161"/>
        <source>Memory clear</source>
        <translation>Отчистить память</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="167"/>
        <source>Memory recall</source>
        <translation>Вызвать из памяти</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="173"/>
        <source>Memory add</source>
        <translation>Прибавить к значению в памяти</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="173"/>
        <source>Memory subtract</source>
        <translation>Отнять от значения в памяти</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="183"/>
        <source>sin</source>
        <translation>sin</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="185"/>
        <source>cos</source>
        <translation>cos</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="187"/>
        <source>tan</source>
        <translation>tg</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="224"/>
        <source>Uncertainty/interval</source>
        <translation>Неопределённость/интервал</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="224"/>
        <source>Relative error</source>
        <translation>Относительная ошибка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="224"/>
        <source>Interval</source>
        <translation>Интервал</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="226"/>
        <source>Move cursor left</source>
        <translation>Перемещать курсор влево</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="226"/>
        <source>Move cursor to start</source>
        <translation>Переместить курсор в начало</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="232"/>
        <source>Move cursor right</source>
        <translation>Перемещать курсор вправо</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="232"/>
        <source>Move cursor to end</source>
        <translation>Переместить курсор в конец</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="240"/>
        <source>Left parenthesis</source>
        <translation>Левая скобка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="240"/>
        <source>Left vector bracket</source>
        <translation>Левая векторная скобка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="242"/>
        <source>Right parenthesis</source>
        <translation>Правая скобка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="242"/>
        <source>Right vector bracket</source>
        <translation>Правая векторная скобка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="244"/>
        <source>Smart parentheses</source>
        <translation>Умные скобки</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="244"/>
        <source>Vector brackets</source>
        <translation>Векторные скобки</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="250"/>
        <source>Argument separator</source>
        <translation>Разделитель аргументов</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="250"/>
        <location filename="../src/keypadwidget.cpp" line="269"/>
        <source>Blank space</source>
        <translation>Пустой пробел</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="250"/>
        <location filename="../src/keypadwidget.cpp" line="269"/>
        <source>New line</source>
        <translation>Новая строка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="269"/>
        <source>Decimal point</source>
        <translation>Десятичная точка</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="297"/>
        <source>Previous result (static)</source>
        <translation>Предыдущий результат (статический)</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="303"/>
        <source>Bitwise AND</source>
        <translation>Побитовое И</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="309"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="309"/>
        <source>Backspace</source>
        <translation>Обратное перемещение</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="324"/>
        <source>Bitwise OR</source>
        <translation>Побитовое ИЛИ</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="324"/>
        <source>Bitwise NOT</source>
        <translation>Побитовое НЕ</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="326"/>
        <source>Clear expression</source>
        <translation>Отчистить выражение</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="332"/>
        <source>Calculate expression</source>
        <translation>Вычислить выражение</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="192"/>
        <source>Exponentiation</source>
        <translation>Возведение в степень</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="222"/>
        <source>Percent or remainder</source>
        <translation>Процент или остаток</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="303"/>
        <source>Multiplication</source>
        <translation>Умножение</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="303"/>
        <source>Bitwise Shift</source>
        <translation>Побитовый сдвиг</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="312"/>
        <source>Addition</source>
        <translation>Сложение</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="312"/>
        <source>Plus</source>
        <translation>Плюс</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="317"/>
        <location filename="../src/keypadwidget.cpp" line="320"/>
        <source>Subtraction</source>
        <translation>Вычитание</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="317"/>
        <location filename="../src/keypadwidget.cpp" line="320"/>
        <source>Minus</source>
        <translation>Минус</translation>
    </message>
    <message>
        <location filename="../src/keypadwidget.cpp" line="324"/>
        <source>Division</source>
        <translation>Деление</translation>
    </message>
</context>
<context>
    <name>PlotDialog</name>
    <message>
        <location filename="../src/plotdialog.cpp" line="45"/>
        <source>Plot</source>
        <translation>График</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="54"/>
        <source>Data</source>
        <translation>Данные</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="56"/>
        <location filename="../src/plotdialog.cpp" line="147"/>
        <source>Title:</source>
        <translation>Заголовок:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="58"/>
        <source>Expression:</source>
        <translation>Выражение:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="63"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="64"/>
        <source>Vector/matrix</source>
        <translation>Вектор/матрица</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="65"/>
        <source>Paired matrix</source>
        <translation>Парная матрица</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="68"/>
        <source>Rows</source>
        <translation>Строки</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="69"/>
        <source>X variable:</source>
        <translation>Переменная X:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="74"/>
        <source>Style:</source>
        <translation>Стиль:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="76"/>
        <source>Line</source>
        <translation>Линия</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="77"/>
        <source>Points</source>
        <translation>Символы</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="78"/>
        <source>Line with points</source>
        <translation>Линия с символами</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="79"/>
        <source>Boxes/bars</source>
        <translation>Прямоугольники/планки погрешностей</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="80"/>
        <source>Histogram</source>
        <translation>Гистограмма</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="81"/>
        <source>Steps</source>
        <translation>Ступенчатый</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="82"/>
        <source>Candlesticks</source>
        <translation>Японские свечи</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="83"/>
        <source>Dots</source>
        <translation>Точки</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="85"/>
        <source>Smoothing:</source>
        <translation>Сглаживание:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="87"/>
        <location filename="../src/plotdialog.cpp" line="182"/>
        <source>None</source>
        <translation>Никакой</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="88"/>
        <source>Monotonic</source>
        <translation>Монотонное</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="89"/>
        <source>Natural cubic splines</source>
        <translation>Естественные кубические сплайны</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="90"/>
        <source>Bezier</source>
        <translation>Безье</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="91"/>
        <source>Bezier (monotonic)</source>
        <translation>Безье (монотонное)</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="93"/>
        <source>Y-axis:</source>
        <translation>Ось Y:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="95"/>
        <source>Primary</source>
        <translation>Основная</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="96"/>
        <source>Secondary</source>
        <translation>Вторичная</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="101"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="104"/>
        <location filename="../src/plotdialog.cpp" line="140"/>
        <location filename="../src/plotdialog.cpp" line="190"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="107"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="114"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="114"/>
        <source>Expression</source>
        <translation>Выражение</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="120"/>
        <source>Function Range</source>
        <translation>Диапазон функции</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="122"/>
        <source>Minimum x value:</source>
        <translation>Минимальное значение x:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="125"/>
        <source>Maximum x value:</source>
        <translation>Максимальное значение x:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="129"/>
        <source>Sampling rate:</source>
        <translation>Частота дискретизации:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="134"/>
        <source>Step size:</source>
        <translation>Размер шага:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="145"/>
        <source>Appearance</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="149"/>
        <source>Display grid</source>
        <translation>Показать сетку</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="151"/>
        <source>Display full border</source>
        <translation>Показать полную рамку</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="153"/>
        <source>Minimum y value:</source>
        <translation>Минимальное значение y:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="158"/>
        <source>Maximum y value:</source>
        <translation>Максимальное значение y:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="163"/>
        <source>Logarithmic x scale:</source>
        <translation>Логарифмическая шкала абсцисс x:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="168"/>
        <source>Logarithmic y scale:</source>
        <translation>Логарифмическая шкала ординат y:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="173"/>
        <source>X-axis label:</source>
        <translation>Подпись для оси абсцисс X:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="175"/>
        <source>Y-axis label:</source>
        <translation>Подпись для оси ординат Y:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="177"/>
        <source>Line width:</source>
        <translation>Толщина линии:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="180"/>
        <source>Legend placement:</source>
        <translation>Размещение легенды:</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="183"/>
        <source>Top-left</source>
        <translation>Сверху слева</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="184"/>
        <source>Top-right</source>
        <translation>Сверху справа</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="185"/>
        <source>Bottom-left</source>
        <translation>Снизу слева</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="186"/>
        <source>Bottom-right</source>
        <translation>Снизу справа</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="187"/>
        <source>Below</source>
        <translation>Ниже</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="188"/>
        <source>Outside</source>
        <translation>Извне</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="350"/>
        <location filename="../src/plotdialog.cpp" line="351"/>
        <source>Calculating…</source>
        <translation>Расчёт…</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="350"/>
        <location filename="../src/plotdialog.cpp" line="497"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/plotdialog.cpp" line="497"/>
        <source>Processing…</source>
        <translation>Обработка…</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="54"/>
        <source>Look &amp;&amp; Feel</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="55"/>
        <source>Numbers &amp;&amp; Operators</source>
        <translation>Числа и операторы</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="56"/>
        <source>Units &amp;&amp; Currencies</source>
        <translation>Единицы измерения и валюты</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="57"/>
        <source>Parsing &amp;&amp; Calculation</source>
        <translation>Разбор и вычисление</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="61"/>
        <source>Ignore system language (requires restart)</source>
        <translation>Игнорировать системный язык (требуется перезапуск)</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="62"/>
        <source>Allow multiple instances</source>
        <translation>Разрешить несколько экземпляров</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="63"/>
        <source>Clear history on exit</source>
        <translation>Очищать историю при выходе</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="64"/>
        <source>Keep above other windows</source>
        <translation>Поддерживать поверх других окон</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="65"/>
        <source>Window title:</source>
        <translation>Заголовок окна:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="67"/>
        <source>Application name</source>
        <translation>Имя приложения</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="68"/>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="69"/>
        <source>Application name + result</source>
        <translation>Имя приложения + результат</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="73"/>
        <source>Style:</source>
        <translation>Стиль:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="76"/>
        <source>Default (requires restart)</source>
        <comment>Default style</comment>
        <translation>По умолчанию (требуется перезапуск)</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="81"/>
        <source>Dark mode</source>
        <translation>Тёмный режим</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="82"/>
        <source>Colorize result</source>
        <translation>Раскрасить результат</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="83"/>
        <source>Custom result font:</source>
        <translation>Шрифт результатов:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="86"/>
        <source>Custom expression font:</source>
        <translation>Шрифт выражения:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="89"/>
        <source>Custom keypad font:</source>
        <translation>Шрифт клавиатуры:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="92"/>
        <source>Custom application font:</source>
        <translation>Шрифт приложения:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="98"/>
        <source>Display expression status</source>
        <translation>Показывать статус выражения</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="101"/>
        <source>Delay:</source>
        <translation>Задержка:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="111"/>
        <source>Expression after calculation:</source>
        <translation>Выражение после расчёта:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="113"/>
        <source>Keep expression</source>
        <translation>Сохранить выражение</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="114"/>
        <source>Clear expression</source>
        <translation>Отчистить выражение</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="115"/>
        <source>Replace with result</source>
        <translation>Заменить на результат</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="116"/>
        <source>Replace with result if shorter</source>
        <translation>Заменить на результат, если он короче</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="120"/>
        <source>Use keyboard keys for RPN</source>
        <translation>Использовать клавиатуру для ПОЛИЗ</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="121"/>
        <source>Parsing mode:</source>
        <translation>Режим анализа:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="123"/>
        <location filename="../src/preferencesdialog.cpp" line="172"/>
        <source>Adaptive</source>
        <translation>Адаптивный</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="124"/>
        <source>Conventional</source>
        <translation>Общепринятый</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="125"/>
        <source>Implicit multiplication first</source>
        <translation>Неявный первый</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="126"/>
        <source>Chain</source>
        <translation>Цепь</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="127"/>
        <source>RPN</source>
        <translation>ПОЛИЗ</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="132"/>
        <source>Read precision</source>
        <translation>Точность чтения</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="133"/>
        <source>Limit implicit multiplication</source>
        <translation>Ограничить неявное умножение</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="134"/>
        <source>Interval calculation:</source>
        <translation>Расчёт интервала:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="136"/>
        <source>Variance formula</source>
        <translation>Формула дисперсии</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="137"/>
        <source>Interval arithmetic</source>
        <translation>Арифметика интервалов</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="141"/>
        <source>Factorize result</source>
        <translation>Разложить результат на множители</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="144"/>
        <source>Binary two&apos;s complement representation</source>
        <translation>Представление двоичных чисел с дополнительным кодом</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="145"/>
        <source>Hexadecimal two&apos;s complement representation</source>
        <translation>Представление шестнадцатеричных чисел с дополнительным кодом</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="146"/>
        <source>Use lower case letters in non-decimal numbers</source>
        <translation>Использовать строчные буквы в недесятичных числах</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="149"/>
        <source>Spell out logical operators</source>
        <translation>Изложить логично логические операции</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="45"/>
        <source>Preferences</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="106"/>
        <source>ms</source>
        <extracomment>milliseconds</extracomment>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="147"/>
        <source>Use dot as multiplication sign</source>
        <translation>Использовать точку как знак умножения</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="148"/>
        <source>Use Unicode division slash in output</source>
        <translation type="unfinished">Использовать косую черту деления Юнокода в выводе</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="150"/>
        <source>Use E-notation instead of 10^n</source>
        <translation>Использовать E-нотацию вместо 10^n</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="151"/>
        <source>Use &apos;j&apos; as imaginary unit</source>
        <translation>Использовать «j» для мнимой единицы</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="152"/>
        <source>Use comma as decimal separator</source>
        <translation>Использовать запятую в качестве десятичного разделителя</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="154"/>
        <source>Ignore comma in numbers</source>
        <translation>Игнорировать запятую в числах</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="155"/>
        <source>Ignore dots in numbers</source>
        <translation>Игнорировать точки в числах</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="156"/>
        <source>Round halfway numbers to even</source>
        <translation>Округлять половинные числа до ближайшего чётного целого числа</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="157"/>
        <source>Indicate repeating decimals</source>
        <translation>Указывать повторяющиеся десятичные дроби</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="162"/>
        <source>Digit grouping:</source>
        <translation>Группировка цифр:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="164"/>
        <source>None</source>
        <translation>Никакой</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="165"/>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="166"/>
        <source>Local</source>
        <translation>Локаль</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="170"/>
        <source>Interval display:</source>
        <translation>Отображение интервалов:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="173"/>
        <source>Significant digits</source>
        <translation>Значимые цифры</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="174"/>
        <source>Interval</source>
        <translation>Интервал</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="175"/>
        <source>Plus/minus</source>
        <translation>Плюс/минус</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="176"/>
        <source>Midpoint</source>
        <translation>Середина</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="177"/>
        <source>Lower</source>
        <translation>Ниже</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="178"/>
        <source>Upper</source>
        <translation>Выше</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="183"/>
        <source>Complex number form:</source>
        <translation>Комплексная форма:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="185"/>
        <source>Rectangular</source>
        <translation>Прямоугольная</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="186"/>
        <source>Exponential</source>
        <translation>Экспоненциальная</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="187"/>
        <source>Polar</source>
        <translation>Полярная</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="188"/>
        <source>Angle/phasor</source>
        <translation>Угловая/векторная</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="196"/>
        <source>Abbreviate names</source>
        <translation>Сокращённые имена</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="197"/>
        <source>Use binary prefixes for information units</source>
        <translation>Использовать двоичные префиксы для информационных единиц</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="198"/>
        <source>Automatic unit conversion:</source>
        <translation>Автоматическое преобразование единиц:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="200"/>
        <source>No conversion</source>
        <translation>Без преобразования</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="201"/>
        <source>Base units</source>
        <translation>Основные единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="202"/>
        <source>Optimal units</source>
        <translation>Оптимальные единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="203"/>
        <source>Optimal SI units</source>
        <translation>Оптимальные единицы СИ</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="207"/>
        <source>Convert to mixed units</source>
        <translation>Преобразовать в смешанные единицы</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="208"/>
        <source>Automatic unit prefixes:</source>
        <translation>Автоматические префиксы единиц:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="210"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="211"/>
        <source>No prefixes</source>
        <translation>Без префиксов</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="212"/>
        <source>Prefixes for some units</source>
        <translation>Префиксы для выбранных единиц</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="213"/>
        <source>Prefixes also for currencies</source>
        <translation>Префиксы также для валют</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="214"/>
        <source>Prefixes for all units</source>
        <translation>Префиксы для всех единиц</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="222"/>
        <source>Enable all SI-prefixes</source>
        <translation>Включить все префиксы СИ</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="223"/>
        <source>Enable denominator prefixes</source>
        <translation>Включить префиксы знаменателя</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="224"/>
        <source>Enable units in physical constants</source>
        <translation>Включить единицы измерения в физических константах</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="225"/>
        <source>Temperature calculation:</source>
        <translation>Режим расчёта температуры:</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="227"/>
        <source>Absolute</source>
        <translation>Абсолютный</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="228"/>
        <source>Relative</source>
        <translation>Относительный</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="229"/>
        <source>Hybrid</source>
        <translation>Гибридный</translation>
    </message>
    <message>
        <location filename="../src/preferencesdialog.cpp" line="234"/>
        <source>Exchange rates updates:</source>
        <translation>Обновления курсов валют:</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/preferencesdialog.cpp" line="237"/>
        <location filename="../src/preferencesdialog.cpp" line="372"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n день</numerusform>
            <numerusform>%n дня</numerusform>
            <numerusform>%n дней</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>days</source>
        <translation type="vanished">
            <numerusform>день</numerusform>
            <numerusform>дня</numerusform>
            <numerusform>дней</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="71"/>
        <source>Execute expressions and commands from a file</source>
        <translation>Выполнить выражения и команды из файла</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="71"/>
        <source>FILE</source>
        <translation>ФАЙЛ</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="73"/>
        <source>Start a new instance of the application</source>
        <translation>Запустить новый экземпляр приложения</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="75"/>
        <source>Specify the window title</source>
        <translation>Укажите заголовок окна</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="75"/>
        <source>TITLE</source>
        <translation>НАЗВАНИЕ</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="77"/>
        <source>Display the application version</source>
        <translation>Показать версию приложения</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="79"/>
        <source>Expression to calculate</source>
        <translation>Выражение для вычисления</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="79"/>
        <source>[EXPRESSION]</source>
        <translation>[ВЫРАЖЕНИЕ]</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="95"/>
        <source>By default, only one instance (one main window) of %1 is allowed.

If multiple instances are opened simultaneously, only the definitions (variables, functions, etc.), mode, preferences, and history of the last closed window will be saved.

Do you, despite this, want to change the default behavior and allow multiple simultaneous instances?</source>
        <translation>По умолчанию разрешён только один экземпляр (одно главное окно) %1.

Если несколько экземпляров открыты одновременно, будут сохранены только определения (переменные, функции и т. д.), режим, настройки и история последнего закрытого окна.

Вы, несмотря на это, хотите изменить поведение по умолчанию и разрешить одновременную работу нескольких экземпляров?</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="109"/>
        <source>%1 is already running.</source>
        <translation>%1 уже исполняется.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="149"/>
        <source>Failed to load global definitions!
</source>
        <translation>Не удалось загрузить глобальные определения!
</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="63"/>
        <location filename="../src/qalculateqtsettings.cpp" line="64"/>
        <location filename="../src/qalculateqtsettings.cpp" line="687"/>
        <source>answer</source>
        <translation>ответ</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="63"/>
        <source>History Answer Value</source>
        <translation>Значение ответа в истории</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="65"/>
        <source>History Index(es)</source>
        <translation>Индекс(ы) истории</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="76"/>
        <source>History index %s does not exist.</source>
        <translation>Индекс истории %s не существует.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="686"/>
        <source>Last Answer</source>
        <translation>Последний ответ</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="689"/>
        <source>Answer 2</source>
        <translation>Ответ 2</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="690"/>
        <source>Answer 3</source>
        <translation>Ответ 3</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="691"/>
        <source>Answer 4</source>
        <translation>Ответ 4</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="692"/>
        <source>Answer 5</source>
        <translation>Ответ 5</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="693"/>
        <source>Memory</source>
        <translation>Память</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="777"/>
        <source>Couldn&apos;t write preferences to
%1</source>
        <translation>Не удалось записать настройки в
%1</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="777"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>QalculateQtSettings</name>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1062"/>
        <source>Update exchange rates?</source>
        <translation>Обновить курсы валют?</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/qalculateqtsettings.cpp" line="1062"/>
        <source>It has been %n day(s) since the exchange rates last were updated.

Do you wish to update the exchange rates now?</source>
        <translation>
            <numerusform>Прошёл %n день с момента последнего обновления курсов обмена.

Вы хотите обновить курсы обмена сейчас?</numerusform>
            <numerusform>Прошло %n дня с момента последнего обновления курсов обмена.

Вы хотите обновить курсы обмена сейчас?</numerusform>
            <numerusform>Прошло %n дней с момента последнего обновления курсов обмена.

Вы хотите обновить курсы обмена сейчас?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1090"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1091"/>
        <source>Fetching exchange rates…</source>
        <translation>Получение курсов валют…</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1137"/>
        <source>Path of executable not found.</source>
        <translation>Путь к исполняемому файлу не найден.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1146"/>
        <source>curl not found.</source>
        <translation>Программа curl не найдена.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1197"/>
        <source>Failed to run update script.
%1</source>
        <translation>Не удалось запустить скрипт обновления.
%1</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1220"/>
        <source>Failed to check for updates.</source>
        <translation>Не удалось проверить наличие обновлений.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1221"/>
        <source>No updates found.</source>
        <translation>Обновлений не найдено.</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1227"/>
        <source>A new version of %1 is available at %2.

Do you wish to update to version %3?</source>
        <translation type="unfinished">Новая версия %1 доступна: %2.

Вы хотите обновиться до версии %3?</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1231"/>
        <source>A new version of %1 is available.

You can get version %3 at %2.</source>
        <translation type="unfinished">Доступна новая версия %1.

Вы можете получить версию %3: %2.</translation>
    </message>
    <message>
        <source>&lt;div&gt;A new version of %1 is available at %2.

Do you wish to update to version %3?&lt;/div&gt;</source>
        <translation type="vanished">&lt;div&gt;Новая версия %1 доступна: %2.

Вы хотите обновиться до версии %3?&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div&gt;A new version of %1 is available.

You can get version %3 at %2.&lt;/div&gt;</source>
        <translation type="vanished">&lt;div&gt;Доступна новая версия %1.

Вы можете получить версию %3: %2.&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1124"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1137"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1146"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1197"/>
        <location filename="../src/qalculateqtsettings.cpp" line="1220"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1125"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/qalculateqtsettings.cpp" line="1126"/>
        <source>Information</source>
        <translation>Информирование</translation>
    </message>
</context>
<context>
    <name>QalculateTranslator</name>
    <message>
        <location filename="../src/main.cpp" line="199"/>
        <source>Cancel</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>Close</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="197"/>
        <source>OK</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="203"/>
        <source>&amp;Yes</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>&amp;Да</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="205"/>
        <source>&amp;No</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>&amp;Нет</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="207"/>
        <source>&amp;Open</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="209"/>
        <source>&amp;Save</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="211"/>
        <source>&amp;Select All</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>&amp;Выбрать все</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="213"/>
        <source>Look in:</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>Смотреть в:</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="215"/>
        <source>File &amp;name:</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>Имя &amp;файла:</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="217"/>
        <source>Files of type:</source>
        <extracomment>Only used when Qt translation is missing</extracomment>
        <translation>Тип файлов:</translation>
    </message>
</context>
<context>
    <name>QalculateWindow</name>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="292"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="293"/>
        <source>Menu (%1)</source>
        <translation>Меню (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="298"/>
        <source>New</source>
        <translation>Новая</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="299"/>
        <source>Function…</source>
        <translation>Функция…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="300"/>
        <source>Variable/Constant…</source>
        <translation>Переменная/константа…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="301"/>
        <source>Unknown Variable…</source>
        <translation>Переменная неизвестного…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="302"/>
        <source>Matrix…</source>
        <translation>Матрица…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="305"/>
        <source>Import CSV File…</source>
        <translation>Загрузить CSV файл…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="306"/>
        <source>Export CSV File…</source>
        <translation>Экспорт в CSV файл…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="308"/>
        <location filename="../src/qalculatewindow.cpp" line="552"/>
        <source>Functions</source>
        <translation>Функции</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="309"/>
        <source>Variables and Constants</source>
        <translation>Переменные и константы</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="310"/>
        <source>Units</source>
        <translation>Единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="312"/>
        <source>Plot Functions/Data</source>
        <translation>Графики функций/данных</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="313"/>
        <source>Floating Point Conversion (IEEE 754)</source>
        <translation>Преобразование чисел с плавающей запятой (IEEE 754)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="314"/>
        <source>Calendar Conversion</source>
        <translation>Преобразование календаря</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="316"/>
        <source>Update Exchange Rates</source>
        <translation>Обновить курсы валют</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="319"/>
        <source>Normal Mode</source>
        <translation>Режим нормальный</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="320"/>
        <source>RPN Mode</source>
        <translation>Режим ПОЛИЗ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="321"/>
        <source>Chain Mode</source>
        <translation>Режим «цепь»</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="323"/>
        <source>Preferences</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="325"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="326"/>
        <source>Report a Bug</source>
        <translation>Сообщить об ошибке</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="327"/>
        <source>Check for Updates</source>
        <translation>Проверить обновления</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="328"/>
        <location filename="../src/qalculatewindow.cpp" line="329"/>
        <location filename="../src/qalculatewindow.cpp" line="892"/>
        <source>About %1</source>
        <translation>О %1</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="331"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="334"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="335"/>
        <source>Mode (%1)</source>
        <translation>Режим (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="340"/>
        <location filename="../src/qalculatewindow.cpp" line="343"/>
        <source>General Display Mode</source>
        <translation>Режим общего отображения</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="345"/>
        <source>Normal</source>
        <translation>Обычное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="348"/>
        <source>Scientific</source>
        <translation>Научное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="351"/>
        <source>Engineering</source>
        <translation>Инженерное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="354"/>
        <source>Simple</source>
        <translation>Простое</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="358"/>
        <location filename="../src/qalculatewindow.cpp" line="359"/>
        <source>Angle Unit</source>
        <translation>Единица измерения углов</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="361"/>
        <source>Radians</source>
        <translation>Радианы</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="363"/>
        <source>Degrees</source>
        <translation>Градусы</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="365"/>
        <source>Gradians</source>
        <translation>Грады</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="368"/>
        <location filename="../src/qalculatewindow.cpp" line="369"/>
        <source>Approximation</source>
        <translation>Приближение</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="371"/>
        <source>Automatic</source>
        <comment>Automatic approximation</comment>
        <translation>Автоматическое</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="373"/>
        <source>Dual</source>
        <comment>Dual approximation</comment>
        <translation>Двойное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="375"/>
        <source>Exact</source>
        <comment>Exact approximation</comment>
        <translation>Точное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="377"/>
        <source>Approximate</source>
        <translation>Приблизительное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="382"/>
        <source>Assumptions</source>
        <translation>Предположения</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="383"/>
        <source>Type</source>
        <comment>Assumptions type</comment>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="385"/>
        <source>Number</source>
        <translation>Число</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="387"/>
        <source>Real</source>
        <translation>Вещественное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="389"/>
        <source>Rational</source>
        <translation>Рациональное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="391"/>
        <source>Integer</source>
        <translation>Целое</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="393"/>
        <source>Boolean</source>
        <translation>Логическое</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="395"/>
        <source>Sign</source>
        <comment>Assumptions sign</comment>
        <translation>Знак</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="397"/>
        <source>Unknown</source>
        <comment>Unknown assumptions sign</comment>
        <translation>Неизвестное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="399"/>
        <source>Non-zero</source>
        <translation>Ненулевое</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="401"/>
        <source>Positive</source>
        <translation>Положительное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="403"/>
        <source>Non-negative</source>
        <translation>Неотрицательное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="405"/>
        <source>Negative</source>
        <translation>Отрицательное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="407"/>
        <source>Non-positive</source>
        <translation>Не положительное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="411"/>
        <location filename="../src/qalculatewindow.cpp" line="412"/>
        <source>Result Base</source>
        <translation>Основание для результата</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="415"/>
        <location filename="../src/qalculatewindow.cpp" line="470"/>
        <source>Binary</source>
        <translation>Двоичное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="417"/>
        <location filename="../src/qalculatewindow.cpp" line="472"/>
        <source>Octal</source>
        <translation>Восьмеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="419"/>
        <location filename="../src/qalculatewindow.cpp" line="474"/>
        <source>Decimal</source>
        <translation>Десятичное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="421"/>
        <location filename="../src/qalculatewindow.cpp" line="476"/>
        <source>Hexadecimal</source>
        <translation>Шестнадцатеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="424"/>
        <location filename="../src/qalculatewindow.cpp" line="479"/>
        <source>Other</source>
        <translation>Другое</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="425"/>
        <location filename="../src/qalculatewindow.cpp" line="480"/>
        <source>Duodecimal</source>
        <translation>Двенадцатеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="427"/>
        <source>Sexagesimal</source>
        <translation>Шестидесятеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="429"/>
        <source>Time format</source>
        <translation>Формат времени</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="431"/>
        <location filename="../src/qalculatewindow.cpp" line="482"/>
        <source>Roman numerals</source>
        <translation>Римские цифры</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="433"/>
        <location filename="../src/qalculatewindow.cpp" line="484"/>
        <source>Unicode</source>
        <translation>Юникод</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="435"/>
        <location filename="../src/qalculatewindow.cpp" line="486"/>
        <source>Bijective base-26</source>
        <translation>Биективное основание-26</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="498"/>
        <source>Custom:</source>
        <comment>Number base</comment>
        <translation>Пользовательское:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="466"/>
        <location filename="../src/qalculatewindow.cpp" line="467"/>
        <source>Expression Base</source>
        <translation>Основание для выражения</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="451"/>
        <source>Other:</source>
        <comment>Number base</comment>
        <translation>Другое:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="518"/>
        <source>Precision:</source>
        <translation>Точность:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="525"/>
        <source>Min decimals:</source>
        <translation>Минимум цифр:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="532"/>
        <source>Max decimals:</source>
        <translation>Максимум цифр:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="535"/>
        <source>off</source>
        <comment>Max decimals</comment>
        <translation>выкл</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="545"/>
        <source>Convert</source>
        <translation>Перевести</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="546"/>
        <source>Convert (%1)</source>
        <translation>Перевести (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="549"/>
        <source>Store</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="549"/>
        <source>Store (%1)</source>
        <translation>Сохранить (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="552"/>
        <source>Functions (%1)</source>
        <translation>Функции (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="560"/>
        <location filename="../src/qalculatewindow.cpp" line="637"/>
        <source>Keypad</source>
        <translation>Клавиатура</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="561"/>
        <source>Keypad (%1)</source>
        <translation>Клавиатура (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="555"/>
        <location filename="../src/qalculatewindow.cpp" line="587"/>
        <source>Number bases</source>
        <translation>Основания систем счисления</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="556"/>
        <source>Number Bases (%1)</source>
        <translation>Основания систем счисления (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="591"/>
        <source>Binary:</source>
        <translation>Двоичное:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="593"/>
        <source>Octal:</source>
        <translation>Восьмеричное:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="595"/>
        <source>Decimal:</source>
        <translation>Десятичное:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="597"/>
        <source>Hexadecimal:</source>
        <translation>Шестнадцатеричное:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="644"/>
        <source>RPN Stack</source>
        <translation>Стек ПОЛИЗ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="663"/>
        <source>Rotate the stack or move the selected register up (%1)</source>
        <translation>Повернуть стек или переместить выбранный регистр вверх (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="666"/>
        <source>Rotate the stack or move the selected register down (%1)</source>
        <translation>Повернуть стек или переместить выбранный регистр вниз (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="669"/>
        <source>Swap the top two values or move the selected value to the top of the stack (%1)</source>
        <translation>Поменять местами два верхних значения или переместить выбранное значение в вершину стека (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="672"/>
        <source>Copy the selected or top value to the top of the stack (%1)</source>
        <translation>Скопировать выбранное или верхнее значение в вершину стека (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="675"/>
        <source>Enter the top value from before the last numeric operation (%1)</source>
        <translation>Введите верхнее значение перед последней числовой операцией (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="678"/>
        <source>Delete the top or selected value (%1)</source>
        <translation>Удалить верхнее или выбранное значение (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="681"/>
        <source>Clear the RPN stack (%1)</source>
        <translation>Очистить стек ПОЛИЗ (%1)</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="892"/>
        <source>Powerful and easy to use calculator</source>
        <translation>Мощный и простой в использовании калькулятор</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2209"/>
        <source>Couldn&apos;t write definitions</source>
        <translation>Не удалось записать определения</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2717"/>
        <source>hexadecimal</source>
        <translation>шестнадцатеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2720"/>
        <source>octal</source>
        <translation>восьмеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2723"/>
        <source>decimal</source>
        <translation>десятеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2726"/>
        <source>duodecimal</source>
        <translation>двенадцатеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2729"/>
        <source>binary</source>
        <translation>двоичное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2732"/>
        <source>roman</source>
        <translation>римское число</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2735"/>
        <source>bijective</source>
        <translation>биективное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2738"/>
        <location filename="../src/qalculatewindow.cpp" line="2741"/>
        <location filename="../src/qalculatewindow.cpp" line="2744"/>
        <source>sexagesimal</source>
        <translation>шестидесятеричное</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2747"/>
        <location filename="../src/qalculatewindow.cpp" line="2750"/>
        <source>latitude</source>
        <translation>широта</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2753"/>
        <location filename="../src/qalculatewindow.cpp" line="2756"/>
        <source>longitude</source>
        <translation>долгота</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2774"/>
        <source>time</source>
        <translation>время</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2825"/>
        <source>Time zone parsing failed.</source>
        <translation>Не удалось выполнить синтаксический анализ часового пояса.</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2848"/>
        <source>bases</source>
        <translation>основания</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2857"/>
        <source>calendars</source>
        <translation>календари</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2866"/>
        <source>rectangular</source>
        <translation>прямоугоная</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2866"/>
        <source>cartesian</source>
        <translation>декартова</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2877"/>
        <source>exponential</source>
        <translation>экспоненциальная</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2888"/>
        <source>polar</source>
        <translation>полярная</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2912"/>
        <source>phasor</source>
        <translation>фазовая</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2912"/>
        <source>angle</source>
        <translation>угловая</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2923"/>
        <source>optimal</source>
        <translation>оптимально</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2934"/>
        <location filename="../src/qalculatewindow.cpp" line="2980"/>
        <source>base</source>
        <translation>основание</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2945"/>
        <source>mixed</source>
        <translation>смешано</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2959"/>
        <source>fraction</source>
        <translation>дробь</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2962"/>
        <source>factors</source>
        <translation>множители</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2971"/>
        <source>partial fraction</source>
        <translation>частичная дробь</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3021"/>
        <source>factorize</source>
        <translation>разложить на множители</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3024"/>
        <source>expand</source>
        <translation>раскрывать</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3195"/>
        <location filename="../src/qalculatewindow.cpp" line="3196"/>
        <location filename="../src/qalculatewindow.cpp" line="3197"/>
        <location filename="../src/qalculatewindow.cpp" line="3574"/>
        <source>Calculating…</source>
        <translation>Расчёт…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3196"/>
        <location filename="../src/qalculatewindow.cpp" line="3583"/>
        <location filename="../src/qalculatewindow.cpp" line="4081"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3250"/>
        <location filename="../src/qalculatewindow.cpp" line="3944"/>
        <source>RPN Operation</source>
        <translation>ПОЛИЗ операция</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3562"/>
        <source>Factorizing…</source>
        <translation>Факторизация…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3566"/>
        <source>Expanding partial fractions…</source>
        <translation>Расширение дробных чисел…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3570"/>
        <source>Expanding…</source>
        <translation>Расширение…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3578"/>
        <source>Converting…</source>
        <translation>Преобразование…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="3940"/>
        <source>RPN Register Moved</source>
        <translation>Регистр ПОЛИЗ перемещён</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4080"/>
        <location filename="../src/qalculatewindow.cpp" line="4081"/>
        <location filename="../src/qalculatewindow.cpp" line="4082"/>
        <source>Processing…</source>
        <translation>Обработка…</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4187"/>
        <location filename="../src/qalculatewindow.cpp" line="5453"/>
        <source>Matrix</source>
        <translation>Матрица</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4288"/>
        <source>Temperature Calculation Mode</source>
        <translation>Режим расчёта температуры</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4294"/>
        <source>The expression is ambiguous.
Please select temperature calculation mode
(the mode can later be changed in preferences).</source>
        <translation>Выражение неоднозначное.
Выберите режим расчёта температуры
(режим позже можно изменить в настройках).</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4297"/>
        <source>Absolute</source>
        <translation>Абсолютный</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4301"/>
        <source>Relative</source>
        <translation>Относительный</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4305"/>
        <source>Hybrid</source>
        <translation>Гибридный</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4346"/>
        <source>Interpretation of dots</source>
        <translation>Использование точки</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4352"/>
        <source>Please select interpretation of dots (&quot;.&quot;)
(this can later be changed in preferences).</source>
        <translation>Выберите использование точки («.»)
(позже это можно изменить в настройках).</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4355"/>
        <source>Both dot and comma as decimal separators</source>
        <translation>Точка и запятая в качестве десятичных разделителей</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4359"/>
        <source>Dot as thousands separator</source>
        <translation>Точка как разделитель тысяч</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4363"/>
        <source>Only dot as decimal separator</source>
        <translation>Только точка в качестве десятичного разделителя</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4391"/>
        <source>Parsing Mode</source>
        <translation>Режим анализа</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4397"/>
        <source>The expression is ambiguous.
Please select interpretation of expressions with implicit multiplication
(this can later be changed in preferences).</source>
        <translation>Выражение неоднозначное.
Выберите интерпретацию выражений с неявным умножением
(позже это можно изменить в настройках).</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4400"/>
        <source>Implicit multiplication first</source>
        <translation>Сначала анализировать неявное умножение</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4405"/>
        <source>Conventional</source>
        <translation>Общепринятый синтаксический анализ</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4410"/>
        <source>Adaptive</source>
        <translation>Адаптивный</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4925"/>
        <source>Gnuplot was not found</source>
        <translation>Программа Gnuplot не найдена</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="4925"/>
        <source>%1 (%2) needs to be installed separately, and found in the executable search path, for plotting to work.</source>
        <translation>Для построения графиков %1 (%2) должен быть установлен дополнительно и находиться по переменной окружения PATH.</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5107"/>
        <source>Example:</source>
        <comment>Example of function usage</comment>
        <translation>Пример:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5145"/>
        <source>Enter</source>
        <comment>RPN Enter</comment>
        <translation>Ввод</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5145"/>
        <source>Calculate</source>
        <translation>Рассчитать</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5146"/>
        <source>Apply to Stack</source>
        <translation>Применить к стеку</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5146"/>
        <source>Insert</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5137"/>
        <source>Keep open</source>
        <translation>Держать открытым</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5162"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5164"/>
        <source>Argument</source>
        <translation>Аргумент</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5177"/>
        <source>%1:</source>
        <translation>%1:</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5232"/>
        <source>True</source>
        <translation>Истина</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5235"/>
        <source>False</source>
        <translation>Ложь</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5261"/>
        <source>Info</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5296"/>
        <location filename="../src/qalculatewindow.cpp" line="5304"/>
        <source>optional</source>
        <comment>optional argument</comment>
        <translation>необязательный</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="5646"/>
        <source>Failed to open %1.
%2</source>
        <translation>Не удалось открыть %1.
%2</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="892"/>
        <source>License: GNU General Public License version 2 or later</source>
        <translation>Лицензия: GNU General Public License, версия 2 или новее</translation>
    </message>
    <message>
        <location filename="../src/qalculatewindow.cpp" line="2209"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>UnitsDialog</name>
    <message>
        <location filename="../src/unitsdialog.cpp" line="37"/>
        <source>Units</source>
        <translation>Единицы измерения</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="46"/>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="63"/>
        <location filename="../src/unitsdialog.cpp" line="537"/>
        <source>Unit</source>
        <translation>Единица измерения</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="84"/>
        <source>New…</source>
        <translation>Новая…</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="85"/>
        <source>Edit…</source>
        <translation>Правка…</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="86"/>
        <location filename="../src/unitsdialog.cpp" line="422"/>
        <location filename="../src/unitsdialog.cpp" line="425"/>
        <source>Deactivate</source>
        <translation>Деактивировать</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="87"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="89"/>
        <source>Convert</source>
        <translation>Перевести</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="90"/>
        <source>Insert</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="427"/>
        <source>Activate</source>
        <translation>Активировать</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="613"/>
        <source>All</source>
        <comment>All units</comment>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="656"/>
        <source>Uncategorized</source>
        <translation>Без категорий</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="662"/>
        <source>User units</source>
        <translation>Пользовательские единицы</translation>
    </message>
    <message>
        <location filename="../src/unitsdialog.cpp" line="332"/>
        <location filename="../src/unitsdialog.cpp" line="669"/>
        <source>Inactive</source>
        <translation>Неактивные</translation>
    </message>
</context>
<context>
    <name>UnknownEditDialog</name>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="30"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="33"/>
        <source>Custom assumptions</source>
        <translation>Собственные предположения</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="36"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="44"/>
        <source>Sign:</source>
        <translation>Знак:</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="97"/>
        <location filename="../src/unknowneditdialog.cpp" line="129"/>
        <source>A unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation>Единица измерения или переменная с таким именем уже существует.
Вы хотите её перезаписать?</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="171"/>
        <source>Edit Unknown Variable</source>
        <translation>Изменить переменную неизвестного</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="184"/>
        <source>New Unknown Variable</source>
        <translation>Новая переменная неизвестного</translation>
    </message>
    <message>
        <location filename="../src/unknowneditdialog.cpp" line="97"/>
        <location filename="../src/unknowneditdialog.cpp" line="129"/>
        <source>Question</source>
        <translation>Вопрос</translation>
    </message>
</context>
<context>
    <name>VariableEditDialog</name>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="30"/>
        <source>Name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="45"/>
        <source>Temporary</source>
        <translation>Временная</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="39"/>
        <source>Value:</source>
        <translation>Значение:</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="41"/>
        <source>current result</source>
        <translation>текущий результат</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="77"/>
        <location filename="../src/variableeditdialog.cpp" line="109"/>
        <source>A unit or variable with the same name already exists.
Do you want to overwrite it?</source>
        <translation>Единица измерения или переменная с таким именем уже существует.
Вы хотите её перезаписать?</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="181"/>
        <source>Edit Variable</source>
        <translation>Изменить переменную</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="195"/>
        <location filename="../src/variableeditdialog.cpp" line="229"/>
        <source>New Variable</source>
        <translation>Новая переменная</translation>
    </message>
    <message>
        <location filename="../src/variableeditdialog.cpp" line="77"/>
        <location filename="../src/variableeditdialog.cpp" line="109"/>
        <source>Question</source>
        <translation>Вопрос</translation>
    </message>
</context>
<context>
    <name>VariablesDialog</name>
    <message>
        <location filename="../src/variablesdialog.cpp" line="37"/>
        <source>Variables</source>
        <translation>Переменные</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="46"/>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="63"/>
        <location filename="../src/variablesdialog.cpp" line="452"/>
        <source>Variable</source>
        <translation>Переменная</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="84"/>
        <source>New</source>
        <translatorcomment>Новая переменная/константа/матрица</translatorcomment>
        <translation>Новая</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="86"/>
        <source>Variable/Constant…</source>
        <translation>Переменная/константа…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="87"/>
        <source>Unknown Variable…</source>
        <translation>Переменная неизвестного…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="88"/>
        <source>Matrix…</source>
        <translation>Матрица…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="90"/>
        <source>Edit…</source>
        <translation>Правка…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="91"/>
        <source>Export…</source>
        <translation>Экспортировать…</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="92"/>
        <location filename="../src/variablesdialog.cpp" line="386"/>
        <location filename="../src/variablesdialog.cpp" line="389"/>
        <source>Deactivate</source>
        <translation>Деактивировать</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="93"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="95"/>
        <source>Insert</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="337"/>
        <source>a matrix</source>
        <translation>матрица</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="339"/>
        <source>a vector</source>
        <translation>вектор</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="357"/>
        <source>positive</source>
        <translation>положительное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="358"/>
        <source>non-positive</source>
        <translation>не положительное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="359"/>
        <source>negative</source>
        <translation>отрицательное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="360"/>
        <source>non-negative</source>
        <translation>неотрицательное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="361"/>
        <source>non-zero</source>
        <translation>ненулевое</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="367"/>
        <source>integer</source>
        <translation>целое</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="368"/>
        <source>boolean</source>
        <translation>логическое</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="369"/>
        <source>rational</source>
        <translation>рациональное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="370"/>
        <source>real</source>
        <translation>вещественное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="371"/>
        <source>complex</source>
        <translation>комплексное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="372"/>
        <source>number</source>
        <translation>число</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="373"/>
        <source>not matrix</source>
        <translation>не матрица</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="376"/>
        <source>unknown</source>
        <translation>неизвестное</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="379"/>
        <source>Default assumptions</source>
        <translation>Предположения по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="391"/>
        <source>Activate</source>
        <translation>Активировать</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="509"/>
        <source>All</source>
        <comment>All variables</comment>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="552"/>
        <source>Uncategorized</source>
        <translation>Без категорий</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="558"/>
        <source>User variables</source>
        <translation>Пользовательские переменные</translation>
    </message>
    <message>
        <location filename="../src/variablesdialog.cpp" line="311"/>
        <location filename="../src/variablesdialog.cpp" line="565"/>
        <source>Inactive</source>
        <translation>Неактивные</translation>
    </message>
</context>
</TS>
